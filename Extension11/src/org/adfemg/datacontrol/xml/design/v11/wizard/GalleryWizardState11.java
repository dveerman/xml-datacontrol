package org.adfemg.datacontrol.xml.design.v11.wizard;

import oracle.ide.Context;

import oracle.jdeveloper.model.JProjectLibraries;

import org.adfemg.datacontrol.xml.design.wizard.GalleryWizardState;


public class GalleryWizardState11 extends GalleryWizardState {

    public GalleryWizardState11(Context context) {
        super(context);
    }

    @Override
    public void commitWizardState() {
        super.commitWizardState();
        JProjectLibraries projLibs =
            JProjectLibraries.getInstance(getProject());
        // TODO: get name from resource bundle (and use same bundle in
        // naming the library in extension.xml)
        projLibs.addLibrary("ADFEMG.org XML Data Control");
    }
}
