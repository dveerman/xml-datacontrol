package org.adfemg.datacontrol.xml.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this annotation to create a validation method on an attribute.
 * <p>
 * The signature of the annotated method has to be:<br/>
 * <code>public void validate<i>AttrName</i>(AttrChangeEvent&lt;<i>AttributeJavaClass</i>&gt; event) throws AttrValException</code>
 * <p>
 * When validation fails the annotated method should throw a
 * {@link oracle.jbo.AttrValException} or {@link oracle.jbo.AttrSetValException}.
 * Most of their constructors require a resource bundle (class), but their is
 * one simple alternative without using a resource bundle:
 * <pre>
 * throw new AttrValException(AttrValException.TYP_ATTRIBUTE, "ErrorMessage", "ERR-CODE",
 *                            event.getElement().getDefinition().getFullName(),
 *                            event.getAttribute());
 * </pre>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
@Documented
public @interface AttrValidation {
}
