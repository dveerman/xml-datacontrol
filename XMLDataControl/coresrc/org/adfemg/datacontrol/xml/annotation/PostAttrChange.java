package org.adfemg.datacontrol.xml.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Use this annotation to define a PostAttrChange method.
 * This method will be fired after the attribute has been changed. The
 * name of the annotated method determines which attribute has to
 * be changed for the method to be called.
 * <p>
 * The signature of the annotated method has to be:<br/>
 * <code>public void <i>attrName</i>Changed(XMLDCElement department)</code>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
@Documented
public @interface PostAttrChange {
}
