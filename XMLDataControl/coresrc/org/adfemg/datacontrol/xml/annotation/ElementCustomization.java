package org.adfemg.datacontrol.xml.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this annotation to define a Customization Class.
 *
 * <p>Next to the use of this annotation the Customization Class still needs to
 * be defined on the DataControl in the DataControl.dcx.</p>
 *
 * <p>Use the target attribute on the annotation to define the BeanClass of
 * the DataControl on which the customization needs to be done.</p>
 *
 * <p>Within your Customization Class you can define several annotations on methods.
 * These annotations will help you to customize the datacontrol both design time
 * and run time.</p>
 *
 * @see AttrValidation
 * @see CalculatedAttr
 * @see Created
 * @see ElementValidation
 * @see Operation
 * @see TransientAttr
 * @see PostAttrChange
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface ElementCustomization {
    String target();

    Class<? extends java.util.ResourceBundle> msgBundle() default java.util.ResourceBundle.class;
}
