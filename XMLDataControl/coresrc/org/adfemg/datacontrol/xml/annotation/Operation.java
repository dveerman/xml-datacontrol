package org.adfemg.datacontrol.xml.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Use this annotation to define a operation on the DataControl. The name
 * of the method will be exposed on the dataControl as well as the
 * returned values and any additional arguments to the annotated method.
 * <p>
 * The first method of the annotated method has to be the XMLDCElement of
 * the current row. Each additional method argument will be exposed as
 * an argument on the datacontrol method.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
@Documented
public @interface Operation {
}
