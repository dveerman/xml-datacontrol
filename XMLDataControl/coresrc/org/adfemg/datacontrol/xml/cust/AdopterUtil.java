package org.adfemg.datacontrol.xml.cust;

import java.util.List;

import oracle.adf.model.adapter.dataformat.AttributeDef;
import oracle.adf.model.adapter.dataformat.StructureDef;

import oracle.binding.meta.AttributeDefinition;
import oracle.binding.meta.StructureDefinition;

import org.adfemg.datacontrol.xml.annotation.AnnotationHelper;
import org.adfemg.datacontrol.xml.java.JClass;
import org.adfemg.datacontrol.xml.java.JClassFactory;
import org.adfemg.datacontrol.xml.java.JMethod;


/**
 * Utility Class for the Adopter classes that implement de Adopter.
 * @see Adopter
 */
public final class AdopterUtil {

    /**
     * TRUE constant for readability to make clear what the intent of the argument is this is used.
     */
    public static final boolean READONLY_TRUE = true;

    /**
     * FALSE constant for readability to make clear what the intent of the argument is this is used.
     */
    public static final boolean READONLY_FALSE = false;

    /**
     * FALSE constant for readability to make clear what the intent of the argument is this is used.
     */
    public static final boolean KEY_FALSE = false;

    /**
     * Private constructor to prevent instances.
     */
    private AdopterUtil() {
    }

    /**
     * Verify the signatue of a method based on its return-type and the types for the method arguments.
     * @param method the method for which the signature needs to be checked.
     * @param returnType the desired return type this method should return. If the method returns a subclass of this
     * type it is also deemed compatible.
     * @param argTypes The desired arguments, zero or more argument types. If the method accepts a supertype of these
     * types it is also deemed compatible.
     * @throws InvalidMethodSignatureException if the method signature does not match the requirements
     * @see Adopter#checkMethodSignature
     */
    public static void checkSignature(final JMethod method, final Class returnType,
                                      Class... argTypes) throws InvalidMethodSignatureException {
        JClass methodReturn = method.getReturnType();
        JClass neededReturn = JClassFactory.forClass(returnType.getName());
        // FIXME: When to check on isPrimitive?! Based on a new argument?!
        if (!neededReturn.isAssignableFrom(methodReturn) && !methodReturn.isPrimitive()) {
            throw new InvalidMethodSignatureException("method return for " + method.toGenericString() + " is not a " +
                                                      returnType);
        }
        List<JClass> methodParams = method.getParameterTypes();
        if (argTypes.length != methodParams.size()) {
            throw new InvalidMethodSignatureException("invalid number of arguments for method " +
                                                      method.toGenericString());
        }
        for (int i = 0; i < argTypes.length; i++) {
            JClass neededParamType = JClassFactory.forClass(argTypes[i].getName());
            if (!methodParams.get(i).isAssignableFrom(neededParamType)) {
                throw new InvalidMethodSignatureException("method argument " + i + " for " + method.toGenericString() +
                                                          " is not a " + argTypes[i]);
            }
        }
    }

    /**
     * Construct the name of a datacontrol structure attribute based on the name of a method by stripping a known
     * prefix and converting the first character to lowercase. For example stripping {@code get} from
     * {@code getFullName} would result in {@code fullName}
     * @param methodName the name of the java method to get the attribute from.
     * @param prefix to strip from the method name
     * @return the attribute name
     * @throws RuntimeException when the method name does not start with the prefix, which should not happen
     * if the Adopter used AdopterUtil.checkMethodStartsWith() during its checkMethodSignature()
     * @see AdopterUtil#checkMethodStartsWith
     */
    public static String getPrefixedAttributeName(final String methodName, final String prefix) {
        if (!methodName.startsWith(prefix)) {
            throw new RuntimeException("method name " + methodName + " should start with '" + prefix + "'");
        }
        int prefixLength = prefix.length();
        return methodName.substring(prefixLength, prefixLength + 1).toLowerCase() +
               methodName.substring(prefixLength + 1);
    }

    /**
     * Construct the name of a datacontrol structure attribute based on the name of a method by stripping a known
     * suffix. For example stripping {@code Changed} from {@code firstNameChanges} would result in {@code firstName}
     * @param methodName the name of the java method to get the attribute from.
     * @param suffix the suffix to strip from the method name
     * @return the attribute name
     * @throws RuntimeException when the method name does not end with the suffix, which should not happen
     * if the Adopter used AdopterUtil.checkMethodEndsWith() during its checkMethodSignature()
     * @see AdopterUtil#checkMethodEndsWith
     */
    public static String getSuffixedAttributeName(final String methodName, final String suffix) {
        if (!methodName.endsWith(suffix)) {
            throw new RuntimeException("method name " + methodName + " should end with '" + suffix + "'");
        }
        return methodName.substring(0, methodName.length() - suffix.length());
    }

    /**
     * Throw an InvalidMethodSignatureException if the given method name does not start with the given prefix.
     * @param method method to verify
     * @param prefix prefix the method name should start with
     * @throws InvalidMethodSignatureException
     */
    public static void checkMethodStartsWith(final JMethod method,
                                             final String prefix) throws InvalidMethodSignatureException {
        if (!method.getName().startsWith(prefix)) {
            throw new InvalidMethodSignatureException("Method name should begin with '" + prefix + "', method found: " +
                                                      method.toGenericString());
        }
    }

    /**
     * Throw an InvalidMethodSignatureException if the given method name does not end with the given suffix.
     * @param method method to verify
     * @param suffix suffix the method name should end with
     * @throws InvalidMethodSignatureException
     */
    public static void checkMethodEndsWith(final JMethod method,
                                           final String suffix) throws InvalidMethodSignatureException {
        if (!method.getName().endsWith(suffix)) {
            throw new InvalidMethodSignatureException("Method name should end with '" + suffix + "', method found: " +
                                                      method.toGenericString());
        }
    }

    /**
     * Throw an InvalidMethodSignatureException if the given method name does not exactly match the given name.
     * @param method method to verify
     * @param methodName the name the given method should have
     * @throws InvalidMethodSignatureException
     */
    public static void checkMethodEquals(final JMethod method,
                                         final String methodName) throws InvalidMethodSignatureException {
        if (!method.getName().equals(methodName)) {
            throw new InvalidMethodSignatureException("Method name should be '" + methodName + "', method found: " +
                                                      method.toGenericString());
        }
    }

    /**
     * Throw an InvalidMethodSignatureException if the given StructureDefinition does not have all of the requested
     * attributes. Can be used to verify an annotation like {@code @ElementValdation(attr={"firstName","lastName"})}
     * to make sure the annotated structure has these two attributes.
     * @param attrs the attribute names that have to be present in the StructureDefinition
     * @param structure the StructureDefinition to verify
     * @throws InvalidMethodSignatureException when one of the required attributes is not found in the structure
     */
    public static void checkExistingAttributes(String[] attrs,
                                               StructureDefinition structure) throws InvalidMethodSignatureException {
        if (!AnnotationHelper.isDefault(attrs)) {
            for (String attr : attrs) {
                if (structure.getAttributeDefinitions().find(attr) == null) {
                    throw new InvalidMethodSignatureException("Invalid attribute on annotation, attribute: " + attr +
                                                              " not foud.");
                }
            }
        }
    }

    /**
     * Utility class to add a attribute to the structure.
     * @param structure structure to add the new attribute to
     * @param type the javaType for the new attribute which is typically the return type of an annotated customization
     * method
     * @param attrName name of the new attribute.
     * @param isReadonly should the new attribute be readonly?
     * @param isKey should the new attribute be marked as key attribute?
     * @return the newly created attribute definition
     */
    public static AttributeDefinition addNewAttributeToStructure(StructureDef structure, JClass type,
                                                                 final String attrName, boolean isReadonly,
                                                                 boolean isKey) {
        // TODO: share code with StructureProvider or move this to some sort of generic factory
        final AttributeDef attrDef = new AttributeDef(attrName, structure, type.getName(), isReadonly, isKey);
        structure.addAttribute(attrDef);
        return attrDef;
    }

}
