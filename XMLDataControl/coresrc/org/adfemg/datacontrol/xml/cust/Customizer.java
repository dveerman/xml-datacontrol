package org.adfemg.datacontrol.xml.cust;

import java.lang.annotation.Annotation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import oracle.adf.model.adapter.AdapterException;
import oracle.adf.model.adapter.dataformat.MethodDef;
import oracle.adf.model.adapter.dataformat.MethodReturnDef;
import oracle.adf.model.adapter.dataformat.StructureDef;
import oracle.adf.share.logging.ADFLogger;

import oracle.binding.meta.AccessorDefinition;
import oracle.binding.meta.DefinitionContainer;
import oracle.binding.meta.OperationDefinition;
import oracle.binding.meta.OperationReturnDefinition;
import oracle.binding.meta.StructureDefinition;

import org.adfemg.datacontrol.xml.annotation.AttrValidation;
import org.adfemg.datacontrol.xml.annotation.CalculatedAttr;
import org.adfemg.datacontrol.xml.annotation.Created;
import org.adfemg.datacontrol.xml.annotation.ElementValidation;
import org.adfemg.datacontrol.xml.annotation.Operation;
import org.adfemg.datacontrol.xml.annotation.PostAttrChange;
import org.adfemg.datacontrol.xml.annotation.TransientAttr;
import org.adfemg.datacontrol.xml.design.DesignTimeUtils;
import org.adfemg.datacontrol.xml.design.MessageUtils;
import org.adfemg.datacontrol.xml.handler.Handler;
import org.adfemg.datacontrol.xml.handler.HandlerRegistry;
import org.adfemg.datacontrol.xml.java.JAnnotation;
import org.adfemg.datacontrol.xml.java.JClass;
import org.adfemg.datacontrol.xml.java.JClassFactory;
import org.adfemg.datacontrol.xml.java.JMethod;
import org.adfemg.datacontrol.xml.java.rt.RtAnnotation;
import org.adfemg.datacontrol.xml.java.rt.RtClass;
import org.adfemg.datacontrol.xml.java.rt.RtMethod;
import org.adfemg.datacontrol.xml.utils.LoggerUtils;


/**
 * In this class we customize the datacontrol based on a customization class created
 * in the client project.
 */
public class Customizer {

    /**
     * private map to keep track of the annotation types and their adopters.
     */
    private static final Map<Class<? extends Annotation>, Adopter<? extends Annotation>> adopters =
        new HashMap<Class<? extends Annotation>, Adopter<? extends Annotation>>() {
            {
                put(AttrValidation.class, new AttrValidationAdopter());
                put(CalculatedAttr.class, new CalculatedAttrAdopter());
                put(Created.class, new CreatedAdopter());
                put(ElementValidation.class, new ElementValidationAdopter());
                put(Operation.class, new OperationAdopter());
                put(PostAttrChange.class, new PostAttrChangeAdopter());
                put(TransientAttr.class, new TransientAttrAdopter());
            }
        };

    /**
     * customizers. key=target (datacontrol beanClass), value=collection of annotated customizer classes
     */
    private final Map<String, Collection<JClass>> custsPerTarget;
    private final Map<JClass, Object> instances;

    private Collection<JClass> unused = Collections.emptyList();

    private static final JClass ANNOTATION_JCLASS =
        JClassFactory.forClass(org.adfemg.datacontrol.xml.annotation.ElementCustomization.class.getName());
    private static final String ANNOTATION_ATTR = "target";

    private static final ADFLogger logger = ADFLogger.createADFLogger(Customizer.class);

    /**
     * The Constructor of the Customizer.
     * Initializes the Map of customizers based on the classes that are
     * provided in the DataContols.dcx.
     * @param custClasses the classes with the annotation ElementCustomization.
     */
    public Customizer(final Collection<JClass> custClasses) {
        // build map of customizers; key=target(datacontrol beanClass), value=collection of annotated customizer classes
        Map<String, Collection<JClass>> customizers = Collections.emptyMap();
        Map<JClass, Object> instances = Collections.emptyMap();
        if (custClasses != null) {
            customizers = new HashMap<String, Collection<JClass>>(custClasses.size());
            instances = new HashMap<JClass, Object>(custClasses.size());
            for (JClass customizer : custClasses) {
                JAnnotation annotation = customizer.getAnnotation(ANNOTATION_JCLASS);
                if (annotation == null) {
                    throw new IllegalStateException("Customizer " + customizer.getName() +
                                                    " doesn't have @ElementCustomization annotation");
                }
                final String target = (String) annotation.getElement(ANNOTATION_ATTR);
                if (!customizers.containsKey(target)) {
                    customizers.put(target, new ArrayList<JClass>());
                }
                customizers.get(target).add(customizer);
                if (!DesignTimeUtils.isDesignTime()) {
                    try {
                        instances.put(customizer, ((RtClass) customizer).newInstance());
                    } catch (IllegalAccessException e) {
                        throw new AdapterException(e);
                    } catch (InstantiationException e) {
                        throw new AdapterException(e);
                    }
                }
            }
            for (String key : customizers.keySet()) {
                customizers.put(key, Collections.unmodifiableCollection(customizers.get(key)));
            }
        }
        // keep map of customizers as unmodifiable object state
        this.custsPerTarget = Collections.unmodifiableMap(customizers);
        this.instances = Collections.unmodifiableMap(instances);
    }

    /**
     * Recursivly process all the posible customization on the structures that are reachable
     * through the accessors on the given structure.
     * @param accessors Accessors within a StructurDef.
     * @see StructureDef#getAccessorDefinitions
     */
    private void customizeAccessors(final DefinitionContainer accessors) {
        for (Iterator iterator = accessors.iterator(); iterator.hasNext();) {
            AccessorDefinition accessor = (AccessorDefinition) iterator.next();
            StructureDefinition struct = accessor.getStructure();
            if (struct instanceof StructureDef) {
                customize((StructureDef) struct);
            }
        }
    }

    /**
     * Process all posible customizations on the strucutres that are used as
     * method returns in the given structure.
     * @param operations Operatings within a StructureDef
     * @see StructureDef#getOperationDefinitions
     */
    private void customizeMethodReturns(final DefinitionContainer operations) {
        for (Iterator iterator = operations.iterator(); iterator.hasNext();) {
            OperationDefinition operation = (OperationDefinition) iterator.next();
            OperationReturnDefinition returnDef = operation.getOperationReturnType();
            if (returnDef instanceof MethodReturnDef) {
                MethodReturnDef mrd = (MethodReturnDef) returnDef;
                StructureDefinition definition = mrd.getStructure();
                if (definition instanceof StructureDef) {
                    customize((StructureDef) definition);
                }

            }
        }
    }

    /**
     * Entry point to the customizer class. Applies all customizations to the supplied MethodDef
     * and all its descendants.
     * @param method
     */
    public void customize(MethodDef method) {
        OperationReturnDefinition operReturn = method.getOperationReturnType();
        if (!(operReturn instanceof MethodReturnDef)) {
            return;
        }
        final StructureDefinition structDef = ((MethodReturnDef) operReturn).getStructure();
        if (structDef instanceof StructureDef) {
            // initially mark all customizers as not being used
            unused = new ArrayList<JClass>(custsPerTarget.size() * 2);
            for (Collection<JClass> custs : custsPerTarget.values()) {
                for (JClass cust : custs) {
                    unused.add(cust);
                }
            }
            // customize StructureDef (and all its children using recursion). This also removes any used
            // customization class from this.unused
            customize((StructureDef) structDef);
            // warn for any customizer that wasn't used
            final String prefix = structDef.getFullName();
            for (JClass cust : unused) {
                JAnnotation annotation = cust.getAnnotation(ANNOTATION_JCLASS);
                Object target = annotation.getElement(ANNOTATION_ATTR);
                if (target instanceof String && ((String) target).startsWith(prefix) && logger.isWarning()) {
                    logger.warning("unused customizer class {0} with target {1}", new Object[] {
                                   cust.getName(), target });
                }
            }
        }
    }

    /**
     * Process all the posible customizations on the given structure and nested structures.
     *
     * @param structure StructureDef
     */
    private void customize(final StructureDef structure) {
        if (custsPerTarget.isEmpty()) {
            // quick abort for common scenario with no customizers at all
            return;
        }

        final Collection<JClass> custs = custsPerTarget.get(structure.getFullName());
        if (custs != null && !custs.isEmpty()) {
            for (JClass cust : custs) {
                apply(cust, structure);
            }
        }
        // recursive customizations
        customizeAccessors(structure.getAccessorDefinitions());
        customizeMethodReturns(structure.getOperationDefinitions());
    }

    /**
     * Apply the customization class on the structure.
     *
     * @param customization The customization to apply.
     * @param structure The structure to apply the customization on.
     */
    private void apply(JClass customization, StructureDef structure) {
        unused.remove(customization);
        if (logger.isFinest()) {
            LoggerUtils.finest(logger, "applying customization {0} to {1}", customization, structure.getFullName());
        }
        for (JMethod method : customization.getMethods()) {
            // iterate all supported annotations and their adopters
            for (Map.Entry<Class<? extends Annotation>, Adopter<? extends Annotation>> entry : adopters.entrySet()) {
                Class<? extends Annotation> annoClass = entry.getKey();
                JClass annoJClass = JClassFactory.forClass(annoClass.getName());
                Adopter<Annotation> adopter = (Adopter<Annotation>) entry.getValue();

                final JAnnotation annotation = method.getAnnotation(annoJClass);
                if (annotation != null) {
                    try {
                        adopter.checkMethodSignature(method, annotation, structure);
                    } catch (InvalidMethodSignatureException e) {
                        if (DesignTimeUtils.isGraphicalDesignTime()) {
                            MessageUtils.showErrorMessage("Method signature error", e.getMessage());
                            continue;
                        } else {
                            throw new RuntimeException(e);
                        }
                    }
                    // adjust (design time) structure
                    adopter.adjustStructure(structure, annotation, method);
                    // create runtime handlers
                    if (!DesignTimeUtils.isDesignTime()) {
                        Collection<Handler> handlers =
                            adopter.createHandlers(((RtAnnotation) annotation).getRealAnnotation(),
                                                   ((RtMethod) method).getRealMethod(), instances.get(customization));
                        if (handlers != null && !handlers.isEmpty()) {
                            HandlerRegistry registryHelper = new HandlerRegistry(structure);
                            for (Handler handler : handlers) {
                                registryHelper.add(handler);
                            }
                        }
                    }
                }
            }
        }
    }

}
