package org.adfemg.datacontrol.xml.cust;

import java.lang.reflect.Method;

import java.util.Collection;
import java.util.Collections;

import oracle.adf.model.adapter.dataformat.StructureDef;

import org.adfemg.datacontrol.xml.annotation.PostAttrChange;
import org.adfemg.datacontrol.xml.events.AttrChangeEvent;
import org.adfemg.datacontrol.xml.handler.Handler;
import org.adfemg.datacontrol.xml.handler.PostPutHandler;
import org.adfemg.datacontrol.xml.java.JAnnotation;
import org.adfemg.datacontrol.xml.java.JMethod;
import org.adfemg.datacontrol.xml.utils.ClassUtils;


/**
 * The Adporter for the PostAttrChange annotation.
 *
 * @see Adopter
 * @see PostAttrChange
 */
public class PostAttrChangeAdopter implements Adopter<PostAttrChange> {

    /**
     * Check the method signature of the PostAttrChange method.
     * The method should start with the attribute name and end with 'Changed'.
     *
     * @inheritDoc
     */
    @Override
    public void checkMethodSignature(JMethod method, JAnnotation annotation,
                                     StructureDef structure) throws InvalidMethodSignatureException {
        AdopterUtil.checkSignature(method, Void.class, AttrChangeEvent.class);
        AdopterUtil.checkMethodEndsWith(method, "Changed");
    }

    @Override
    public void adjustStructure(StructureDef structure, JAnnotation annotation, JMethod method) {
    }

    /**
     * Creates a PostPutHandler that fires the method created in the
     * customization class an AttrChangeEvent as arguments.
     *
     * @see PostPutHandler
     * @see AttrChangeEvent
     * @inheritDoc
     */
    @Override
    public Collection<Handler> createHandlers(final PostAttrChange annotation, final Method method,
                                              final Object customizer) {
        PostPutHandler handler = new PostPutHandler() {
            final String attrName = getAttributeName(method.getName());

            @Override
            public void doPostPut(AttrChangeEvent attrChangeEvent) {
                ClassUtils.invokeMethod(customizer, method, attrChangeEvent);
            }

            @Override
            public boolean handlesAttribute(String input) {
                return attrName.equals(input);
            }
        };
        return Collections.<Handler>singletonList(handler);
    }

    private String getAttributeName(String methodName) {
        return AdopterUtil.getSuffixedAttributeName(methodName, "Changed");
    }
}
