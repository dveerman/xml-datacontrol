package org.adfemg.datacontrol.xml.cust;

import java.lang.reflect.Method;

import java.util.Collection;
import java.util.Collections;

import oracle.adf.model.adapter.dataformat.StructureDef;

import org.adfemg.datacontrol.xml.annotation.AttrValidation;
import org.adfemg.datacontrol.xml.events.AttrChangeEvent;
import org.adfemg.datacontrol.xml.handler.Handler;
import org.adfemg.datacontrol.xml.handler.PrePutHandler;
import org.adfemg.datacontrol.xml.java.JAnnotation;
import org.adfemg.datacontrol.xml.java.JMethod;
import org.adfemg.datacontrol.xml.utils.ClassUtils;


/**
 * The Adporter for the AttrValidation annotation.
 *
 * @see Adopter
 * @see AttrValidation
 */
public class AttrValidationAdopter implements Adopter<AttrValidation> {

    /**
     * Check the method signature of the AttrValidation method.
     * The method should start with 'validate' followed with the attribute name.
     *
     * @inheritDoc
     */
    @Override
    public void checkMethodSignature(JMethod method, JAnnotation annotation,
                                     StructureDef structure) throws InvalidMethodSignatureException {
        AdopterUtil.checkSignature(method, Void.class, AttrChangeEvent.class);
        AdopterUtil.checkMethodStartsWith(method, "validate");
    }

    /**
     * @inheritDoc
     */
    @Override
    public void adjustStructure(StructureDef structure, JAnnotation annotation, JMethod method) {
    }

    /**
     * Creates a PrePutHandler that fires the method created in the
     * customization class with the old and new value as arguments.
     *
     * @see PrePutHandler
     * @inheritDoc
     */
    @Override
    public Collection<Handler> createHandlers(final AttrValidation annotation, final Method method,
                                              final Object customizer) {
        final String attrName = getAttributeName(method.getName());
        PrePutHandler handler = new PrePutHandler() {
            @Override
            public void doPrePut(AttrChangeEvent attrChangeEvent) {
                ClassUtils.invokeMethod(customizer, method, attrChangeEvent);
            }

            @Override
            public boolean handlesAttribute(String input) {
                return attrName.equals(input);
            }
        };
        return Collections.<Handler>singletonList(handler);
    }

    /**
     * Utility method to get the Attribute name from the method signature.
     *
     * @param method the method to get the attribute name from.
     * @return the attribute name.
     */
    private String getAttributeName(String methodName) {
        return AdopterUtil.getPrefixedAttributeName(methodName, "validate");
    }
}
