package org.adfemg.datacontrol.xml.cust;

import java.lang.reflect.Method;

import java.util.Collection;
import java.util.Collections;

import oracle.adf.model.adapter.dataformat.StructureDef;

import org.adfemg.datacontrol.xml.DataControl;
import org.adfemg.datacontrol.xml.annotation.AnnotationHelper;
import org.adfemg.datacontrol.xml.annotation.ElementValidation;
import org.adfemg.datacontrol.xml.data.XMLDCElement;
import org.adfemg.datacontrol.xml.events.AttrChangeEvent;
import org.adfemg.datacontrol.xml.events.ValidationListener;
import org.adfemg.datacontrol.xml.handler.Handler;
import org.adfemg.datacontrol.xml.handler.PostPutHandler;
import org.adfemg.datacontrol.xml.java.JAnnotation;
import org.adfemg.datacontrol.xml.java.JMethod;
import org.adfemg.datacontrol.xml.utils.ClassUtils;


/**
 * The Adporter for the ElementValidation annotation.
 *
 * @see Adopter
 * @see ElementValidation
 */
public class ElementValidationAdopter implements Adopter<ElementValidation> {

    /**
     * @inheritDoc
     */
    @Override
    public void checkMethodSignature(JMethod method, JAnnotation annotation,
                                     StructureDef structure) throws InvalidMethodSignatureException {
        AdopterUtil.checkSignature(method, Void.class, XMLDCElement.class);
        Object attrValue = annotation.getElement("attr");
        String[] attrs;
        if (attrValue == null) {
            attrs = new String[0];
        }
        if (attrValue instanceof Object[]) {
            // design time annotations return Object[]
            Object[] objects = (Object[]) attrValue;
            attrs = new String[objects.length];
            for (int i = 0, n = objects.length; i < n; i++) {
                attrs[i] = objects[i] == null ? null : objects[i].toString();
            }
        } else if (attrValue instanceof String[]) {
            // runtime annotations return String[]
            attrs = (String[]) attrValue;
        } else {
            throw new InvalidMethodSignatureException("unexpected attr type: " + attrValue);
        }
        AdopterUtil.checkExistingAttributes(attrs, structure);
    }

    @Override
    public void adjustStructure(StructureDef structure, JAnnotation annotation, JMethod method) {
    }

    /**
     * Creates a PostPutHandler that adds a validation listener to the datacontrol.
     *
     * @see PostPutHandler
     * @inheritDoc
     */
    @Override
    public Collection<Handler> createHandlers(final ElementValidation annotation, final Method method,
                                              final Object customizer) {
        PostPutHandler handler = new PostPutHandler() {
            @Override
            public boolean handlesAttribute(String attrName) {
                // handle this attribute if annotation had no attribute-names
                // specified (so validation should occur on any attribute change)
                // or when the attribute being changed is part of the list of
                // attribute names in the annotation
                if (AnnotationHelper.isDefault(annotation.attr())) {
                    return true;
                }
                for (String annoAttr : annotation.attr()) {
                    if (attrName.equals(annoAttr)) {
                        return true;
                    }
                }
                return false;
            }

            @Override
            public void doPostPut(AttrChangeEvent attrChangeEvent) {
                // add validation listener to invoke real validation when dataControl
                // initiates validation
                DataControl dc = attrChangeEvent.getElement().getDataControl();
                dc.addValidationListener(new ElementValidator(attrChangeEvent.getElement(), customizer, method));
            }

        };
        return Collections.<Handler>singletonList(handler);
    }

    private static final class ElementValidator implements ValidationListener {
        private final XMLDCElement element;
        private final Object customizer;
        private final Method method;

        public ElementValidator(final XMLDCElement element, final Object customizer, final Method method) {
            this.element = element;
            this.customizer = customizer;
            this.method = method;
        }

        @Override
        public void validate() {
            ClassUtils.invokeMethod(customizer, method, element);
        }
    }

}
