package org.adfemg.datacontrol.xml.cust;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import oracle.adf.model.adapter.dataformat.StructureDef;

import org.adfemg.datacontrol.xml.annotation.TransientAttr;
import org.adfemg.datacontrol.xml.data.XMLDCElement;
import org.adfemg.datacontrol.xml.events.AttrChangeEvent;
import org.adfemg.datacontrol.xml.handler.Handler;
import org.adfemg.datacontrol.xml.handler.InsteadGetHandler;
import org.adfemg.datacontrol.xml.handler.InsteadPutHandler;
import org.adfemg.datacontrol.xml.java.JAnnotation;
import org.adfemg.datacontrol.xml.java.JMethod;
import org.adfemg.datacontrol.xml.utils.ClassUtils;


/**
 * The Adporter for the TransientAttr annotation.
 *
 * @see Adopter
 * @see TransientAttr
 */
public class TransientAttrAdopter implements Adopter<TransientAttr> {

    /**
     * Check the method signature of the TransientAttr method.
     * The method should start with 'init' followed with the attribute name.
     *
     * @inheritDoc
     */
    @Override
    public void checkMethodSignature(JMethod method, JAnnotation annotation,
                                     StructureDef structure) throws InvalidMethodSignatureException {
        AdopterUtil.checkSignature(method, Object.class, XMLDCElement.class);
        AdopterUtil.checkMethodStartsWith(method, "init");
    }

    /**
     * Method to adjust the structure of the DataControl.
     * We add the transient attribute as updateable attribute to the current structure.
     * The name of the attribute is extracted from the method name in the Customization Class.
     *
     * @inheritDoc
     */
    @Override
    public void adjustStructure(StructureDef structure, JAnnotation annotation,
                                JMethod method) {
        // attribuut toevoegen aan StructureDef
        AdopterUtil.addNewAttributeToStructure(structure, method.getReturnType(), getAttributeName(method.getName()),
                                               AdopterUtil.READONLY_FALSE, AdopterUtil.KEY_FALSE);
    }

    /**
     * Creates a ValueHolder and puts this ValueHolder on both the Handlers:
     *  - TransientInsteadPut
     *  - TransientInsteadGet
     *
     * The ValueHolder is used to remember the value that is set with the
     * TransientInsteadSet handler, this handler is fired instead of the normal put.
     * The TransientInsteadGet handler reads the value from the ValueHolder, this
     * handler is fired instead of the normal get.
     *
     * @inheritDoc
     */
    @Override
    public Collection<Handler> createHandlers(TransientAttr annotation, final Method method, final Object customizer) {
        final String attrName = getAttributeName(method.getName());
        final String propKey = "$$transient$$" + attrName;

        InsteadPutHandler putHandler = new InsteadPutHandler() {
            @Override
            public Object doInsteadPut(AttrChangeEvent attrChangeEvent) {
                TransientAttrAdopter.ValueHolder holder =
                    new TransientAttrAdopter.ValueHolder<Serializable>((Serializable) attrChangeEvent.getNewValue());
                XMLDCElement element = attrChangeEvent.getElement();
                Map<String, Serializable> props = element.getProperties();
                props.put(propKey, holder);
                return attrChangeEvent.getOldValue();
            }

            @Override
            public boolean handlesAttribute(String input) {
                return attrName.equals(input);
            }
        };
        InsteadGetHandler getHandler = new InsteadGetHandler() {
            @Override
            public Object doInsteadGet(XMLDCElement element) {
                Map<String, Serializable> props = element.getProperties();
                if (!props.containsKey(propKey)) {
                    // no transient value known yet, invoke annotated method
                    Object value = ClassUtils.invokeMethod(customizer, method, element);
                    TransientAttrAdopter.ValueHolder holder =
                        new TransientAttrAdopter.ValueHolder<Serializable>((Serializable) value);
                    props.put(propKey, holder);
                }
                return ((TransientAttrAdopter.ValueHolder) props.get(propKey)).getValue();
            }

            @Override
            public boolean handlesAttribute(String input) {
                return attrName.equals(input);
            }
        };
        return Arrays.<Handler>asList(putHandler, getHandler);
    }

    /**
     * Utility method to get the Attribute name from the method signature.
     *
     * @param method the method to get the attribute name from.
     * @return the attribute name.
     */
    private String getAttributeName(String methodName) {
        return AdopterUtil.getPrefixedAttributeName(methodName, "init");
    }

    /**
     * The ValueHolder class for the Transient attribute which can contain null values.
     *
     * @param <T> The type of attribute.
     */
    private static final class ValueHolder<T extends Serializable> implements Serializable {
        @SuppressWarnings("compatibility:-1313423557148604507")
        private static final long serialVersionUID = 1L;

        private final T value;

        public ValueHolder(T value) {
            this.value = value;
        }

        public T getValue() {
            return value;
        }
    }

}
