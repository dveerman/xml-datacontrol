package org.adfemg.datacontrol.xml.cust;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import oracle.adf.model.adapter.dataformat.MethodDef;
import oracle.adf.model.adapter.dataformat.StructureDef;

import org.adfemg.datacontrol.xml.annotation.Operation;
import org.adfemg.datacontrol.xml.data.XMLDCElement;
import org.adfemg.datacontrol.xml.handler.Handler;
import org.adfemg.datacontrol.xml.handler.OperationHandler;
import org.adfemg.datacontrol.xml.java.JAnnotation;
import org.adfemg.datacontrol.xml.java.JClass;
import org.adfemg.datacontrol.xml.java.JClassFactory;
import org.adfemg.datacontrol.xml.java.JMethod;
import org.adfemg.datacontrol.xml.utils.ClassUtils;


/**
 * The Adporter for the Operation annotation.
 *
 * @see Adopter
 * @see Operation
 */
public class OperationAdopter implements Adopter<Operation> {

    /**
     * Check the method signature of the Operation.
     * We check if the first argument to the method is an XMLDCElement.
     *
     * @see XMLDCElement
     * @inheritDoc
     */
    @Override
    public void checkMethodSignature(JMethod method, JAnnotation annotation,
                                     StructureDef structure) throws InvalidMethodSignatureException {
        List<JClass> args = method.getParameterTypes();
        JClass xmldce = JClassFactory.forClass(XMLDCElement.class.getName());
        if (args == null || args.size() == 0 || !xmldce.isAssignableFrom(args.get(0))) {
            throw new InvalidMethodSignatureException("first method argument for method " + method.toGenericString() +
                                                      " should be an XMLDCElement");
        }
    }

    /**
     * Adjusts the structure of the datacontrol and add a methodDef to it.
     *
     * We loop over the parameters of the method and add these to the new methodDef.
     * At the time being, we do not discover the real parameter name so we use the
     * name 'arg' concatenated with a number.
     *
     * If the returntype is void, we do not set a returntype.
     *
     * @see MethodDef
     * @inheritDoc
     */
    @Override
    public void adjustStructure(StructureDef structure, JAnnotation annotation, final JMethod method) {
        final String methodName = method.getName();
        final MethodDef methodDef = new MethodDef(methodName, structure);
        List<JClass> params = method.getParameterTypes();
        for (int i = 1, n = params.size(); i < n; i++) { // do not expose first (XMLDCElement) parameter in datacontrol structure
            // FIXME: would be nice if we can discover the parameter names. Can probably be done with new DtClass
            // approach. However OperationHandler needs to be changes as well as that works with argument-names which
            // we can't determine at runtime (pure reflection), so OperationHandler should only work with arg-types
            methodDef.addParameter(argName(i - 1), params.get(i).getName());
            i++;
        }
        JClass returnType = method.getReturnType();
        methodDef.setReturnType(returnType.getName());
        structure.addMethod(methodDef);
    }

    /**
     * Static method to create an argument name based a int.
     * We use the prefix 'arg' and concatenate this with the index.
     *
     * @param index the number of the argument.
     * @return the name of the argument.
     */
    static String argName(int index) {
        return "arg" + index;
    }

    /**
     * Creates a OperationHandler that fires the method created in the
     * customization class with the XMLDCElement as first arguments.
     *
     * @see OperationHandler
     * @see XMLDCElement
     * @inheritDoc
     */
    @Override
    public Collection<Handler> createHandlers(final Operation annotation, final Method method,
                                              final Object customizer) {
        OperationHandler handler = new OperationHandler() {
            /**
             * We add the XMLDCElement as first element to the List of arguments.
             * After that we add all the arguments from the map.
             */
            @Override
            public Object invokeOperation(XMLDCElement element, Map arguments) {
                List<Object> argVals = getArgumentValues(arguments);
                List<Object> allArgs = new ArrayList<Object>(argVals.size() + 1);
                allArgs.add(element); // first arg is always XMLDCElement
                allArgs.addAll(argVals); // add all other argument values
                Object retval = ClassUtils.invokeMethod(customizer, method, allArgs.toArray());
                return retval;
            }

            @Override
            public boolean handlesOperation(String name, Map arguments) {
                boolean retval = method.getName().equals(name);
                Class<?>[] paramTypes = method.getParameterTypes();
                // we ignore first method argument as that is always XMLDCElement
                if (paramTypes.length != arguments.size() + 1) {
                    return false;
                }
                int i = 0;
                for (Object arg : getArgumentValues(arguments)) {
                    Class<?> paramType = paramTypes[i + 1];
                    if (paramType.isPrimitive()) {
                        // FIXME: check if arg can be unboxed to required primitive type
                        // for now we just assume it is
                        // retval = retval && (paramType.);
                    } else {
                        retval = retval && (paramType.isInstance(arg));
                    }
                    i++;
                }
                return retval;
            }

            private List<Object> getArgumentValues(Map args) {
                // return sorted list of argument values
                int size = args.size();
                List<Object> retval = new ArrayList<Object>(size);
                for (int i = 0; i < size; i++) {
                    retval.add(args.get(argName(i)));
                }
                return retval;
            }

        };

        return Collections.<Handler>singletonList(handler);
    }
}
