package org.adfemg.datacontrol.xml.handler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.adfemg.datacontrol.xml.data.XMLDCElement;
import org.adfemg.datacontrol.xml.events.AttrChangeEvent;

import oracle.adf.model.adapter.dataformat.StructureDef;


/**
 * A registry of handlers to a structure.
 *
 * @see Handler
 * @see StructureDef
 * @see XMLDCElement
 */
public class HandlerRegistry {
    /**
     * key in properties van StructureDef waar de customization handlers
     * zijn opgeslagen.
     */
    private static final String KEY_HANDLERS = "__xmldc_handlers_";

    /**
     * The reference to the structureDef.
     */
    private final StructureDef structure;

    /**
     * Public constructure to create a HandlerRegistry.
     * @param structure The StructureDef to register the handlers to.
     */
    public HandlerRegistry(StructureDef structure) {
        super();
        this.structure = structure;
    }

    /**
     * Adds a handler to the list of handlers on the structure.
     * If this is the first handler to add to the structure we initialize
     * a new Collection of handlers.
     * This should only be invoked when building the datacontrol structure. At runtime,
     * XMLDCElement and XMLDCCollection should only retrieve the handlers and not register new ones.
     * TODO: split into get-only (read-only) HandlerRegistry and read-write one for Customizer
     * @param handler the Handler to add to the structure.
     */
    public void add(final Handler handler) {
        Collection<Handler> handlers = (Collection<Handler>) structure.getProperty(KEY_HANDLERS);
        if (handlers == null) {
            synchronized (structure) {
                // recheck if other thread created handlers while we were waiting for lock
                handlers = (Collection<Handler>) structure.getProperty(KEY_HANDLERS);
                if (handlers == null) {
                    handlers = new ArrayList<Handler>(2);
                    structure.addProperty(KEY_HANDLERS, handlers);
                }
            }
        }
        handlers.add(handler);
    }

    /**
     * Get all the AttributeHandler availeable on the structure for a specific
     * attribute and of a specific handlerType.
     *
     * @param <T>
     * @param handlerType The handlerType to check for.
     * @param attrName The name of the attrbute.
     * @return A collection of AttributeHandlers.
     * @see AttributeHandler
     */
    private <T extends AttributeHandler> Collection<T> getAttributeHandlers(final Class<T> handlerType,
                                                                            final String attrName) {
        Collection<T> handlers = getHandlers(handlerType);
        if (handlers.isEmpty()) {
            return Collections.emptyList();
        }
        Collection<T> retval = new ArrayList<T>();
        for (Handler handler : handlers) { //Only add the handler if it handles the attribute from the argument.
            if (((AttributeHandler) handler).handlesAttribute(attrName)) {
                retval.add((T) handler);
            }
        }
        return retval;
    }

    /**
     * Get all the handlers of a specific handlerType registered to the structure.
     *
     * @param <T>
     * @param handlerType The handlerType to check for.
     * @return A collection of Handlers.
     * @see Handler
     */
    public <T extends Handler> Collection<T> getHandlers(final Class<T> handlerType) {
        Collection<Handler> handlers = (Collection<Handler>) structure.getProperty(KEY_HANDLERS);
        if (handlers == null || handlers.isEmpty()) {
            return Collections.emptyList();
        }
        Collection<T> retval = new ArrayList<T>();
        for (Handler handler : handlers) { //Only add the correct HandlerType handlers:
            if (handlerType.isInstance(handler)) {
                retval.add((T) handler);
            }
        }
        return retval;
    }

    /**
     * Invokes all the PrePut handlers for the current structure.
     *
     * @param attrChangeEvent The AttrChangeEvent that triggered the put.
     */
    public void invokePrePut(AttrChangeEvent attrChangeEvent) {
        for (PrePutHandler handler : getAttributeHandlers(PrePutHandler.class, attrChangeEvent.getAttribute())) {
            handler.doPrePut(attrChangeEvent);
        }
    }

    /**
     * Invokes the InsteadGet handler for the attribute on the current structure.
     *
     * @param element The current XMLDCElement.
     * @param attributeName The name of the attribute we're getting.
     * @return The result of the insteadGet operation.
     */
    public Object invokeInsteadGet(final XMLDCElement element, final String attributeName) {
        InsteadGetHandler getter = getAttributeHandlers(InsteadGetHandler.class, attributeName).iterator().next();
        return getter.doInsteadGet(element);
    }

    /**
     * Invokes the InsteadPut handler for the attribute on the current structure.
     *
     * @param attrChangeEvent The AttrChangeEvent that triggered the put.
     * @param attributeName The name of the attribute we're putting.
     * @return The result of the insteadPut operation, should be the oldValue.
     */
    public Object invokeInsteadPut(AttrChangeEvent attrChangeEvent, String attributeName) {
        InsteadPutHandler putter = getAttributeHandlers(InsteadPutHandler.class, attributeName).iterator().next();
        return putter.doInsteadPut(attrChangeEvent);
    }

    /**
     * Will get the PostPut handlers known for the attributeName.
     * Fires off the PostPut action with the attrChangeEvent.
     *
     * @param attrChangeEvent the attrChangeEvent that triggered the put.
     * @param attributeName the attribute name.
     */
    public void invokePostPut(AttrChangeEvent attrChangeEvent, String attributeName) {
        Collection<PostPutHandler> putters = getAttributeHandlers(PostPutHandler.class, attributeName);

        for (PostPutHandler putter : putters) {
            putter.doPostPut(attrChangeEvent);
        }
    }

    /**
     * Will get the Created handlers known for the element.
     *
     * @param element The XMLDCElement that is created.
     */
    public void invokeCreated(final XMLDCElement element) {
        Collection<CreatedHandler> creates = getHandlers(CreatedHandler.class);

        for (CreatedHandler create : creates) {
            create.invokeCreated(element);
        }
    }

    /**
     * Indicates whether this HandlerRegistry has at least one handler for the
     * requested type for the requested attribute. Can be used to determine if
     * instead-of handlers exist and should be invoked instead of default handling.
     *
     * @param handlerType type of handler to look for
     * @param attributeName name of the attribute
     * @return {@code true} if at least one handler of the requested type
     *         is registered for the requested attribute, otherwise
     *         {@code false}
     */
    public boolean hasHandler(Class<? extends AttributeHandler> handlerType, final String attributeName) {
        return !getAttributeHandlers(handlerType, attributeName).isEmpty();
    }

    /**
     * Indicates whether this HandlerRegistry has at least one handler for the
     * requested type.
     *
     * @param handlerType type of handler to look for
     * @return {@code true} if at least one handler of the requested type
     *         is registered, otherwise {@code false}
     */
    public boolean hasHandler(Class<? extends ElementHandler> handlerType) {
        return !getHandlers(handlerType).isEmpty();
    }
}
