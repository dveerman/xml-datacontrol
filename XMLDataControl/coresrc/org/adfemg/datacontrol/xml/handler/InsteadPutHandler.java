package org.adfemg.datacontrol.xml.handler;

import org.adfemg.datacontrol.xml.events.AttrChangeEvent;

/**
 * This handler is the hook for an insteadPut.
 * Meaning the actual put will not be fired, but we'll use the alternative put.
 */
public interface InsteadPutHandler extends AttributeHandler {
    /**
     * Implement the logic for this handler in this method.
     *
     * @param attrChangeEvent The AttrChangeEvent that triggered the put.
     * @return The result of the insteadPut operation, should be the oldValue.
     */
    public Object doInsteadPut(AttrChangeEvent attrChangeEvent);
}
