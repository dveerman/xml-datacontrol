package org.adfemg.datacontrol.xml.handler;

import org.adfemg.datacontrol.xml.data.XMLDCElement;

/**
 * This handler is the hook for an insteadGet.
 * Meaning the actual get will not be fired, but we'll use the alternative get.
 */
public interface InsteadGetHandler extends AttributeHandler {
    /**
     * Implement the logic for this handler in this method.
     *
     * @param element The XMLDCElement.
     * @return The result of the insteadGet operation.
     */
    public Object doInsteadGet(XMLDCElement element);
}
