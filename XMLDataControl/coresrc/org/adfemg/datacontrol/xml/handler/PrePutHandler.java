package org.adfemg.datacontrol.xml.handler;

import org.adfemg.datacontrol.xml.events.AttrChangeEvent;

/**
 * This handler is a hook for the prePut.
 * Meaning this logic will be invoked before the actual put has been done
 * on the element.
 */
public interface PrePutHandler extends AttributeHandler {
    /**
     * Implement your logic in this method.
     *
     * @param attrChangeEvent The AttrChangeEvent that triggered the put.
     */
    public void doPrePut(AttrChangeEvent attrChangeEvent);
}
