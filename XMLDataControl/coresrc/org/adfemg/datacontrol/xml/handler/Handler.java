package org.adfemg.datacontrol.xml.handler;

/**
 * Defines the interface for all the Handlers that can be registered
 * in the HandlerRegistry.
 *
 * The Handlers are hooks in the framework to be used by the annotation framework.
 *
 * @see HandlerRegistry
 */
public interface Handler {
}
