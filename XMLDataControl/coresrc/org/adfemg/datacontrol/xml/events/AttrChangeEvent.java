package org.adfemg.datacontrol.xml.events;

import org.adfemg.datacontrol.xml.data.XMLDCElement;

/**
 * AttrChangeEvent that holds the:
 *  - Old value
 *  - New value
 *  - Name of the attribute
 *  - XMLDCElement
 * @param <T> Type of value.
 */
public class AttrChangeEvent<T> {
    private T oldValue;
    private T newValue;
    private String attribute;
    private XMLDCElement element;

    public AttrChangeEvent() {
        super();
    }

    /**
     * Constructor to create an AttrChangeEvent with all the fields.
     *
     * @param oldValue The old value.
     * @param newValue the new value.
     * @param attribute the attribute name.
     * @param element the XMLDCElement.
     */
    public AttrChangeEvent(T oldValue, T newValue, String attribute, XMLDCElement element) {
        super();
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.attribute = attribute;
        this.element = element;
    }

    public T getOldValue() {
        return oldValue;
    }

    public String getAttribute() {
        return attribute;
    }

    public T getNewValue() {
        return newValue;
    }

    public XMLDCElement getElement() {
        return element;
    }
}
