package org.adfemg.datacontrol.xml.events;

import java.util.EventListener;

/**
 * Interface for DataChangeListeners.
 *
 * Use this listener to register a data change to the DataControl.
 *
 * @see EventListener
 */
public interface DataChangeListener extends EventListener {
    void dataChanged(DataChangeEvent event);
}
