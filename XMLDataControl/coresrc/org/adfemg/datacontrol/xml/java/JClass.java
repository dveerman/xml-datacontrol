package org.adfemg.datacontrol.xml.java;

import java.util.Collection;

/**
 * Represents a single Java class.
 */
public interface JClass {

    /**
     * Get the fully qualified name of the java class including package declaration.
     * @return for example {@code com.example.application.MyClass}
     */
    public String getName();

    /**
     * Get all declared methods for this class.
     * @return all declared methods whether they are private, public or any other access modifier
     */
    public Collection<JMethod> getMethods();

    /**
     * Determines if the specified Class object represents a primitive type. There are nine predefined Class objects to
     * represent the eight primitive types and void. These have the same names as the primitive types that they
     * represent, namely {@code boolean}, {@code byte}, {@code char}, {@code short}, {@code int}, {@code long},
     * {@code float}, and {@code double}.
     * @return true if and only if this class represents a primitive type
     */
    public boolean isPrimitive();

    /**
     * Determines if the class or interface represented by this Class object is either the same as, or is a superclass
     * or superinterface of, the class or interface represented by the specified Class parameter. It returns true if
     * so; otherwise it returns false. If this Class object represents a primitive type, this method returns true if the
     * specified Class parameter is exactly this Class object; otherwise it returns false.
     * <p>for example JClassFactory("java.util.Map").isAssignableFrom(JClassFactory("java.util.HashMap")) returns
     * true
     * @param subject the Class object to be checked
     * @return the boolean value indicating whether objects of the type {@code subject} can be assigned to objects of
     * this class
     */
    public boolean isAssignableFrom(JClass subject);

    /**
     * Returns the annotation that is present on this element for the specified annotation type or any of its
     * superclasses or superinterfaces, else null.
     * @param annotationClass the Class object corresponding to the annotation type
     * @return this element's annotation for the specified annotation type if present on this element, else null
     */
    public JAnnotation getAnnotation(JClass annotationClass);

    /**
     * Returns all annotations that are present on this element or any of its superclasses or superinterfaces,
     * else null.
     * @return this element's annotations if present on this element, else an empty list
     */
    public Collection<JAnnotation> getAnnotations();

}
