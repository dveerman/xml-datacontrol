package org.adfemg.datacontrol.xml.java.rt;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import oracle.adf.model.adapter.AdapterException;

import org.adfemg.datacontrol.xml.java.JAnnotation;

/**
 * Implementation of JAnnotation that wraps the native Java reflection API java.lang.annotation.Annotation.
 */
public class RtAnnotation implements JAnnotation {

    private final Annotation annotation;

    /**
     * Constructor.
     * @param annotation Java annotation being represented.
     */
    public RtAnnotation(final Annotation annotation) {
        this.annotation = annotation;
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.tag-is-missing", "oracle.jdeveloper.java.description-is-empty",
                      "oracle.jdeveloper.java.tag-is-misplaced" })
    public Object getElement(String name) {
        try {
            Method method = annotation.annotationType().getMethod(name);
            return method.invoke(annotation);
        } catch (NoSuchMethodException e) {
            throw new AdapterException(e);
        } catch (InvocationTargetException e) {
            throw new AdapterException(e);
        } catch (IllegalAccessException e) {
            throw new AdapterException(e);
        }
    }

    /**
     * Returns the java.lang.annotation.Annotation wrapped in this JAnnotation for situations where users want to
     * break the design-time/runtime abstraction from the JAnnotation interfaces and are willing to downcast to
     * RtAnnotation and get the wrapped Annotation, for example to use further reflection.
     * @return java.lang.annotation.Annotation
     */
    public Annotation getRealAnnotation() {
        return annotation;
    }

    /**
     * compares this annotation to another annotation.
     * @param object other annotation to compare to
     * @return <tt>true</tt> if given object is a RtAnnotation representing where
     * this.getRealAnnotation().equals(object.getRealAnnotation())
     */
    @Override
    public boolean equals(Object object) {
        return object instanceof RtAnnotation && ((RtAnnotation) object).getRealAnnotation().equals(annotation);
    }

    /**
     * hashCode of the java.lang.annotation.Annotation in this RtAnnotation.
     * @return this.getRealAnnotation().hashCode()
     */
    @Override
    public int hashCode() {
        return annotation.hashCode();
    }

}
