package org.adfemg.datacontrol.xml.java.rt;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;

import org.adfemg.datacontrol.xml.java.JAnnotation;
import org.adfemg.datacontrol.xml.java.JClass;
import org.adfemg.datacontrol.xml.java.JMethod;

/**
 * Implementation of JMethod that wraps the native Java reflection API java.lang.reflect.Method.
 */
public class RtMethod implements JMethod {

    private final Method method;

    /**
     * Constructor.
     * @param method Java method being represented.
     */
    public RtMethod(final Method method) {
        this.method = method;
    }

    /**
     * Gets the name of the java method.
     * @return name of the method
     * @see Method#getName
     */
    @Override
    public String getName() {
        return method.getName();
    }

    /**
     * Gets the ordered list of parameters/arguments to this method.
     * @return list of parameter types
     * @see Method#getParameterTypes
     */
    @Override
    public List<JClass> getParameterTypes() {
        Class<?>[] paramTypes = method.getParameterTypes();
        List<JClass> retval = new ArrayList<JClass>(paramTypes.length);
        for (Class<?> pt : paramTypes) {
            retval.add(new RtClass<Object>(pt));
        }
        return retval;
    }

    /**
     * Gets the return type of this method.
     * @return retun type of this method which could be "void", one of the primitives ("boolean", "int", etc)
     * or a true java class like java.lang.String
     */
    @Override
    public JClass getReturnType() {
        return new RtClass<Object>(method.getReturnType());
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public JAnnotation getAnnotation(JClass annotationClass) {
        for (Annotation a : method.getAnnotations()) {
            if (a.annotationType().getName().equals(annotationClass.getName())) {
                return new RtAnnotation(a);
            }
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public String toGenericString() {
        return method.toGenericString();
    }

    /**
     * Returns the java.lang.reflect.Method wrapped in this JMethod for situations where users want to
     * break the design-time/runtime abstraction from the JClass interfaces and are willing to downcast to
     * RtMethod and get the wrapped Method, for example to invoke it through reflection.
     * @return java.lang.reflect.Method
     */
    public Method getRealMethod() {
        return method;
    }

    /**
     * compares this method to another method.
     * @param object other method to compare to
     * @return <tt>true</tt> if given object is a RtMethod representing where
     * this.getRealMethod().equals(object.getRealMethod())
     */
    @Override
    public boolean equals(Object object) {
        return object instanceof RtMethod && ((RtMethod) object).method.equals(method);
    }

    /**
     * hashCode of the java.lang.reflect.Method in this RtMethod.
     * @return this.getRealMethod().hashCode()
     */
    @Override
    public int hashCode() {
        return method.hashCode();
    }

}
