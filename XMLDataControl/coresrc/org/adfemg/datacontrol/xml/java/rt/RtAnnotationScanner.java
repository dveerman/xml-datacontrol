package org.adfemg.datacontrol.xml.java.rt;

import java.io.InputStream;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletContext;

import oracle.adf.share.ADFContext;
import oracle.adf.share.Environment;
import oracle.adf.share.logging.ADFLogger;

import org.adfemg.datacontrol.xml.java.AnnotationScanner;
import org.adfemg.datacontrol.xml.java.JClass;
import org.adfemg.datacontrol.xml.utils.LoggerUtils;

/**
 * Runtime annotation scanner to scans for all JAR files on the classpath that contain a given (triggering) resource
 * for classes that have a given annotation as well as all classes with that annotation in any accessibl WEB-INF/lib
 * directory if a ServletContext is available.
 */
public class RtAnnotationScanner extends AnnotationScanner {

    private static final String WEB_INF_CLASSES = "/WEB-INF/classes/";

    private static final ADFLogger logger = ADFLogger.createADFLogger(RtAnnotationScanner.class);

    /**
     * Default constructor.
     * @param annotationClass annotation class that should be present on classes being found
     * @param jarTriggerResource resource that should be present in a JAR to include it in the scanning. For performance
     * reasons we only scan for annotated classes in JARs that contain this resource. For example
     * <tt>META-INF/adfm.xml</tt> to only scan JARs that are known to contain ADF Model artifacts.
     * @param skipJars list of JAR file names (without directories or paths) to skip during scanning even if they
     * contain the <tt>jarTriggerResource</tt>. This could also be used to increase performance by skipping certain
     * JAR files.
     */
    public RtAnnotationScanner(final JClass annotationClass, final String jarTriggerResource,
                               final Set<String> skipJars) {
        super(annotationClass, jarTriggerResource, skipJars);
    }

    /**
     * Find classes that potentially have the requested annotation. This not only scans JAR files by
     * invoking super.potentialClasses() but also includes any classes in any WEB-INF/lib directory
     * accessible through the ServletContext. This includes the WEB-INF/lib directory in the current
     * web application, as well as all WEB-INF/lib directories in JAR files in the current web application.
     * If no ServletContext is available this just returns the classes from the JAR classpath scanning by
     * the superclass.
     * @return set of classnames that might have the requested annotation
     */
    @Override
    protected Set<String> potentialClasses() {
        Set<String> webInfClasses = potentialWebInfClasses(); // scan .class files in WEB-INF/classes
        Set<String> jarClasses = super.potentialClasses(); // have superclass do dificult JAR scanning
        Set<String> retval = new HashSet<String>(webInfClasses.size() + jarClasses.size());
        retval.addAll(webInfClasses);
        retval.addAll(jarClasses);
        return retval;
    }

    /**
     * Scan <code>WEB-INF/classes</code> for classes that may be annotated
     * with any of the Faces configuration annotations.
     * @param sc the <code>ServletContext</code> for the application being scanned
     * @param paths a set of paths to process
     * @param classList the <code>Set</code> to which annotated classes will be added
     */
    private Set<String> potentialWebInfClasses() {
        ServletContext servletCtx = getServletContext();
        if (servletCtx == null) {
            logger.warning("could not determine ServletContext, skip scanning {0}", WEB_INF_CLASSES);
            return Collections.emptySet(); // non-web environment like adf model tester or unit testing
        }
        final Set<String> retval = new HashSet<String>(50);
        Set<String> roots = servletCtx.getResourcePaths(WEB_INF_CLASSES);
        logger.finer("starting WEB-INF scan with root(s) {0}", roots);
        processWebInfClasses(roots, retval, servletCtx);
        return retval;
    }

    private ServletContext getServletContext() {
        // be careful for NullPointerException in non-web environments like ADF Model Tester or Unit test
        final ADFContext ctx = ADFContext.getCurrent();
        Environment env = ctx == null ? null : ctx.getEnvironment();
        final Object obj = env == null ? null : env.getContext();
        if (obj != null && !(obj instanceof ServletContext)) {
            throw new IllegalStateException("unable to locate ServletContext");
        }
        return (ServletContext) obj;
    }

    private void processWebInfClasses(Set<String> dirEntries, Set<String> classList, ServletContext servletCtx) {
        logger.finest("scanning WEB-INF entries {0}", dirEntries);
        if (dirEntries == null || dirEntries.isEmpty()) {
            return;
        }
        for (String entry : dirEntries) {
            if (entry.endsWith("/")) { // recurse into sub directory
                processWebInfClasses(servletCtx.getResourcePaths(entry), classList, servletCtx);
            } else if (entry.endsWith(".class")) {
                try {
                    final InputStream stream = servletCtx.getResource(entry).openStream();
                    if (containsAnnotation(stream)) {
                        String cname = convertToClassName(entry, WEB_INF_CLASSES);
                        LoggerUtils.finest(logger, "bytecode scanner probably found annotated class {0} in {1}", cname,
                                           WEB_INF_CLASSES);
                        classList.add(cname);
                    }
                } catch (Exception e) {
                    logger.warning("error loading .class file for annotation scanning: " + entry, e);
                    break;
                }
            }
        }
    }

    private String convertToClassName(final String pathEntry, final String stripPrefix) {
        String className = pathEntry;
        if (stripPrefix != null && className.startsWith(stripPrefix)) {
            className = className.substring(stripPrefix.length());
        }
        return super.convertToClassName(className);
    }

}
