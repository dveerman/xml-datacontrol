package org.adfemg.datacontrol.xml.java;

import java.util.Set;

import oracle.adf.share.logging.ADFLogger;

import org.adfemg.datacontrol.xml.design.DesignTimeUtils;
import org.adfemg.datacontrol.xml.java.dt.DtAnnotationScanner;
import org.adfemg.datacontrol.xml.java.dt.DtClass;
import org.adfemg.datacontrol.xml.java.rt.RtAnnotationScanner;
import org.adfemg.datacontrol.xml.java.rt.RtClass;
import org.adfemg.datacontrol.xml.utils.ClassUtils;

/**
 * Starting point for a lightweight reflection framework that can work with both the java.lang.reflect.* framework
 * at runtime as well as the oracle.javatools.parser.java.v2.model.* framework at design time.
 * <p>The framework doesn't support all the features of full blown reflection but only the minimal set of functions
 * we need in the XML DataControl, which is mainly for customization classes processing.
 * <p>A number of interfaces exist in this package that are agnostic to runtime or design time. The specific
 * implementations for these two environments live in the {@code dt} and {@code rt} subpackages.
 */
public final class JClassFactory {

    private static final ADFLogger logger = ADFLogger.createADFLogger(JClassFactory.class);

    /**
     * Find a class by the given name.
     * @param className fully qualified classname, for example {@code com.exmple.application.MyClass}
     * @return DtClass when running in design time mode (JDeveloper, ojaudit, ojdeploy, etc) or RtClass when
     * running in runtime modus (weblogic or other runtime environment)
     */
    public static JClass forClass(final String className) {
        if (DesignTimeUtils.isDesignTime()) {
            DtClass d = new DtClass(className);
            // do not return DtClass if it failed to load class (happens with enums and property files, etc)
            if (d.getRealClass() == null) {
                logger.finer("failed to load design time class {0}", className);
                return null;
            } else {
                return d;
            }
        } else {
            return new RtClass<Object>(ClassUtils.loadClass(className));
        }
    }

    /**
     * Find all java classes with the given annotation.
     * @param annotationClass annotation class that should be present on the java class to be included in the result
     * set.
     * @param jarTriggerResource the resource that should be present in a classpath JAR file to be included in the
     * scanning. This limits the number of JAR files that are read for performance reasons. For example this could
     * be <tt>META-INF/adfm.xml</tt> to only include JAR files that are known to contain ADF Model artifacts.
     * @param skipJars set of JAR file names that should never be scanned. Can be used for JAR files that are known
     * to include the <tt>jarTriggerResource</tt> but still don't need to be scanned.
     * @return set of java classes that have the requested annotation
     * @see DtAnnotationScanner
     * @see RtAnnotationScanner
     */
    public static Set<JClass> findAnnotatedClasses(final JClass annotationClass, final String jarTriggerResource,
                                                   final Set<String> skipJars) {
        logger.finer("scanning for java annotations");
        return DesignTimeUtils.isDesignTime() ?
               new DtAnnotationScanner(annotationClass, jarTriggerResource, skipJars).getAnnotatedClasses() :
               new RtAnnotationScanner(annotationClass, jarTriggerResource, skipJars).getAnnotatedClasses();
    }

}
