/**
 * Java Reflection API interfaces that are independent of design-time or run-time usage.
 */
package org.adfemg.datacontrol.xml.java;

