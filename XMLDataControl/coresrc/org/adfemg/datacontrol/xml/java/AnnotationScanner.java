package org.adfemg.datacontrol.xml.java;

import java.io.IOException;
import java.io.InputStream;

import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.adf.share.logging.ADFLogger;

import org.adfemg.datacontrol.xml.utils.LoggerUtils;

/**
 * Scans all JAR files on the classpath that contain a given (triggering) resource for classes that have a given
 * annotation.
 */
public abstract class AnnotationScanner {

    // regex to get URL to JAR based on URL to found trigger resource in that JAR
    private static final Pattern RESOURCE_JAR_PATTERN = Pattern.compile("(.*\\.jar).*");
    // regex to get just the name of the JAR file from a JAR content url such as jar:file:/home/duke/duke.jar!/
    private static final Pattern JAR_URL_NAME_PATTERN = Pattern.compile(".*[\\/](.*?\\.jar)!/");

    // constructor args
    private final JClass annotationClass;
    private final String jarTrigger;
    private final Set<String> skipJars;
    private final ClassFile classFileScanner;

    private static final ADFLogger logger = ADFLogger.createADFLogger(AnnotationScanner.class);

    /**
     * Default constructor.
     * @param annotationClass annotation class that should be present on classes being found
     * @param jarTriggerResource resource that should be present in a JAR to include it in the scanning. For performance
     * reasons we only scan for annotated classes in JARs that contain this resource. For example
     * <tt>META-INF/adfm.xml</tt> to only scan JARs that are known to contain ADF Model artifacts.
     * @param skipJars list of JAR file names (without directories or paths) to skip during scanning even if they
     * contain the <tt>jarTriggerResource</tt>. This could also be used to increase performance by skipping certain
     * JAR files.
     */
    public AnnotationScanner(final JClass annotationClass, final String jarTriggerResource,
                             final Set<String> skipJars) {
        this.annotationClass = annotationClass;
        this.jarTrigger = jarTriggerResource;
        this.skipJars = skipJars == null ? Collections.<String>emptySet() : new HashSet<String>(skipJars);
        // create one instance of ClassFile that we re-use for each scanned class to re-use some expensive buffers
        // in ClassFile instead of creating new ones for each scanned class
        this.classFileScanner = new ClassFile(annotationClass.getName());
    }

    /**
     * Scans all potential annotated classes from potentialClasses() for any Class that has an annotation with the
     * annotationClass supplied to the constructor.
     * @return Set of classes verified to have an annotation with the annotationClass supplied to the constructor
     * by actually loading them and verifying they have the annotation
     */
    public final Set<JClass> getAnnotatedClasses() {
        Set<String> classNames = potentialClasses();
        // loads all potential classes and uses normal reflection to make sure the have the annotation
        final Set<JClass> retval = verifyByLoading(classNames);
        return retval;
    }

    /**
     * Finds all potential classes with the requested annotation. This should be on the safe side, so prefer to
     * include a class in the result that later turns out not to have the annotation than not include a class
     * that might have the annotation. The returned set of classes should later be verified by true classloading to
     * have the requested annotation.
     * <p>This default implementation scans all JARs on the classpath that contain the resource as specified by
     * getJarTriggerResource. It then scans all the class file entries in these JAR files to search for classes that
     * have the annotation.
     * @return set of classnames that might have the requested annotation
     */
    protected Set<String> potentialClasses() {
        final Set<String> retval = new HashSet<String>(50);
        for (URL jarContentUrl : findJarFiles()) {
            logger.finest("scanning JAR {0}", jarContentUrl);
            try {
                if (skipJar(jarContentUrl)) {
                    logger.finest("skipping customization scan of JAR {0}", jarContentUrl);
                    continue;
                }
                processJarEntries(jarContentUrl, retval);
            } catch (Exception e) {
                logger.warning("error processing " + jarContentUrl, e);
                continue;
            }
        }
        return retval;
    }

    /**
     * Determines if a JAR file should be skipped and its entries should not be scanned.
     * @param jarFile a JarFile to consider for skipping, for example <tt>adf-richclient-bootstrap.jar</tt>
     * @return <tt>true</tt> if <tt>jarFile</tt> is in the list of JARs to skip that was supplied to the constructor.
     */
    private boolean skipJar(final URL jarContentUrl) {
        // skip known ADF libraries that are likely on classpath but don't contain XML DC Customizations
        return this.skipJars.contains(simpleJarName(jarContentUrl));
    }

    private String getJarTriggerResource() {
        return jarTrigger;
    }

    /**
     * Get just the name of a JAR file from a jar file content URL.
     * @param jarContentUrl URL to the content of a JAR file, so ending with <tt>!/</tt>, for example
     * <tt>jar:file:/home/duke/duke.jar!/</tt>
     * @return just the name of the JAR file itself, for example <tt>duke.jar</tt>
     */
    private String simpleJarName(final URL jarContentUrl) {
        Matcher matcher = JAR_URL_NAME_PATTERN.matcher(jarContentUrl.toString());
        if (!matcher.matches()) {
            logger.warning("not a valid jar content url: " + jarContentUrl);
            return null;
        }
        return matcher.group(1);
    }

    /**
     * Find all JAR files on the classpath that contain the trigger resource as supplied to the constructor.
     * @return list of JarFiles that each contain the triggering resource
     */
    private List<URL> findJarFiles() {
        ClassLoader classLoader = getJarClassLoader();
        List<URL> triggerUrls = Collections.emptyList();
        try {
            triggerUrls = Collections.list(classLoader.getResources(getJarTriggerResource()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        logger.finest("starting JAR scan based on triggers {0}", triggerUrls);
        List<URL> retval = new ArrayList<URL>();
        for (URL trigger : triggerUrls) {
            Matcher matcher = RESOURCE_JAR_PATTERN.matcher(trigger.toString());
            if (!matcher.matches()) {
                continue;
            }
            try {
                URL jarUrl = jarContentUrl(matcher.group(1));
                retval.add(jarUrl);
            } catch (MalformedURLException e) {
                logger.warning("error opening jar for annotation scanning: " + matcher.group(1), e);
                continue;
            }
        }
        return retval;
    }

    /**
     * Get the classloader to load JAR files.
     * @return the current thread context classloader.
     * @see Thread#getContextClassLoader
     */
    protected ClassLoader getJarClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

    private URL jarContentUrl(final String jarUrl) throws MalformedURLException {
        StringBuilder retval = new StringBuilder(32);
        if (!jarUrl.startsWith("jar:")) {
            retval.append("jar:");
        }
        if (jarUrl.startsWith("zip:")) {
            retval.append("file:").append(jarUrl.substring(4));
        } else if (jarUrl.startsWith("bundle:")) {
            retval.append("file:").append(jarUrl.substring(7));
        } else {
            retval.append(jarUrl);
        }
        retval.append("!/");
        return new URL(retval.toString());
    }

    @SuppressWarnings("oracle.jdeveloper.java.insufficient-catch-block")
    private void processJarEntries(URL jarContentUrl, Set<String> classList) throws IOException {
        logger.finest("scanning JAR {0} for annotations", jarContentUrl);
        JarFile jarFile = null;
        try {
            jarFile = ((JarURLConnection) jarContentUrl.openConnection()).getJarFile();
            processJarEntries(jarFile, classList);
        } finally {
            if (jarFile != null) {
                try {
                    jarFile.close();
                } catch (IOException ignored) {
                }
            }
        }
    }

    private void processJarEntries(final JarFile jarFile, final Set<String> classList) {
        for (Enumeration<JarEntry> entries = jarFile.entries(); entries.hasMoreElements();) {
            JarEntry entry = entries.nextElement();
            if (entry.isDirectory()) {
                continue;
            }
            String name = entry.getName();
            if (name.startsWith("META-INF")) {
                continue;
            }
            if (name.endsWith(".class")) {
                String cname = convertToClassName(name);
                try {
                    final InputStream stream = jarFile.getInputStream(entry);
                    if (containsAnnotation(stream)) {
                        LoggerUtils.finest(logger,
                                           "bytecode scanner probably found annotated class {0} in JAR entry {1}",
                                           cname, entry.getName());
                        classList.add(cname);
                    }
                } catch (Exception e) {
                    logger.warning("error loading .class file for annotation scanning: " + entry, e);
                    break;
                }
            }
        }
    }

    /**
     * Determines if a .class file is likely to have the annotation we are scanning for without actually loading
     * the class through a classloader but by reading the bytecode. This prevents filling up the java permGen
     * with lots of classes that don't have the annotation and are of no interest. This scanning is on the safe side,
     * so when any error occurs this returns <tt>true</tt> and the class should be loaded through its classloader to
     * make the final call.
     * @param inputStream InputStream to the bytecode of a java class. Will be closed by this method on completion
     * regardless whether any exception occurs
     * @return <tt>true</tt> if the java class has the searched annotation or when an error occred, or <tt>false</tt>
     * if the class is known not to have the annotation.
     * @throws IOException
     */
    @SuppressWarnings("oracle.jdeveloper.java.insufficient-catch-block")
    protected final boolean containsAnnotation(InputStream inputStream) throws IOException {
        if (inputStream == null) {
            return false;
        }
        ReadableByteChannel channel = null;
        try {
            channel = Channels.newChannel(inputStream);
            return classFileScanner.containsAnnotation(channel);
        } finally {
            if (channel != null) {
                try {
                    channel.close();
                    inputStream.close();
                } catch (Exception ignored) {
                }
            }
        }
    }

    private JClass getAnnotationClass() {
        return annotationClass;
    }

    /**
     * Convert the full path to a java .class file to the name of the java class.
     * @param filepath full path to the java class, for example <tt>com/example/MyClass.class</tt>
     * @return name of the java class, which is the given filepath stripped of the .class suffix and all / characters
     * replaced with .
     */
    protected final String convertToClassName(final String filepath) {
        String className = filepath;
        // remove the .class suffix
        className = className.substring(0, (className.length() - 6));
        return className.replace('/', '.');
    }

    private Set<JClass> verifyByLoading(Set<String> classNames) {
        logger.finest("inspecting annotations on potential classes {0}", classNames);
        final Set<JClass> retval = new HashSet<JClass>(classNames.size());
        if (classNames != null && !classNames.isEmpty()) {
            for (String className : classNames) {
                logger.finest("verifying class {0}", className);
                JClass cls = JClassFactory.forClass(className);
                if (cls != null && cls.getAnnotation(getAnnotationClass()) != null) {
                    logger.finest("{0} verified to have annotation", className);
                    retval.add(cls);
                }
            }
        }
        return retval;
    }

    /**
     * This class is encapsulating binary .class file information as defined at
     * http://java.sun.com/docs/books/vmspec/2nd-edition/html/ClassFile.doc.html
     * <p/>
     * This is used by the annotation frameworks to quickly scan .class files
     * for the presence of annotations. This avoid the annotation framework
     * having to load each .class file in the class loader.
     * <p/>
     * Taken from the GlassFish V2 source base.
     */
    private static final class ClassFile {
        private final String annotationClassNameInternal;
        private static final int magic = 0xCAFEBABE;
        private short majorVersion;
        private short minorVersion;
        private ByteBuffer header;

        private ClassFile(final String annotationClassName) {
            this.header = ByteBuffer.allocate(12000);
            this.annotationClassNameInternal = annotationClassName.replace(".", "/");
        }

        /**
         * Read the input channel and initialize instance data structure.
         * @param classBytes ReadableByteChannel that provides the bytes of a classfile
         * @return <code>true</code> if the bytes representing this classfile include one of the annotations we're
         * looking for.
         * @throws IOException if an I/O error occurs while reading the class
         */
        public boolean containsAnnotation(final ReadableByteChannel classBytes) throws IOException {
            /* this is the .class file layout
             * ClassFile {
             *   u4             magic;
             *   u2             minor_version;
             *   u2             major_version;
             *   u2             constant_pool_count;
             *   cp_info        constant_pool[constant_pool_count-1];
             *   u2             access_flags;
             *   u2             this_class;
             *   u2             super_class;
             *   u2             interfaces_count;
             *   u2             interfaces[interfaces_count];
             *   u2             fields_count;
             *   field_info     fields[fields_count];
             *   u2             methods_count;
             *   method_info    methods[methods_count];
             *   u2             attributes_count;
             *   attribute_info attributes[attributes_count];
             * }
             */
            header.clear();
            if (classBytes.read(header) == -1) {
                return false; // reached end-of-stream
            }
            header.rewind();
            if (header.getInt() != magic) {
                return false;
            }
            minorVersion = header.getShort();
            majorVersion = header.getShort();
            int constantPoolSize = header.getShort();
            return hasAnnotationConstant(constantPoolSize, classBytes);
        }

        private static final byte CLASS = 7;
        private static final int FIELDREF = 9;
        private static final int METHODREF = 10;
        private static final int STRING = 8;
        private static final int INTEGER = 3;
        private static final int FLOAT = 4;
        private static final int LONG = 5;
        private static final int DOUBLE = 6;
        private static final int INTERFACEMETHODREF = 11;
        private static final int NAMEANDTYPE = 12;
        private static final int ASCIZ = 1;
        private static final int UNICODE = 2;

        private byte[] bytes = new byte[Short.MAX_VALUE];

        private boolean hasAnnotationConstant(final int numConstants,
                                              final ReadableByteChannel classBytes) throws IOException {
            for (int i = 1; i < numConstants; i++) {
                if (!refill(header, classBytes, 1)) {
                    return true; // something went wrong, return true just to be on safe side
                }
                final byte type = header.get();
                switch (type) {
                case ASCIZ:
                case UNICODE:
                    if (!refill(header, classBytes, 2)) {
                        return true; // something went wrong, return true just to be on safe side
                    }
                    final short length = header.getShort();
                    if (length < 0 || length > Short.MAX_VALUE) {
                        return true; // something went wrong, return true just to be on safe side
                    }
                    if (length > header.capacity()) {
                        return true; // something went wrong, return true just to be on safe side
                    }
                    if (!refill(header, classBytes, length)) {
                        return true; // something went wrong, return true just to be on safe side
                    }
                    header.get(bytes, 0, length);
                    // to speed up the process, quickly compare a number of individual characters from the annotation
                    // class name to prevent constructing a String object for each comparison. Shift all character
                    // comparison by 1 as the header reference is in internal notation like Lcom/example/MyClass
                    // optimized for Lorg/adfemg/datacontrol/... so we compare some critical bytes from the first
                    // package names
                    if (bytes[0] == 'L' && bytes[1] == annotationClassNameInternal.charAt(0) &&
                        bytes[5] == annotationClassNameInternal.charAt(4) &&
                        bytes[10] == annotationClassNameInternal.charAt(9) &&
                        bytes[12] == annotationClassNameInternal.charAt(11)) {
                        String stringValue;
                        if (type == ASCIZ) {
                            stringValue = new String(bytes, 0, length, "US-ASCII");
                        } else {
                            stringValue = new String(bytes, 0, length, "ISO-8859-1" /*RIConstants.CHAR_ENCODING*/);
                        }
                        if (stringValue.contains(annotationClassNameInternal)) {
                            return true; // found annotation
                        }
                    }
                    break;
                case CLASS:
                case STRING:
                    if (!refill(header, classBytes, 2)) {
                        return true; // something went wrong, return true just to be on safe side
                    }
                    header.getShort();
                    break;
                case FIELDREF:
                case METHODREF:
                case INTERFACEMETHODREF:
                case INTEGER:
                case FLOAT:
                    if (!refill(header, classBytes, 4)) {
                        return true; // something went wrong, return true just to be on safe side
                    }
                    header.position(header.position() + 4);
                    break;
                case LONG:
                case DOUBLE:
                    if (!refill(header, classBytes, 8)) {
                        return true; // something went wrong, return true just to be on safe side
                    }
                    header.position(header.position() + 8);
                    // for long, and double, they use 2 constantPool
                    i++;
                    break;
                case NAMEANDTYPE:
                    if (!refill(header, classBytes, 4)) {
                        return true; // something went wrong, return true just to be on safe side
                    }
                    header.getShort();
                    header.getShort();
                    break;
                default:
                    // somehow the JVM bytecode specification has changed :-(
                    LoggerUtils.warning(logger, "unknown type constant pool {0} at position {1}", type, i);
                    break;
                }
            }
            return false;
        }

        private boolean refill(ByteBuffer buffer, ReadableByteChannel input, int length) throws IOException {
            int cap = buffer.capacity();
            if (buffer.position() + length > cap) {
                buffer.compact();
                int read = input.read(buffer);
                if (read < 0) {
                    return false; // end of input channel found
                }
                buffer.rewind();
            }
            return true; // not finished yet, probably more bytes available
        }

    } // END ClassFile

}
