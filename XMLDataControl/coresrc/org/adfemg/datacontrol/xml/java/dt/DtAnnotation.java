package org.adfemg.datacontrol.xml.java.dt;

import java.util.Map;

import oracle.javatools.parser.java.v2.model.JavaAnnotation;

import org.adfemg.datacontrol.xml.java.JAnnotation;

/**
 * Implementation of JAnnotation that wraps the JDeveloper design time API
 * oracle.javatools.parser.java.v2.model.JavaAnnotation.
 */
public class DtAnnotation implements JAnnotation {

    private final JavaAnnotation annotation;

    /**
     * Constructor.
     * @param annotation Java annotation being represented.
     */
    public DtAnnotation(final JavaAnnotation annotation) {
        this.annotation = annotation;
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.tag-is-missing", "oracle.jdeveloper.java.description-is-empty",
                      "oracle.jdeveloper.java.tag-is-misplaced" })
    public Object getElement(String name) {
        Map args = annotation.getArguments();
        return args.get(name);
    }

    /**
     * Returns the oracle.javatools.parser.java.v2.model.JavaAnnotation wrapped in this JAnnotation for situations
     * where users want to break the design-time/runtime abstraction from the JAnnotation interfaces and are
     * willing to downcast to DtAnnotation and get the wrapped Annotation, for example to use JDeveloper's JavaManager
     * APIs.
     * @return oracle.javatools.parser.java.v2.model.JavaAnnotation
     */
    public JavaAnnotation getRealAnnotation() {
        return annotation;
    }

    /**
     * compares this annotation to another annotation.
     * @param object other annotation to compare to
     * @return <tt>true</tt> if given object is a DtAnnotation representing where
     * this.getRealAnnotation().equals(object.getRealAnnotation())
     */
    @Override
    public boolean equals(Object object) {
        return object instanceof DtAnnotation && ((DtAnnotation) object).getRealAnnotation().equals(annotation);
    }

    /**
     * hashCode of the oracle.javatools.parser.java.v2.model.JavaAnnotation in this DtAnnotation.
     * @return this.getRealAnnotation().hashCode()
     */
    @Override
    public int hashCode() {
        return annotation.hashCode();
    }

}
