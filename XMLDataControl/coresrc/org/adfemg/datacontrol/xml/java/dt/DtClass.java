package org.adfemg.datacontrol.xml.java.dt;

import java.util.ArrayList;
import java.util.Collection;

import oracle.ide.model.Project;

import oracle.javatools.parser.java.v2.model.JavaAnnotation;
import oracle.javatools.parser.java.v2.model.JavaClass;
import oracle.javatools.parser.java.v2.model.JavaMethod;

import oracle.jdeveloper.java.JavaManager;
import oracle.jdeveloper.java.JavaModel;

import org.adfemg.datacontrol.xml.design.ProjectUtils;
import org.adfemg.datacontrol.xml.java.JAnnotation;
import org.adfemg.datacontrol.xml.java.JClass;
import org.adfemg.datacontrol.xml.java.JMethod;

/**
 * Implementation of JClass that wraps the JDeveloper design time API oracle.javatools.parser.java.v2.model.JavaClass.
 */
public class DtClass implements JClass {

    final JavaClass jcls;

    /**
     * Constructor.
     * @param className name of the Java class to represent.
     * @see ProjectUtils#getCurrentDataControlProject
     */
    public DtClass(final String className) {
        Project project = ProjectUtils.getCurrentDataControlProject();
        // even sees pending changes in unsaved java source files
        JavaModel jmodel = JavaManager.getJavaManager(project);
        jcls = jmodel.getClass(className);
    }

    /**
     * Constructor.
     * @param clazz Java class to represent.
     */
    public DtClass(final JavaClass clazz) {
        this.jcls = clazz;
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public String getName() {
        return jcls.getQualifiedName();
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public Collection<JMethod> getMethods() {
        Collection<JavaMethod> methods = jcls.getMethods();
        Collection<JMethod> retval = new ArrayList<JMethod>(methods.size());
        for (JavaMethod method : methods) {
            retval.add(new DtMethod(method));
        }
        return retval;
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public boolean isPrimitive() {
        return jcls.isPrimitive();
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public boolean isAssignableFrom(JClass subject) {
        if (!(subject instanceof DtClass)) {
            throw new IllegalArgumentException("can only compare to other design time classes");
        }
        return jcls.isAssignableFrom(((DtClass) subject).getRealClass());
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public JAnnotation getAnnotation(JClass annotationClass) {
        if (!(annotationClass instanceof DtClass)) {
            throw new IllegalArgumentException("can only compare to other design time classes");
        }
        final JavaAnnotation janno = jcls.getAnnotation(((DtClass) annotationClass).getRealClass());
        return janno == null ? null : new DtAnnotation(janno);
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced" })
    public Collection<JAnnotation> getAnnotations() {
        Collection<JavaAnnotation> jannos = jcls.getAnnotations();
        Collection<JAnnotation> retval = new ArrayList<JAnnotation>(jannos.size());
        for (JavaAnnotation janno : jannos) {
            retval.add(new DtAnnotation(janno));
        }
        return retval;
    }

    /**
     * Returns the oracle.javatools.parser.java.v2.model.JavaClass wrapped in this DtClass for situations
     * where users want to break the design-time/runtime abstraction from the JClass interfaces and are willing
     * to downcast to DtClass and get the wrapped JavaClass, for example to further invoke the JDeveloper java APIs.
     * @return oracle.javatools.parser.java.v2.model.JavaClass
     */
    public JavaClass getRealClass() {
        return jcls;
    }

    /**
     * compares this class to another class.
     * @param object other class to compare to
     * @return <tt>true</tt> if given object is a DtClass representing where
     * this.getRealClass().equals(object.getRealClass())
     */
    @Override
    public boolean equals(Object object) {
        return object instanceof DtClass && ((DtClass) object).getRealClass().equals(jcls);
    }

    /**
     * hashCode of the oracle.javatools.parser.java.v2.model.JavaClass in this DtClass.
     * @return this.getRealClass().hashCode()
     */
    @Override
    public int hashCode() {
        return jcls.hashCode();
    }

    /**
     * @inheritDoc
     */
    @Override
    @SuppressWarnings({ "oracle.jdeveloper.java.description-is-empty", "oracle.jdeveloper.java.tag-is-misplaced",
                      "oracle.jdeveloper.java.tag-is-missing" })
    public String toString() {
        return DtClass.class.getSimpleName() + "[" + jcls.getQualifiedName() + "]";
    }

}
