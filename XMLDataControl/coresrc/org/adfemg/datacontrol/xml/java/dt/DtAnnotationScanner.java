package org.adfemg.datacontrol.xml.java.dt;

import java.net.URL;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import oracle.adf.share.logging.ADFLogger;

import oracle.ide.net.URLPath;

import oracle.jdeveloper.java.JavaModel;
import oracle.jdeveloper.model.PathsConfiguration;

import org.adfemg.datacontrol.xml.design.ProjectUtils;
import org.adfemg.datacontrol.xml.java.AnnotationScanner;
import org.adfemg.datacontrol.xml.java.JClass;
import org.adfemg.datacontrol.xml.java.rt.RtAnnotationScanner;
import org.adfemg.datacontrol.xml.utils.LoggerUtils;

/**
 * Design time annotation scanner to scans for all JAR files on the current datacontrol project run classpath that
 * contain a given (triggering) resource for classes that have a given annotation as well as all source and compiled
 * classes in the current datacontrol project with that annotation.
 * @see ProjectUtils#getCurrentDataControlProjectPaths
 */
public class DtAnnotationScanner extends AnnotationScanner {

    private static final ADFLogger logger = ADFLogger.createADFLogger(RtAnnotationScanner.class);

    /**
     * Default constructor.
     * @param annotationClass annotation class that should be present on classes being found
     * @param jarTriggerResource resource that should be present in a JAR to include it in the scanning. For performance
     * reasons we only scan for annotated classes in JARs that contain this resource. For example
     * <tt>META-INF/adfm.xml</tt> to only scan JARs that are known to contain ADF Model artifacts.
     * @param skipJars list of JAR file names (without directories or paths) to skip during scanning even if they
     * contain the <tt>jarTriggerResource</tt>. This could also be used to increase performance by skipping certain
     * JAR files.
     */
    public DtAnnotationScanner(final JClass annotationClass, final String jarTriggerResource,
                               final Set<String> skipJars) {
        super(annotationClass, jarTriggerResource, skipJars);
    }

    /**
     * Find classes that potentially have the requested annotation. This not only scans JAR files by
     * invoking super.potentialClasses() but also includes any .java and .class classes in the current datacontrol
     * project.
     * @return set of classnames that might have the requested annotation
     * @see ProjectUtils#getCurrentDataControlProjectPaths
     */
    @Override
    protected Set<String> potentialClasses() {
        // include all .java source filesfrom project as well as .class files in project output directory
        Set<String> projectClasses = getCompiledAndSourceClassNames();
        Set<String> jarClasses = super.potentialClasses(); // have superclass do dificult JAR scanning
        Set<String> retval = new HashSet<String>(projectClasses.size() + jarClasses.size());
        retval.addAll(projectClasses);
        retval.addAll(jarClasses);
        return retval;
    }

    /**
     * Use a different classloader in JDeveloper design time environment that looks at the current datacontrol
     * project classpath.
     * @return current datacontrol project's classpath classloader
     */
    @Override
    protected ClassLoader getJarClassLoader() {
        URLPath runPath = ProjectUtils.getCurrentDataControlProjectPaths().getRunClassPath();
        runPath.expandToIncludeManifestClassPaths();
        // build JavaModel that ignores sources and only looks at run path
        JavaModel jmodel = JavaModel.getInstance(null, runPath);
        final ClassLoader retval = jmodel.getClassLoader();
        return retval;
    }

    private Set<String> getCompiledAndSourceClassNames() {
        PathsConfiguration paths = ProjectUtils.getCurrentDataControlProjectPaths();
        URLPath sourcePath = paths.getProjectSourcePath();
        URLPath runDirectories = getRunDirectories(paths);
        // look in source paths as well as compiled classes directory (ignore expensive to scan JARs)
        if (logger.isFinest()) {
            LoggerUtils.finest(logger, "scanning classes in source path {0} and run directories {1}", sourcePath,
                               runDirectories);
        }
        JavaModel jmodel = JavaModel.getInstance(sourcePath, runDirectories);
        Collection<String> allClasses = jmodel.getJavaClassLocator().getAllClasses();
        if (logger.isFiner()) {
            LoggerUtils.finest(logger, "nominating project classes for loading: {0}", allClasses);
        }
        return new HashSet<String>(allClasses);
    }

    private URLPath getRunDirectories(PathsConfiguration paths) {
        URLPath runPath = paths.getRunClassPath();
        runPath.expandToIncludeManifestClassPaths();
        // get directories from runPath (ignore JAR entries)
        List<URL> directories = new ArrayList<URL>(10);
        for (URL u : runPath.asList()) {
            if ("file".equals(u.getProtocol())) {
                directories.add(u);
            }
        }
        return new URLPath(directories);
    }

}
