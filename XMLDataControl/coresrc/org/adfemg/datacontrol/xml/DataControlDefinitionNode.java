package org.adfemg.datacontrol.xml;

import java.lang.reflect.Constructor;

import java.net.URL;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.namespace.QName;

import oracle.adf.model.adapter.AdapterException;
import oracle.adf.share.logging.ADFLogger;

import oracle.xml.parser.schema.XMLSchema;
import oracle.xml.parser.schema.XSDBuilder;
import oracle.xml.parser.schema.XSDException;

import org.adfemg.datacontrol.xml.design.DesignTimeUtils;
import org.adfemg.datacontrol.xml.design.MessageUtils;
import org.adfemg.datacontrol.xml.ha.SnapshotManagerImpl;
import org.adfemg.datacontrol.xml.provider.Configuration;
import org.adfemg.datacontrol.xml.provider.DynamicParameter;
import org.adfemg.datacontrol.xml.provider.Provider;
import org.adfemg.datacontrol.xml.provider.customization.CustomizationProvider;
import org.adfemg.datacontrol.xml.provider.data.DataProvider;
import org.adfemg.datacontrol.xml.provider.filter.DataFilter;
import org.adfemg.datacontrol.xml.provider.filter.MultiDataFilter;
import org.adfemg.datacontrol.xml.provider.structure.StructureProvider;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;
import org.adfemg.datacontrol.xml.utils.ClassUtils;
import org.adfemg.datacontrol.xml.utils.DomUtils;
import org.adfemg.datacontrol.xml.utils.LoggerUtils;
import org.adfemg.datacontrol.xml.utils.URLUtils;
import org.adfemg.datacontrol.xml.utils.XmlParseUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;


/**
 * Configuration class of the XML DataControl. This should be initialized with the
 * {@literal <Source>} node from the DataControls.dcx configuration file. This Node can
 * be supplied through the constructor or by caling {@link #load}. After loading
 * of this source node, several getter methods are available to read this metadata.
 */
public class DataControlDefinitionNode {

    public static final String NS_DEFINITION = "http://adfemg.org/adfm/datacontrol/configuration";

    public static final String ELEM_DEFINITION = "definition";
    public static final String DEF_ATTR_SCHEMA = "schema";
    public static final String DEF_ATTR_SCHEMA_DT = "schema-dt";
    public static final String DEF_ATTR_SCHEMA_ROOT = "schema-root";
    public static final String DEF_ATTR_DC_OPERATION = "dc-operation";
    public static final String DEF_ATTR_CHANGE_TRACKING = "change-tracking";

    // When adding a new type of provider, be sure to also update XMLDCConstants with the
    // default implementation
    public static final String ELEM_DATA_PROVIDER = "data-provider";
    public static final String ELEM_MAPPER_PROVIDER = "type-mapper";
    public static final String ELEM_CUST_PROVIDER = "customization-provider";
    public static final String ELEM_STRUCT_PROVIDER = "structure-provider";
    public static final String PROV_ATTR_CLASS = "class";

    public static final String ELEM_PARAMETERS = "parameters";

    // Parameter Constants
    public static final String ELEM_PARAM = "parameter";
    public static final String PARAM_ATTR_NAME = "name";
    public static final String PARAM_ATTR_VALUE = "value";

    // Dynamic Parameter Constants
    public static final String ELEM_DYNAMIC_PARAM = "dynamic-parameter";
    public static final String PARAM_DYNAMIC_ATTR_NAME = "name";
    public static final String PARAM_DYNAMIC_ATTR_JAVA_TYPE = "java-type";
    public static final String PARAM_DYNAMIC_ATTR_XML_TYPE = "xml-type";

    // XML Parameter Constants
    public static final String ELEM_XML_PARAM = "xml-parameter";
    public static final String PARAM_XML_ATTR_NAME = "name";

    // List Parameter Constants
    public static final String ELEM_LIST_PARAM = "list-parameter";
    public static final String PARAM_LIST_ATTR_NAME = "name";
    public static final String VALUE_ELEM_LIST_PARAM = "value";

    private static final ADFLogger logger = ADFLogger.createADFLogger(DataControlDefinitionNode.class);

    // regexp pattern om "variabelen" in de waarde van attributen te vinden
    // zodat we deze kunnen vervangen door context init parameters
    private static final Pattern REPLACE_PATTERN = Pattern.compile(".*?(\\$\\{)([^}]*)(\\}).*", Pattern.DOTALL);

    private static final Pattern QNAME_PATTERN = Pattern.compile("\\{([^}]*)\\}(.+)");

    /**
     * Default name that will be used for the datacontrol operation if none is specified
     * in the configuration XML.
     */
    public static final String DFLT_DC_OPERATION = "getXML";

    private DataControlDefinition dcDef;

    // properties from DataControls.dcx
    private String schema;
    private String schemaDesignTime;
    private String schemaRoot;
    private String datacontrolOperation;
    private String changeTracking;
    private Map<Class<? extends Provider>, DataControlProviderDefinition> providerDefs =
        new HashMap<Class<? extends Provider>, DataControlProviderDefinition>(); // provider definitions
    private Map<Class<? extends Provider>, Provider> providerInstances =
        new HashMap<Class<? extends Provider>, Provider>(); // provider instances

    // cached values
    private URL url;
    private XMLSchema fetchedSchema;

    /**
     * Default no-args constructor.
     */
    public DataControlDefinitionNode() {
    }

    /**
     * Constructor with the {@literal <Source>} node from the DataControls.dcx file.
     * Loads the definition from the Node.
     * @param dcDef the parent DataControlDefinition this DataControlDefinitionNode belongs to
     * @param node The {@literal <Source>} node from the DataControls.dcx file.
     */
    public DataControlDefinitionNode(final DataControlDefinition dcDef, final Node node) {
        this.dcDef = dcDef;
        load(node);
    }

    /**
     * Returns the DataControlDefinition this definitionNode belongs to, or null
     * if this DataControlDefinitionNode was created using the no-arg constructor.
     * @return DataControlDefinition
     */
    public DataControlDefinition getDataControlDefinition() {
        return dcDef;
    }

    /**
     * Load method to load (re-)initialise a definition from a Node.
     *
     * @param node The {@code {http://adfemg.org/adfm/datacontrol/configuration}definition}
     *             node from the DataControls.dcx file.
     */
    public void load(final Node node) {
        // (re-)initialise
        schema = null;
        schemaRoot = null;
        datacontrolOperation = null;
        changeTracking = null;
        providerDefs.clear();
        url = null;

        if (!(node instanceof Element) || !NS_DEFINITION.equals(node.getNamespaceURI()) ||
            !ELEM_DEFINITION.equals(node.getLocalName())) {
            return;
        }
        loadDefinition((Element) node);
    }

    /**
     * Get the information from the Element and put them in the variables.
     * @param definition The {@code {http://adfemg.org/adfm/datacontrol/configuration}definition}
     *                   Element containing the definition information.
     */
    private void loadDefinition(final Element definition) {
        schema = getOptionalAttribute(definition, DEF_ATTR_SCHEMA);
        schemaDesignTime = getOptionalAttribute(definition, DEF_ATTR_SCHEMA_DT);
        schemaRoot = getOptionalAttribute(definition, DEF_ATTR_SCHEMA_ROOT);
        changeTracking = getOptionalAttribute(definition, DEF_ATTR_CHANGE_TRACKING);
        datacontrolOperation = getOptionalAttribute(definition, DEF_ATTR_DC_OPERATION);
        // when adding a new type of provider, be sure to also update XMLDCConstants.DFLT_PROVIDERS
        // TODO: perhaps it is better to include the defaults in the call to loadProviderDef so we
        // force developers to think about default here. Could very well be that defaults still come
        // from central map.
        providerDefs.put(DataProvider.class,
                         loadProviderDef(DomUtils.findFirstChildElement(definition, ELEM_DATA_PROVIDER)));
        providerDefs.put(TypeMapper.class,
                         loadProviderDef(DomUtils.findFirstChildElement(definition, ELEM_MAPPER_PROVIDER)));
        providerDefs.put(CustomizationProvider.class,
                         loadProviderDef(DomUtils.findFirstChildElement(definition, ELEM_CUST_PROVIDER)));
        providerDefs.put(StructureProvider.class,
                         loadProviderDef(DomUtils.findFirstChildElement(definition, ELEM_STRUCT_PROVIDER)));
    }

    /**
     * Loads a provider out of the provided Element.
     * @param provElem The element from DataControls.dcx that defines the provider.
     * @return {@code null} if provElem is null or otherwise the DataControlProviderDefinition
     *         represented by the given element.
     */
    private DataControlProviderDefinition loadProviderDef(final Element provElem) {
        if (provElem == null) {
            return null;
        }
        String className = getOptionalAttribute(provElem, PROV_ATTR_CLASS);
        DataControlProviderDefinition providerDef = new DataControlProviderDefinition(className);
        // load parameters
        Element params = DomUtils.findFirstChildElement(provElem, ELEM_PARAMETERS);
        if (params != null) {
            // iterate normal name-value parameters in DataControls.dcx and apply to provider definition
            for (Element param : DomUtils.findChildElements(params, ELEM_PARAM)) {
                providerDef.setStringParameter(getOptionalAttribute(param, PARAM_ATTR_NAME),
                                               getOptionalAttribute(param, PARAM_ATTR_VALUE));
            }
            // iterate all xml-parameters in DataControls.dcx and apply to provider definition
            for (Element param : DomUtils.findChildElements(params, ELEM_XML_PARAM)) {
                String name = getOptionalAttribute(param, PARAM_XML_ATTR_NAME);
                // find first CDATA, Text or Element child
                NodeList children = param.getChildNodes();
                Node target = null;
                for (int i = 0, n = children.getLength(); i < n; i++) {
                    Node child = children.item(i);
                    if (child.getNodeType() == Node.CDATA_SECTION_NODE || child.getNodeType() == Node.ELEMENT_NODE ||
                        (child.getNodeType() == Node.TEXT_NODE && !child.getNodeValue().trim().isEmpty())) {
                        // at design time we see the CDATA secition, but runtime parser exposes this as text node
                        target = child;
                        break;
                    }
                }
                if (target == null) {
                    logger.warning("xml parameter {0} in DataControls.dcx is missing a child CDATA section with the actual XML",
                                   name);
                } else if (target.getNodeType() == Node.CDATA_SECTION_NODE || target.getNodeType() == Node.TEXT_NODE) {
                    // parse cdata or text node
                    String xmlText = target.getNodeValue();
                    try {
                        Document xmlDoc = XmlParseUtils.parse(xmlText);
                        providerDef.setXmlParameter(name, xmlDoc.getDocumentElement());
                    } catch (SAXException e) {
                        LoggerUtils.warning(logger, "error parsing xml-parameter {0}:\n{1}", name, xmlText);
                        logger.warning(e);
                        if (DesignTimeUtils.isGraphicalDesignTime()) {
                            MessageUtils.showErrorMessage("XML parse error",
                                                          "could not parse " + providerDef.getClassName() +
                                                          " xml parameter " + name + ":\n" + xmlText + "\n" +
                                                          e.getMessage());
                        } else {
                            throw new AdapterException(e);
                        }
                    }
                } else if (target.getNodeType() == Node.ELEMENT_NODE) {
                    logger.warning("xml parameter {0} in DataControls.dcx should use <![CDATA[....]]> for proper namespace handling, not {1} with value {2}", new Object[] {
                                   name, target, target.getNodeValue()
                    });
                    providerDef.setXmlParameter(name, target.cloneNode(true));
                }
            }
            // iterate all dynamic-parameters in DataControls.dcx and apply to provider definition
            for (Element param : DomUtils.findChildElements(params, ELEM_DYNAMIC_PARAM)) {
                final String paramName = getOptionalAttribute(param, PARAM_DYNAMIC_ATTR_NAME);
                final String paramJavaType = getOptionalAttribute(param, PARAM_DYNAMIC_ATTR_JAVA_TYPE);
                // get (optional) xml type as qname
                final String paramXmlType = getOptionalAttribute(param, PARAM_DYNAMIC_ATTR_XML_TYPE);
                QName xmlTypeQName = null;
                if (paramXmlType != null && !paramXmlType.isEmpty()) {
                    // xml-type has to be in format "{namespace}localName"
                    Matcher matcher = QNAME_PATTERN.matcher(paramXmlType);
                    if (matcher.matches()) {
                        String namespace = matcher.group(1);
                        String localPart = matcher.group(2);
                        xmlTypeQName = namespace.isEmpty() ? new QName(localPart) : new QName(namespace, localPart);
                    } else {
                        throw new IllegalArgumentException(PARAM_DYNAMIC_ATTR_XML_TYPE +
                                                           " has to be in format \"{namespaceURI}typeName\"");
                    }
                }
                providerDef.addDynamicParam(new DynamicParameter(paramName, paramJavaType, xmlTypeQName));
            }
            // iterate all list-parameters in DataControls.dcx and apply to provider definition
            for (Element param : DomUtils.findChildElements(params, ELEM_LIST_PARAM)) {
                String name = getOptionalAttribute(param, PARAM_LIST_ATTR_NAME);
                List<String> list = new ArrayList<String>();
                for (Element value : DomUtils.findChildElements(param, VALUE_ELEM_LIST_PARAM)) {
                    list.add(value.getTextContent());
                }
                providerDef.setListParameter(name, list);
            }
        }
        if (ELEM_DATA_PROVIDER.equals(provElem.getLocalName())) {
            // data-provider can contain nested data-provider(s)
            for (Element childDataProv : DomUtils.findChildElements(provElem, ELEM_DATA_PROVIDER)) {
                DataControlProviderDefinition child = loadProviderDef(childDataProv);
                if (child != null) {
                    providerDef.addNestedProviderDefinition(child);
                }
            }
        }
        return providerDef;
    }

    /**
     * Get an attribute by its name from the Element while replacing any ${...} expression
     * with the appropriate value. The ${...} expressions are replaced with servlet context
     * init parameters or system properties values by the name enclosed in the expression.
     * @param element The element containing the attribute.
     * @param attrName The name of the attribute to look for in the element.
     * @return The attribute as String or {@code null} if the attribute did not exist or
     *         contained an empty string.
     * @see Configuration#getParameter
     */
    private String getOptionalAttribute(final Element element, final String attrName) {
        String ret = element.getAttribute(attrName);
        if (ret == null || ret.trim().isEmpty()) {
            return null;
        }
        return resolveExpressions(ret);
    }

    /**
     * Replace all ${...} expressions in the given string
     * @param value string with potential ${...} expressions
     * @return input value with all ${...} expressions replaced which could be a full copy
     * of input value of no expressions have been found
     * @see Configuration#getParameter
     */
    private String resolveExpressions(final String value) {
        if (value == null) {
            return null;
        }
        // TODO: We should prob. re-write this to a use EL resolving.
        String retval = value;
        Matcher matcher = REPLACE_PATTERN.matcher(retval);
        while (matcher.matches()) {
            final String prefix = retval.substring(0, matcher.start(1));
            final String paramName = matcher.group(2);
            String newVal = Configuration.getConfiguration().getParameter(paramName);
            if (newVal == null) {
                logger.warning("Unable to resolve {0} parameter, replacing with empty string", paramName);
            }
            final String postfix = retval.substring(matcher.end(3), retval.length());
            retval = prefix + (newVal == null ? "" : newVal) + postfix;
            matcher = REPLACE_PATTERN.matcher(retval); // keep doing this until all expressions are replaced
        }
        return retval;
    }

    /**
     * We can not return the Metadata (XML) in its original state as placeholders in
     * provider parameters have already been replaced with runtime values.
     *
     * @return always throws an UnsupportedOperationException.
     */
    public Node createMetadata() {
        throw new UnsupportedOperationException("createMetadata is not supported in the XMLDataControl");
    }

    /**
     * Get the URL of the schema.
     * First we try if the schema is an URL, else we try if the Schema is a
     * relative path to a file and to create a File. If this also failes we
     * try to load the schema as a resource.
     *
     * @return the URL, {@code null} if all tries fail.
     */
    public URL getSchemaUrl() {
        if (url != null || schema == null) {
            // return cached URL or simply don't try if no schema is specified
            return url;
        }
        url = URLUtils.findResource(getSchemaUrlString());
        return url;
    }

    private String getSchemaUrlString() {
        // use schema-dt when it is set and we are actually running in design-time mode (the JDeveloper IDE)
        return DesignTimeUtils.isDesignTime() && schemaDesignTime != null ? schemaDesignTime : schema;
    }

    /**
     * Get the actual XML Schema. Will cache the result so subsequent calls
     * will just return the cached schema. When the schema cannot be loaded,
     * this will throw an exception at runtime or show a dialog at design
     * time.
     * @return the XMLSchema, which might be cached from a previous fetch.
     * @throws XSDException when problems arise parsing the XSD document from the
     * {@code schema} or {@code schema-dt} attributes in DataControls.dcx
     * @see #getSchemaUrl
     */
    public XMLSchema getSchema() throws XSDException {
        if (fetchedSchema != null) {
            // return cached XMLSchema so we only build this on first invoke
            return fetchedSchema;
        }
        final XSDBuilder builder = new XSDBuilder();
        LoggerUtils.begin(logger, "fetching XSD", "schemaUrl", String.valueOf(getSchemaUrl()));
        try {
            URL schemaUrl = getSchemaUrl();
            if (schemaUrl == null) {
                if (DesignTimeUtils.isGraphicalDesignTime()) {
                    MessageUtils.showErrorMessage("Loading Exception",
                                                  "Failed to load XML Schema " + getSchemaUrlString() +
                                                  " as URL, File or Classpath Resource. " +
                                                  "\nPlease check the schema or schema-dt in the definition node in the DataControls.dcx " +
                                                  "file and make sure the XSD can be found. \nWhen using a classpath resource, " +
                                                  "first compile your project so the files exist in the classes directory.");
                    return null;
                } else {
                    throw new IllegalArgumentException("Failed to load " + getSchemaUrlString() +
                                                       " as URL, File or Classpath Resource. Check the schema in the definition node in the DataControls.dcx file.");
                }
            }
            fetchedSchema = builder.build(schemaUrl);
            return fetchedSchema;
        } finally {
            logger.end("fetching XSD");
        }
    }

    /**
     * Get the providers.
     * @return an unmodifiableMap of the providers.
     */
    public Map<Class<? extends Provider>, DataControlProviderDefinition> getProviderDefinitions() {
        return Collections.unmodifiableMap(providerDefs);
    }

    /**
     * Get an instantiated provider of a certain type, or otherwise instantiate it
     * and cache it for future reference.
     * @param <T> Type of provider to get. Should be one of the keys in XMLDCConstants.DFLT_PROVIDERS
     * @param iface type of provider to get
     * @return Provider instance that only gets created on the first call while subsequent calls will return the
     * same cached provider
     * @throws IllegalArgumentException if requested interface type is not a key in XMLDCConstants.DFLT_PROVIDERS
     */
    public <T extends Provider> T getProviderInstance(Class<T> iface) {
        if (!providerInstances.containsKey(iface)) {
            // only initialize on first call and cache for subsequent calls
            @SuppressWarnings({ "unchecked", "cast" })
            Class<? extends T> dfltImpl = (Class<? extends T>) XMLDCConstants.DFLT_PROVIDERS.get(iface);
            if (dfltImpl == null) {
                throw new IllegalArgumentException("unsupported provider type " + iface.getName());
            }
            // get provider definition from DataControls.dcx
            DataControlProviderDefinition providerDef = getProviderDefinitions().get(iface);
            if (DataProvider.class.equals(iface)) {
                // when asking/initializing the DataProvider always wrap this at the top level with
                // a SnapshotManager
                DataControlProviderDefinition snapProviderDef =
                    new DataControlProviderDefinition(SnapshotManagerImpl.class.getName());
                snapProviderDef.addNestedProviderDefinition(providerDef);
                if (logger.isFiner()) {
                    logger.finer("wrapping dataprovider {0} for {1} with a snapshot provider {2} for HA cluster support", new Object[] {
                                 providerDef, getDataControlDefinition().getDCName(), snapProviderDef
                    });
                }
                providerDef = snapProviderDef;
            }
            final Provider impl = newProviderInstance(providerDef, iface, dfltImpl);
            providerInstances.put(iface, impl);
        }
        @SuppressWarnings("unchecked")
        T provider = (T) providerInstances.get(iface);
        return provider;
    }

    /**
     * Returns all providers. Remember that this are all providers that are defined in DataControls.dcx.
     * It will force load all the providers.
     * This is run-time only functionality.
     *
     * @return all providers for this DataControlDefinitionNode
     */
    public Collection<Provider> getProviderInstances() {
        Collection<Provider> retval = new ArrayList<Provider>(XMLDCConstants.DFLT_PROVIDERS.size());
        for (Class<? extends Provider> provInt : XMLDCConstants.DFLT_PROVIDERS.keySet()) {
            retval.add(getProviderInstance(provInt));
        }
        return Collections.unmodifiableCollection(retval);
    }

    /**
     * Instantiate a (new) provider and set all parameters on this provider.
     *
     * @param <T> Type of the (new) provider.
     * @param providerDef Definition of the (new) provider.
     * @param iface The interface to implement by the (new) provider.
     * @param defaultClass The default Class to use if the {@code providerDef}
     *                     does not contain a Class.
     * @return Instance of the {@code iface}
     */
    private <T extends Provider> T newProviderInstance(final DataControlProviderDefinition providerDef,
                                                       final Class<T> iface, final Class<? extends T> defaultClass) {
        Class<? extends T> cls = null;
        if (providerDef != null) {
            cls = providerDef.loadClass(iface);
        }
        if (cls == null) {
            cls = defaultClass;
        }
        // instantiate class
        T provider;
        if (MultiDataFilter.class.isAssignableFrom(cls)) {
            // MultiDataFilter needs to be constructed with list of nested DataProviders as constructor arg
            List<DataControlProviderDefinition> nestedDataProvDefs = providerDef.getNestedProviderDefinitions();
            List<DataProvider> nestedDataProvs = new ArrayList<DataProvider>(nestedDataProvDefs.size());
            @SuppressWarnings("unchecked")
            Class<? extends DataProvider> dfltDataProvImpl =
                (Class<? extends DataProvider>) XMLDCConstants.DFLT_PROVIDERS.get(DataProvider.class);
            for (DataControlProviderDefinition nestedDataProvDef : nestedDataProvDefs) {
                nestedDataProvs.add(newProviderInstance(nestedDataProvDef, DataProvider.class, dfltDataProvImpl));
            }
            // now instantiate MultiDataFilter with nested DataProviders
            try {
                @SuppressWarnings({ "unchecked", "cast" })
                Constructor<? extends MultiDataFilter> constructor =
                    (Constructor<? extends MultiDataFilter>) cls.getConstructor(List.class);
                @SuppressWarnings({ "cast", "unchecked" })
                T multiDataFilter = (T) constructor.newInstance(nestedDataProvs);
                provider = multiDataFilter;
            } catch (NoSuchMethodException e) {
                throw new IllegalStateException(cls.getName() +
                                                " should have constructor with single List<DataProvider> argument", e);
            } catch (Exception e) {
                throw new AdapterException(e);
            }
        } else if (DataFilter.class.isAssignableFrom(cls)) {
            // DataFilter needs to be constructed with nested DataProvider as constructor arg
            DataControlProviderDefinition nestedDataProvDef = providerDef.getNestedProviderDefinitions().get(0);
            @SuppressWarnings("unchecked")
            Class<? extends DataProvider> dfltDataProvImpl =
                (Class<? extends DataProvider>) XMLDCConstants.DFLT_PROVIDERS.get(DataProvider.class);
            DataProvider nestedDataProv = newProviderInstance(nestedDataProvDef, DataProvider.class, dfltDataProvImpl);
            // now instantiate DataFilter with nested DataProvider
            try {
                @SuppressWarnings({ "cast", "unchecked" })
                Constructor<? extends DataFilter> constructor =
                    (Constructor<? extends DataFilter>) cls.getConstructor(DataProvider.class);
                @SuppressWarnings({ "cast", "unchecked" })
                T dataFilter = (T) constructor.newInstance(nestedDataProv);
                provider = dataFilter;
            } catch (NoSuchMethodException e) {
                throw new IllegalStateException(cls.getName() +
                                                " should have constructor with single DataProvider argument", e);
            } catch (Exception e) {
                throw new AdapterException(e);
            }
        } else {
            provider = ClassUtils.newInstance(cls);
        }
        // supply all parameters to the instantiated class
        if (providerDef != null) {
            // give all static parameters from this definition to the actual provider
            // no need to get default values yet as the providers themselves do that when getting values
            for (String stringParam : providerDef.getStringParameterNames()) {
                provider.setStringParameter(stringParam, providerDef.getRawStringParameter(stringParam, null, null));
            }
            for (String listParam : providerDef.getListParameterNames()) {
                provider.setListParameter(listParam, providerDef.getRawListParameter(listParam, null, null));
            }
            for (String xmlParam : providerDef.getXmlParameterNames()) {
                provider.setXmlParameter(xmlParam, providerDef.getRawXmlParameter(xmlParam, null, null));
            }
        }
        return provider;
    }

    /**
     * Get the Schema Root.
     * @return The schemaRoot as String.
     */
    public String getRoot() {
        return schemaRoot;
    }

    /**
     * Gets the name of the operation that should be exposed on the datacontrol.
     * @return value of the dc-operation attribute in the DataControls.dcx configuration or
     *         {@link #DFLT_DC_OPERATION} if this wasn't specified.
     */
    public String getDatacontrolOperation() {
        return datacontrolOperation != null ? datacontrolOperation : DFLT_DC_OPERATION;
    }

    /**
     * Method to determine if change-tracking is enabled in the datacontrol.
     * @return value of the change-tracking attribute in the DataControls.dcx configuration or false if this wasn't specified.
     */
    public boolean isChangeTracking() {
        return Boolean.valueOf(this.changeTracking);
    }

    /**
     * Get all dynamic parameter (definitions) for this definition node.
     * @return all dynamic parameter definitions
     */
    public Set<DynamicParameter> getDynamicParams() {
        final DataControlProviderDefinition dataProvConfig = getProviderDefinitions().get(DataProvider.class);
        return dataProvConfig == null ? null : dataProvConfig.getAllDynamicParams();
    }

    @Override
    public String toString() {
        return DataControlDefinitionNode.class.getSimpleName() + "[" + getDataControlDefinition().getQualifiedName() +
               "." + getDatacontrolOperation() + "]";
    }

}
