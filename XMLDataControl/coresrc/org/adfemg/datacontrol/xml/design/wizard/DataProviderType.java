package org.adfemg.datacontrol.xml.design.wizard;

import org.adfemg.datacontrol.xml.provider.data.ELDataProvider;
import org.adfemg.datacontrol.xml.provider.data.EmptyElementProvider;
import org.adfemg.datacontrol.xml.provider.data.ResourceDataProvider;
import org.adfemg.datacontrol.xml.provider.data.WSDataProvider;

public enum DataProviderType {
    WEBSERVICE("Web Service", WSDataProvider.class.getName()),
    EXPR_LANGUAGE("EL Expression", ELDataProvider.class.getName()),
    EMPTY_ELEMENT("Empty Element", EmptyElementProvider.class.getName()),
    RESOURCE("Classpath Resource", ResourceDataProvider.class.getName()),
    CUSTOM("Custom Java Class", "com.example.CustomDataProvider");

    private final String label;
    private final String className;

    DataProviderType(String label, String className) {
        this.label = label;
        this.className = className;
    }

    public String getLabel() {
        return label;
    }

    public String getClassName() {
        return className;
    }

    @Override
    public String toString() {
        return getLabel();
    }

    public static DataProviderType valueByClassName(String className) {
        for (DataProviderType dpt : DataProviderType.values()) {
            if (dpt.getClassName().equals(className)) {
                return dpt;
            }
        }
        return null;
    }

}
