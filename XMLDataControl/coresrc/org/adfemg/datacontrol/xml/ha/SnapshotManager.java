package org.adfemg.datacontrol.xml.ha;

import java.io.Serializable;

import org.adfemg.datacontrol.xml.provider.data.DataProvider;

/**
 * DataProvider that is capable of retaining the last result it has returned and returning
 * it as a Serializabl snapshot so it can be replicated to other nodes in a HA cluster where
 * it can be used for failover support.
 */
public interface SnapshotManager extends DataProvider {

    /**
     * Return the Serializable snapshot that gets replicated to other nodes in the cluster.
     * @return Serializable state
     */
    public Serializable createSnapshot();

    /**
     * Invoked on the secondary node in a replicating cluster so this SnapshotManager can
     * restore its state is it was on the primary server so it can return the same cached
     * information on the subsequent call to getRootElement
     * @param handle Serialized snapshot as created by createSnapshot
     */
    public void restoreSnapshot(Serializable handle);

    /**
     * Signals the SnapshotManager it can remove any persisted state it might have as it is
     * no longer needed. Typically called before destroying the associated datacontrol and can
     * be very useful if the createSnapshot doesn't actually return all state but persists it
     * elsewhere and just returns a handle to that persisted state. If that scneario is used
     * this method can be used to clear this persisted state as it is no longer needed.
     * @param handle Serialized snapshot or handle to it as created by createSnapshot
     */
    public void removeSnapshot(Serializable handle);

}
