package org.adfemg.datacontrol.xml.ha;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.adfemg.datacontrol.xml.utils.DomUtils;
import org.adfemg.datacontrol.xml.utils.XPointer;
import org.adfemg.datacontrol.xml.utils.XmlFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import org.xml.sax.SAXException;

/**
 * Since org.w3c.dom.Document doesn't have to support Serialization, this class can be used to wrap
 * a Document to give it Serialization capabilities including any userdata stored in nodes in the
 * document using SerializableUserData
 * @see SerializableUserData
 */
public class SerializableDocument implements Serializable {

    @SuppressWarnings("compatibility:2515651121580291688")
    private static final long serialVersionUID = 2L;
    private static final String USER_DATA_KEY = SerializableDocument.class.getName();

    // transient state as we use custom (de)serialization
    private transient Document document; // use custom xml serialization
    private transient List<Node> userdataNodes = new ArrayList<Node>(); // use custom xml serialization

    private SerializedState serializedState; // state for lazy deserialization

    /**
     * Public static method to get the single SerializableDocument instance that should exist for
     * a Document. By using this factory method we ensure that only a single SerializableDocument can exist
     * for each document. This is important for serialization so an object graph with multiple references
     * to this single document will all use the same SerializableDocument instance so it will be serialized
     * only once. This prevents each reference to the document to do its own serializion.
     * @param document XML document to be wrapped
     * @return already existing SerializableDocument for the given document or a new instance of
     * SerializableDocument for this document that will also be returned in subsequent calls to
     * forDocument for this same document.
     */
    public static SerializableDocument forDocument(Document document) {
        synchronized (document) {
            SerializableDocument retval = (SerializableDocument) document.getUserData(USER_DATA_KEY);
            if (retval == null) {
                retval = new SerializableDocument(document);
                document.setUserData(USER_DATA_KEY, retval, null);
            }
            return retval;
        }
    }

    /**
     * Private constructor so we can ensure only a single SerializableDocument exists for each Document
     * @param document
     * @see #forDocument
     */
    private SerializableDocument(Document document) {
        this.document = document;
    }

    /**
     * Gets the XML document that is wrapped in this SerializableDocument
     * @return XML document
     */
    public Document getDocument() {
        ensureDeserialization();
        return document;
    }

    /**
     * Add the give Node to the nodes that are known to have SerializableUserData.
     * Developers should normally not invoke this directly as this is handled by
     * SerializableUserData
     * @param userdataNode node with serializable user data
     * @throws IllegalArgumentException when given node is not owned by this document
     */
    public void add(Node userdataNode) {
        ensureDeserialization();
        checkOwnerDocument(userdataNode);
        userdataNodes.add(userdataNode);
    }

    /**
     * Unregister the given Node as a node that has serializable user data. Developers should
     * normally not invoke this directly as this is handled by SerializableUserData when a node
     * is deleted from the document.
     * @param userdataNode node being removed
     * @return {@code true} if the specified node was previously registered using {@link #add} and has now
     * been removed
     * @throws IllegalArgumentException when given node is not owned by this document
     */
    public boolean remove(Node userdataNode) {
        ensureDeserialization();
        checkOwnerDocument(userdataNode);
        return userdataNodes.remove(userdataNode);
    }

    /**
     * Returns true if the given node has been registered as having serializable user data.
     * @param node Node that is part of the document and might have serializable user data
     * @return true if the given node has been registered as having serializable user data
     * @see #add
     * @see #remove
     * @throws IllegalArgumentException when given node is not owned by this document
     */
    boolean nodeHasState(Node node) {
        ensureDeserialization();
        checkOwnerDocument(node);
        return userdataNodes.contains(node);
    }

    private void checkOwnerDocument(Node node) {
        if (node.getOwnerDocument() != document) {
            throw new IllegalArgumentException("node is not part of this registry's document");
        }
    }

    // custom serialization
    private void writeObject(ObjectOutputStream out) throws IOException {
        if (serializedState != null) {
            // we still have state from previous deserialization (ensureDeserialization wasn't invoked)
            out.writeObject(serializedState);
            return;
        }
        // get serializable UserData state for each Node and record the XPath to these Nodes
        Map<XPointer, Map<String, Serializable>> states =
            new HashMap<XPointer, Map<String, Serializable>>(userdataNodes.size());
        for (Node node : userdataNodes) {
            SerializableUserData<Node> data = SerializableUserData.forNode(node);
            if (data.isEmpty()) {
                break; // no need to serialize nodes with (now) empty UserData
            }
            states.put(XPointer.forElement((Element) node), new LinkedHashMap<String, Serializable>(data));
        }
        // serialize XML document
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            XmlFactory.newNonIndentingTransformer().transform(new DOMSource(document), new StreamResult(baos));
        } catch (TransformerException e) {
            throw new IOException("error serializing SerializableDocument", e);
        }
        out.writeObject(new SerializedState(baos.toByteArray(), states));
    }

    // custom deserialization to delay actual deserialization until the time it is actually needed and not as soon
    // as the object is replicated to other nodes in the cluster. Waiting for deserialization until the actual failover
    // has occured is much more efficient
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        serializedState = (SerializableDocument.SerializedState) in.readObject();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[" +
               (serializedState != null ? "serialized" : DomUtils.getQName(document.getDocumentElement())) + "]";
    }

    // process any state that was deserialized. Should be invoked by each method that requires object state to ensure
    // full dserialization has occured as readObject delays this
    private void ensureDeserialization() {
        if (serializedState == null) {
            return;
        }
        // first remove serializedState before restoring actual state to prevent endless loop due to
        // UserDataMap.forNode().put() which will invoke this SerializableDocument
        SerializedState restore = serializedState;
        serializedState = null;
        try {
            document = XmlFactory.newDocumentBuilder().parse(new ByteArrayInputStream(restore.xmldocument));
            document.setUserData(USER_DATA_KEY, this, null);
            userdataNodes = new ArrayList<Node>(restore.userdata.size());
            // restore UserData for each node that had this
            for (Map.Entry<XPointer, Map<String, Serializable>> entry : restore.userdata.entrySet()) {
                XPointer xpointer = entry.getKey();
                Element element = xpointer.resolve(document);
                SerializableUserData.forNode(element).putAll(entry.getValue());
            }
        } catch (SAXException e) {
            throw new RuntimeException("error deserializing SerializableDocument", e);
        } catch (IOException e) {
            throw new RuntimeException("error deserializing SerializableDocument", e);
        }
    }

    private static class SerializedState implements Serializable {
        @SuppressWarnings("compatibility:7715031309937434728")
        private static final long serialVersionUID = 2L;

        private byte[] xmldocument;
        @SuppressWarnings("oracle.jdeveloper.java.field-not-serializable")
        private Map<XPointer, Map<String, Serializable>> userdata;

        private SerializedState(final byte[] xmldocument, final Map<XPointer, Map<String, Serializable>> userdata) {
            this.xmldocument = xmldocument;
            this.userdata = userdata;
        }
    }


}
