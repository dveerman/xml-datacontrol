package org.adfemg.datacontrol.xml.provider;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;

import oracle.adf.share.ADFContext;
import oracle.adf.share.Environment;

/**
 * Utility class to retrieve global (default) application wide XML DataControl configutation parameters.
 */
public class Configuration {

    private static final String APP_SCOPE_KEY = Configuration.class.getName();

    private Map<String,String> cache = new HashMap<String,String>(10);

    /**
     * Private constructor. Clients should use getConfiguration() to get the Configuration.
     * @see #getConfiguration
     */
    private Configuration() {
        super();
    }

    /**
     * Get the Configuration object for the ADF application.
     * @return Configuration instance
     */
    public static Configuration getConfiguration() {
        if (!ADFContext.hasCurrent()) {
            // No context available, we are prob. in the design mode of JDev.
            return new Configuration();
        }
        ADFContext adfCtxt = ADFContext.getCurrent();
        Map<String, Object> applicationScope = adfCtxt.getApplicationScope();
        Object retval = applicationScope.get(APP_SCOPE_KEY);
        if (retval instanceof Configuration) {
            return (Configuration) retval;
        } else {
            retval = new Configuration();
            applicationScope.put(APP_SCOPE_KEY, retval);
            return (Configuration) retval;
        }
    }

    private String getInitParameter(final String name) {
        if (!ADFContext.hasCurrent()) {
            // No context available, we are prob. in the design mode of JDev.
            return null;
        }
        ADFContext adfCtxt = ADFContext.getCurrent();
        Environment env = adfCtxt.getEnvironment();
        Object ctxt = env == null ? null : env.getContext();
        if (ctxt instanceof ServletContext) {
            ServletContext sCtxt = (ServletContext) ctxt;
            return sCtxt.getInitParameter(name);
        }
        return null;
    }

    private String getSystemProperty(final String name) {
        return System.getProperty(name);
    }

    /**
     * Get a configuration parameter.
     * @param name name of the parameter to get
     * @return value from servlet context context-param, otherwise system property, or {@code null} if
     * these are all unknown
     */
    public String getParameter(final String name) {
        if (cache.containsKey(name)) {
            return cache.get(name);
        }
        String retval = getInitParameter(name);
        if (retval == null) {
            retval = getSystemProperty(name);
        }
        cache.put(name, retval);
        return retval;
    }

}
