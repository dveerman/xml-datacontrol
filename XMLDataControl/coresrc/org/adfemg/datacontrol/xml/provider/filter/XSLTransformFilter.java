package org.adfemg.datacontrol.xml.provider.filter;

import java.io.IOException;

import java.net.URL;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;

import org.adfemg.datacontrol.xml.provider.data.DataProvider;
import org.adfemg.datacontrol.xml.provider.data.DataRequest;
import org.adfemg.datacontrol.xml.utils.URLUtils;
import org.adfemg.datacontrol.xml.utils.XmlFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class XSLTransformFilter extends DataFilter {

    public static final String PARAM_STYLESHEET = "stylesheet"; // URL or file of XSL stylesheet

    public XSLTransformFilter(DataProvider dataProvider) {
        super(dataProvider);
    }

    @Override
    public Element getRootElement(DataRequest request) {
        DOMResult out = new DOMResult();
        Element wrapped = getSourceElement(request);
        Transformer transformer = createTransformer(request);
        transform(wrapped, out, transformer);
        if (!(out.getNode() instanceof Document)) {
            throw new IllegalStateException("Huh?? XSL transformation did not return a Document");
        }
        return ((Document) out.getNode()).getDocumentElement();
    }

    /**
     * Get the XSL stylesheet to use for the transformation of the XML we get from our
     * nested DataProvider for the given request.
     * @param request DataRequest
     * @return XSL stylesheet for the given request
     */
    protected Source getStylesheet(DataRequest request) {
        String stylesheet = getStringParameter(PARAM_STYLESHEET, request, XSLTransformFilter.class, null);
        if (stylesheet == null) {
            throw new IllegalArgumentException("mandatory parameter " + PARAM_STYLESHEET + " unknown");
        }
        URL url = URLUtils.findResource(stylesheet);
        if (url == null) {
            throw new IllegalArgumentException("unable to find stylesheet \"" + stylesheet + "\"");
        }
        try {
            // TODO: expensive to read and parse XSL file each time
            return new StreamSource(url.openStream());
        } catch (IOException e) {
            throw new IllegalArgumentException("unable to load " + url, e);
        }
    }

    /**
     * Perform the actual XSL transformation on the XML element we got from our nested DataProvider.
     * @param input XML element from the nested DataProvider
     * @param dest Destination document to transform to
     * @param transformer Stylesheet/XSL to apply to the source
     */
    protected void transform(Element input, Result dest, Transformer transformer) {
        try {
            transformer.transform(new DOMSource(createDocument(input)), dest);
        } catch (TransformerException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Create a Transformer for given request.
     */
    protected Transformer createTransformer(DataRequest request) {
        // create transformer in separate method so subclass can easily override it
        // TODO: isn't it expensive to create new transformer on each call? but caching might be dangerous if
        // xsl location uses a dynamic parameter. Need some caching based on xslt location and
        // TransformerFactory.newTemplates to do xslt compilation once
        return XmlFactory.newTransformer(getStylesheet(request));
    }

    /**
     * Create a XML document with the given element as root element to be used as source for the
     * XSL transformation.
     * @param rootElement XML element
     * @return XML document with a copy of the given element as its root without altering the source
     * element or removing it from its source document
     */
    protected synchronized Document createDocument(Element rootElement) {
        Document doc = XmlFactory.newDocument();
        Node importedNode = doc.importNode(rootElement, true); // importNode doesn't change/remove source node
        doc.appendChild(importedNode);
        return doc;
    }

}
