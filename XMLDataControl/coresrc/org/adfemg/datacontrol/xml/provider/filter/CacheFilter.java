package org.adfemg.datacontrol.xml.provider.filter;

import java.util.LinkedHashMap;
import java.util.Map;

import org.adfemg.datacontrol.xml.provider.data.DataProvider;
import org.adfemg.datacontrol.xml.provider.data.DataRequest;

import org.w3c.dom.Element;

public abstract class CacheFilter extends DataFilter {

    /**
     * Name of the parameter to specify whether the cached XML Element should be cloned before returning it.
     * This is typically used when the CacheFilter is nested in other DataFilter(s) that manipulate the element
     * and you want to make sure the cached element is not altered.
     */
    public static final String PARAM_CLONE = "clone";
    public static final boolean DFLT_CLONE = false;

    public CacheFilter(DataProvider dataProvider) {
        super(dataProvider);
    }

    protected abstract Map<Map, Element> getCache(String structDefFullName);

    protected Map<Map, Element> getCache(DataRequest request) {
        return getCache(request.getStructureDefinition().getFullName());
    }

    @Override
    public Element getRootElement(DataRequest request) {
        Map<Map, Element> cache = getCache(request);
        Map<String, Object> dynamicParamValues = request.getDynamicParamValues();
        // use new map instance as cache-key to prevent changes to the key by downstream filters
        Map<String, Object> dynParsClone = new LinkedHashMap<String, Object>(dynamicParamValues);
        Element cached = cache.get(dynParsClone);
        if (cached != null) {
            return cloneIfNeeded(cached, request);
        } else {
            Element result = getSourceElement(request);
            cache.put(dynParsClone, result);
            return cloneIfNeeded(result, request);
        }
    }

    /**
     * Clones the given element if this CacheFilter has a parameter clone with its value set to {@code true},
     * otherwise it simply returns the given element without altering it.
     * @param element element to return or clone
     * @param request
     * @return element without any changes, unless this CacheFilter has its {@code clone} parameter set
     * to {@code true} in which case this returns a cloned element.
     */
    protected Element cloneIfNeeded(Element element, DataRequest request) {
        String clone = getStringParameter(PARAM_CLONE, request, CacheFilter.class, Boolean.toString(DFLT_CLONE));
        if (Boolean.valueOf(clone)) {
            return (Element) element.cloneNode(true);
        } else {
            return element;
        }
    }
}
