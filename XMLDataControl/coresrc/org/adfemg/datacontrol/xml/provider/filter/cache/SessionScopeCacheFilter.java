package org.adfemg.datacontrol.xml.provider.filter.cache;

import java.util.Map;

import oracle.adf.share.ADFContext;

import org.adfemg.datacontrol.xml.provider.data.DataProvider;

public class SessionScopeCacheFilter extends AdfScopeCacheFilter {

    public SessionScopeCacheFilter(DataProvider dataProvider) {
        super(dataProvider);
    }

    @Override
    protected Map<String, Object> getScope() {
        return ADFContext.getCurrent().getSessionScope();
    }

}
