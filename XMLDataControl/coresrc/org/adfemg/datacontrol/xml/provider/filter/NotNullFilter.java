package org.adfemg.datacontrol.xml.provider.filter;

import oracle.adf.share.logging.ADFLogger;

import org.adfemg.datacontrol.xml.provider.data.DataProvider;
import org.adfemg.datacontrol.xml.provider.data.DataRequest;

import org.w3c.dom.Element;

public class NotNullFilter extends DataFilter {

    private static final ADFLogger logger = ADFLogger.createADFLogger(NotNullFilter.class);

    // name of the DataControls.dcx parameter that specifies the name of
    // the dynamic-parameter that has to be non-null for the "query" to go through
    public static final String PARAM_REQUIRED_PARAM = "required-parameter";

    public NotNullFilter(DataProvider dataProvider) {
        super(dataProvider);
    }

    @Override
    public Element getRootElement(DataRequest request) {
        String requiredDynParamName = getStringParameter(PARAM_REQUIRED_PARAM, request, NotNullFilter.class, null);
        if (requiredDynParamName == null) {
            throw new IllegalArgumentException("NotNullFilter missing required parameter " + PARAM_REQUIRED_PARAM);
        }
        if (request.getDynamicParamValues().get(requiredDynParamName) == null) {
            logger.fine("dynamic parameter {0} null, preventing call to wrapped data-provider", requiredDynParamName);
            return null;
        } else {
            return getSourceElement(request);
        }
    }
}
