package org.adfemg.datacontrol.xml.provider.data.ws;

import com.sun.xml.ws.client.BindingProviderProperties;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.NamingException;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;
import javax.xml.ws.soap.SOAPBinding;

import oracle.adf.model.connection.url.URLConnection;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;

import org.adfemg.datacontrol.xml.utils.LoggerUtils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Convenience class fof invoking a webservice using JAX-WS. Has several setter methods to
 * initialize the client that use the Builder pattern. They return the same instance of WSClient
 * with its new value set. This can be used to invoke a number of these setters in a single
 * chain:<br/>
 * {@code new WSClient().setEndPointUrl("...").setRequestBody(...).invoke()}
 * <p>The invoke() method does the actual web service invocation. Internally it uses the
 * create...() methods in this class to build the supporting objects. These create method
 * are friendly to subclassing to extend this WSClient with new capabilities.
 */
public class WSClient {

    /**
     * Namespace for the javax.xml.ws.Service and associated Port.
     */
    public static final String NAMESPACE = "http://xmlns.adfemg.org/xmldc";

    /**
     * The default service name of the javax.xml.ws.Service we'll be creating. Can be a fixed
     * name as this is never used in the actual http request.
     */
    public static final QName DFLT_SERVICE_NAME = new QName(NAMESPACE, "XMLDCService");

    /**
     * The dfault name of the port of the Service we'll be creating. Can be a fixed name as this is
     * never used in the actual http request.
     */
    public static final QName DFLT_SERVICE_PORT = new QName(NAMESPACE, "XMLDCPort");

    private QName serviceName = DFLT_SERVICE_NAME;
    private QName portName = DFLT_SERVICE_PORT;
    private String endPointUrl;
    private String soapActionUri;
    private Integer connectTimeout;
    private Integer requestTimeout;
    private String soapBindingId = SoapVersion.V1_1.getBinding();
    private InvocationType invocationType = InvocationType.SYNCHRONOUS;
    private HandlerResolver handlerResolver = new DefaultHandlerResolver();
    private List<Element> requestHeader = Collections.emptyList();
    private List<Element> requestBody = Collections.emptyList();

    private static final ADFLogger logger = ADFLogger.createADFLogger(WSClient.class);
    private static final String LOG_ACTION = "invoking web service";

    /**
     * Invoke the web service. Before calling this, you should have called a series of
     * setters to initialize the WSClient, for example:<br/>
     * {@code new WSClient().setEndPointUrl("...").setRequestBody(...).invoke()}
     * @return SOAPMessage as returned by the web service
     * @throws SOAPException
     */
    public SOAPMessage invoke() throws SOAPException {
        final Service service = createService();
        final Dispatch<SOAPMessage> dispatch = createDispatch(service);
        final SOAPMessage request = createRequest(dispatch);
        LoggerUtils.begin(logger, LOG_ACTION, "endPointUrl", endPointUrl, "soapAction", getSoapActionUri(),
                          "invocationType", invocationType);
        try {
            return invokeService(dispatch, request);
        } finally {
            logger.end(LOG_ACTION);
        }
    }

    /**
     * Factory method to create and initialize javax.xml.ws.Service.
     * @return javax.xml.ws.Service that should contain a Port with the name from
     * getPort() as that will be used by createDispatch()
     */
    protected Service createService() {
        final Service service = Service.create(getServiceName());
        String url = getEndPointUrl();
        service.addPort(getPortName(), getSoapBindingId(), url);
        service.setHandlerResolver(getHandlerResolver()); // register (optional) HandlerResolver
        return service;
    }

    /**
     * Factory method to create and initialize {@literal javax.xml.ws.Dispatch<SOAPMessage>}.
     * @param service the javax.xml.ws.Service to create the Dispatch for
     * @return javax.xml.ws.Dispatch that will be used to invoke the web service
     */
    protected Dispatch<SOAPMessage> createDispatch(final Service service) {
        final Dispatch<SOAPMessage> dispatch =
            service.createDispatch(getPortName(), SOAPMessage.class, Service.Mode.MESSAGE);
        Map<String, Object> requestCtx = dispatch.getRequestContext();
        final String soapActionUri = getSoapActionUri();
        if (soapActionUri != null && !soapActionUri.isEmpty()) {
            requestCtx.put(BindingProvider.SOAPACTION_USE_PROPERTY, Boolean.TRUE);
            requestCtx.put(BindingProvider.SOAPACTION_URI_PROPERTY, soapActionUri);
        }
        final Integer requestTimeout = getRequestTimeout();
        if (requestTimeout != null) {
            requestCtx.put(BindingProviderProperties.REQUEST_TIMEOUT, requestTimeout.intValue());
        }
        final Integer connectTimeout = getConnectTimeout();
        if (connectTimeout != null) {
            requestCtx.put(BindingProviderProperties.CONNECT_TIMEOUT, connectTimeout.intValue());
        }
        return dispatch;
    }

    /**
     * Factory method to create a new SOAPMessage request to dispatch to the web service. Actual filling of
     * the body and header is handled by invoking buildHeader() and buildBody().
     * @param dispatch {@literal Dispatch<SOAPMessage>} as returned by {@link #createDispatch}
     * @return SOAPMessage containing a SOAPBody and optional SOAPHeader
     * @throws SOAPException
     * @see #createDispatch
     * @see #buildHeader
     * @see #buildBody
     */
    protected SOAPMessage createRequest(final Dispatch<SOAPMessage> dispatch) throws SOAPException {
        final MessageFactory mf = ((SOAPBinding) dispatch.getBinding()).getMessageFactory();
        final SOAPMessage msg = mf.createMessage();
        buildHeader(msg.getSOAPHeader());
        buildBody(msg.getSOAPBody());
        return msg;
    }

    /**
     * Add content to the web service reuqest soap header. Will get the header information from
     * getRequestHeader and add that to the given SOAPHeader
     * @param header SOAPHeader to add the content to
     * @throws SOAPException
     * @see #getRequestHeaderElements
     */
    protected void buildHeader(final SOAPHeader header) throws SOAPException {
        for (Element e : getRequestHeaderElements()) {
            appendClonedChild(header, e);
        }
    }

    /**
     * Add content to the web service reuqest soap body. Will get the body information from
     * getRequestBody and add that to the given SOAPBody
     * @param body SOAPBody to add the content to
     * @throws SOAPException
     * @see #getRequestBodyElements
     */
    protected void buildBody(final SOAPBody body) throws SOAPException {
        for (Element e : getRequestBodyElements()) {
            appendClonedChild(body, e);
        }
    }

    /**
     * Convenience method to add a org.w3c.dom.Node to a given SOAPElement.
     * @param parent SOAPElement to add the given Node to
     * @param child org.w3c.dom.Node that will be cloned and then added to the
     * given parent, so the original given child is not changed in any way. Can be
     * {@code null} in which case this method does nothing and leaves the parent
     * untouched.
     * @see #buildHeader
     * @see #buildBody
     */
    private void appendClonedChild(final SOAPElement parent, final Node child) {
        if (child != null) {
            // copy/clone node to be safe
            Node clone = parent.getOwnerDocument().importNode(child, true);
            parent.appendChild(clone);
        }
    }

    /**
     * Perform the actual service invocation and return the response SOAPMessage.
     * @param dispatch Dispatch to use for sending the request.
     * @param request SOAP request to send
     * @return response SOAPMessage
     */
    protected SOAPMessage invokeService(final Dispatch<SOAPMessage> dispatch, final SOAPMessage request) {
        switch (getInvocationType()) {
        case SYNCHRONOUS:
            return dispatch.invoke(request);
        case ONE_WAY:
            dispatch.invokeOneWay(request);
            return null;
        default:
            throw new IllegalArgumentException("unsupported invocation type: " + getInvocationType());
        }
    }

    // ----- Builder pattern (setters return this) -----

    /**
     * Get the name for the Service to create.
     * @return explicit value as set by setServiceName or default DFLT_SERVICE_NAME if none has been set
     * @see #setServiceName
     * @see #DFLT_SERVICE_NAME
     */
    public QName getServiceName() {
        return serviceName;
    }

    /**
     * Explicitly sets the name for the Service to create.
     * @param name QName
     * @return this WSClient so the invoker can call number of setters on a single line by chaining them together
     * @see #getServiceName
     * @see #DFLT_SERVICE_NAME
     */
    public WSClient setServiceName(final QName name) {
        this.serviceName = name;
        return this;
    }

    /**
     * Get the name for the Port to create within the Service.
     * @return explicit value as set by setPortName or default DFLT_SERVICE_PORT if none has been set
     * @see #setPortName
     * @see #DFLT_SERVICE_PORT
     */
    public QName getPortName() {
        return portName;
    }

    /**
     * Explicitly sets the name for the Port to create within the Service.
     * @param name QName
     * @return this WSClient so the invoker can call number of setters on a single line by chaining them together
     * @see #getPortName
     * @see #DFLT_SERVICE_PORT
     */
    public WSClient setPortName(final QName name) {
        this.portName = name;
        return this;
    }

    /**
     * Get the enpoint URL for the service to invoke.
     * @return value as set by setEndPointUrl, setEndPointConnection or setEndPointConnectionName or {@code null}
     * if none has been set yet although the service invocation would obviously fail without a url.
     * @see #setEndPointUrl
     * @see #setEndPointConnection
     * @see #setEndPointConnectionName
     */
    public String getEndPointUrl() {
        return endPointUrl;
    }

    /**
     * Sets the endpoint URL for the service to invoke.
     * @param url full URL of the service endpoint
     * @return this WSClient so the invoker can call number of setters on a single line by chaining them together
     * @see #getEndPointUrl
     * @see #setEndPointConnection
     * @see #setEndPointConnectionName
     */
    public WSClient setEndPointUrl(final String url) {
        this.endPointUrl = url;
        return this;
    }

    /**
     * Sets the endpoint for the service to invoke using an ADF Connection name.
     * @param adfConnectionName name of a valid ADF Connection that should be available through
     * ADFContext.getConnectionsContext
     * @return this WSClient so the invoker can call number of setters on a single line by chaining them together
     * @see #getEndPointUrl
     * @see #setEndPointUrl
     * @see #setEndPointConnection
     * @see ADFContext#getConnectionsContext
     */
    public WSClient setEndPointConnectionName(final String adfConnectionName) {
        if (adfConnectionName == null || adfConnectionName.isEmpty()) {
            return setEndPointConnection(null);
        }
        try {
            Context ctx = ADFContext.getCurrent().getConnectionsContext();
            Object obj = ctx.lookup(adfConnectionName);
            return setEndPointConnection((URLConnection) obj);
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Sets the endpoint for the service to invoke using an ADF Connection.
     * @param connection valid ADF Connection that should be available through ADFContext.getConnectionsContext
     * @return this WSClient so the invoker can call number of setters on a single line by chaining them together
     * @see #getEndPointUrl
     * @see #setEndPointUrl
     * @see #setEndPointConnectionName
     * @see ADFContext#getConnectionsContext
     */
    public WSClient setEndPointConnection(final URLConnection connection) {
        // TODO: use other connection properties (credentials, proxy, timeout)
        // not sure if we can get JAX-WS to use HttpClient.HTTPConnection
        // we can get from ADF Connection
        setEndPointUrl(connection == null ? null : connection.getURL().toExternalForm());
        return this;
    }

    /**
     * Get the connection timeout in milliseconds.
     * @return explicit value as set by setConnectTimeout or {@code null} if none has been set
     * @see #setConnectTimeout
     */
    public Integer getConnectTimeout() {
        return connectTimeout;
    }

    /**
     * Explicitly sets the connection timeout in milliseconds.
     * @param milliseconds connection timeout in milliseconds or -1 to disable timeout and wait forever
     * @return this WSClient so the invoker can call number of setters on a single line by chaining them together
     * @see #getConnectTimeout
     */
    public WSClient setConnectTimeout(final int milliseconds) {
        this.connectTimeout = milliseconds;
        return this;
    }

    /**
     * Get the total request timeout in milliseconds.
     * @return explicit value as set by setRequestTimeout or {@code null} if none has been set
     * @see #setRequestTimeout
     */
    public Integer getRequestTimeout() {
        return requestTimeout;
    }

    /**
     * Explicitly sets the total request timeout in milliseconds.
     * @param milliseconds total request timeout in milliseconds or -1 to disable timeout and wait forever
     * @return this WSClient so the invoker can call number of setters on a single line by chaining them together
     * @see #getRequestTimeout
     */
    public WSClient setRequestTimeout(final int milliseconds) {
        this.requestTimeout = milliseconds;
        return this;
    }

    /**
     * Get the SOAP action URI that should be sent as a http request header.
     * @return explicit value as set by setSoapActionUri or {@code null} if none has been set
     * @see #setSoapActionUri
     */
    public String getSoapActionUri() {
        return soapActionUri;
    }

    /**
     * Explicitly sets the SOAP action URI that should be sent as a http request header.
     * @param uri SOAP action URI or {@code null} to clear any previously set action uri
     * @return this WSClient so the invoker can call number of setters on a single line by chaining them together
     * @see #getSoapActionUri
     */
    public WSClient setSoapActionUri(final String uri) {
        this.soapActionUri = uri;
        return this;
    }

    /**
     * Get the SOAP binding that should be used to create a Port in the javax.xml.ws.Service.
     * @return explicit value as set by setSoapBindingId or setSoapVersion or the binding from SoapVersion.V1_1
     * as default if none has been set
     * @see #setSoapActionUri
     * @see SoapVersion#V1_1
     */
    public String getSoapBindingId() {
        return soapBindingId;
    }

    /**
     * Explicitly sets the SOAP binding that should be used to create a Port in the javax.xml.ws.Service.
     * @param id binding id which normally is the return of getBinding in one the SoapVersion enums but in
     * those cases invoking setSoapVersion is easier and this setSoapBindingId method could be used for cases
     * not supported by the SoapVersion enum.
     * @return this WSClient so the invoker can call number of setters on a single line by chaining them together
     * @see #getSoapBindingId
     * @see #setSoapVersion
     */
    public WSClient setSoapBindingId(final String id) {
        this.soapBindingId = id;
        return this;
    }

    /**
     * Explicitly sets the SOAP binding that should be used to create a Port in the javax.xml.ws.Service
     * based on the given SoapVersion.
     * @param version SoapVersion which we call getBinding() on to get the binding id for setSoapBindingId
     * @return this WSClient so the invoker can call number of setters on a single line by chaining them together
     * @see #getSoapBindingId
     * @see #setSoapBindingId
     * @see SoapVersion#getBinding
     */
    public WSClient setSoapVersion(final SoapVersion version) {
        this.soapBindingId = version.getBinding();
        return this;
    }

    /**
     * Get the invocation type for this service call.
     * @return explicit value as set by setInvocationType or {@code InvocationType.SYNCHRONOUS} as default if none
     * has been set
     * @see #setInvocationType
     */
    public InvocationType getInvocationType() {
        return invocationType;
    }

    /**
     * Explicitly sets the invocation type for this service call.
     * @param type InvocationType
     * @return this WSClient so the invoker can call number of setters on a single line by chaining them together
     * @see #getInvocationType
     */
    public WSClient setInvocationType(final InvocationType type) {
        this.invocationType = type;
        return this;
    }

    /**
     * Get the HandlerResolver that should supply the Handlers for this service call.
     * @return explicit HandlerResolver as set by setHandlerResolver or a default HandlerResolver returning a
     * single LoggingSoapHandler if no explicit HandlerResolver has been set
     * @see #setHandlerResolver
     */
    public HandlerResolver getHandlerResolver() {
        return handlerResolver;
    }

    /**
     * Explicitly sets the HandlerResolver for this service call.
     * @param resolver HandlerResolver
     * @return this WSClient so the invoker can call number of setters on a single line by chaining them together
     * @see #getHandlerResolver
     */
    public WSClient setHandlerResolver(final HandlerResolver resolver) {
        this.handlerResolver = resolver;
        return this;
    }

    /**
     * Get the xml elements to add to the soap header which can be an empty list to indicate no information should
     * be added to the soap header.
     * @return explicit value as set by setRequestHeaderElements or empty list as default if none has been set
     * @see #setRequestHeaderElements
     */
    public List<Element> getRequestHeaderElements() {
        return Collections.unmodifiableList(requestHeader);
    }

    /**
     * Explicitly sets the soap header elements for this service call.
     * @param headerElements elements to add to the soap header
     * @return this WSClient so the invoker can call number of setters on a single line by chaining them together
     * @see #getRequestHeaderElements
     */
    public WSClient setRequestHeaderElements(final List<Element> headerElements) {
        requestHeader = new ArrayList<Element>(headerElements);
        return this;
    }

    /**
     * Get the xml elements to add to the soap body which can be an empty list to indicate no information should
     * be added to the soap body.
     * @return explicit value as set by setRequestBodyElements or empty list as default if none has been set
     * @see #setRequestBodyElements
     */
    public List<Element> getRequestBodyElements() {
        return Collections.unmodifiableList(requestBody);
    }

    /**
     * Explicitly sets the soap body elements for this service call.
     * @param bodyElements elements to add to the soap body
     * @return this WSClient so the invoker can call number of setters on a single line by chaining them together
     * @see #getRequestBodyElements
     */
    public WSClient setRequestBodyElements(final List<Element> bodyElements) {
        requestBody = new ArrayList<Element>(bodyElements);
        return this;
    }

    // ----- END Builder pattern (setters return this) -----

    /**
     * Enum for supported SOAP versions.
     */
    public static enum SoapVersion {
        V1_1(SOAPBinding.SOAP11HTTP_BINDING),
        V1_2(SOAPBinding.SOAP12HTTP_BINDING);
        private String binding;

        private SoapVersion(final String binding) {
            this.binding = binding;
        }

        /**
         * Gets the javax.xml.ws.soap.SOAPBinding to use for this soap version.
         * @return one of the java.xml.ws.soap.SOAPBinding binding constants that can be used
         * in creation of a Port on javax.xml.ws.Service
         */
        public String getBinding() {
            return binding;
        }

        /**
         * Return the enum constant belonging to the given binding string.
         * @param bindingId binding string to find the enum constant for
         * @return found enum, or {@code null} if the given binding does not map to any of the
         * known enums
         * @see #getBinding
         */
        public static SoapVersion fromBinding(final String bindingId) {
            for (SoapVersion v : SoapVersion.values()) {
                if (v.binding.equals(bindingId)) {
                    return v;
                }
            }
            return null;
        }
    }

    /**
     * Different ways to invoke a service that should lead to different methods being called
     * on javax.xml.ws.Dispatch.
     */
    public static enum InvocationType {
        SYNCHRONOUS,
        ASYNCHRONOUS,
        ONE_WAY;
    }

    /**
     * Default HandlerResolver which will return a single LoggingSoapHandler handler.
     * Will be used by WSClient if no explicit HandlerResolver has been set.
     */
    private static class DefaultHandlerResolver implements HandlerResolver {
        private static final LoggingSoapHandler handler = new LoggingSoapHandler();

        /**
         * Gets the list of handlers to use for the given port.
         * @param portInfo port to get the handlers for which is ignored in this default
         * implementation since it always returns the same (single item) list.
         * @return list with a single LoggingSoapHandler entry
         */
        @Override
        public List<Handler> getHandlerChain(PortInfo portInfo) {
            return Collections.<Handler>singletonList(handler);
        }
    }

}
