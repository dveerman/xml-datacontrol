/**
 * Data Providers provide the XML Element at runtime that contains the data the XML DataControl should work on.
 */
package org.adfemg.datacontrol.xml.provider.data;

