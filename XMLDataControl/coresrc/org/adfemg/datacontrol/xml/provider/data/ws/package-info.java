/**
 * Supporting classes for the Web Service Data Provider that invokes a SOAP web service to retrieve the XML Element
 * that XML DataControl should operate on.
 */
package org.adfemg.datacontrol.xml.provider.data.ws;

