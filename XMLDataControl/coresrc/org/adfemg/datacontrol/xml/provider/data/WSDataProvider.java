package org.adfemg.datacontrol.xml.provider.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;

import oracle.adf.model.adapter.AdapterException;
import oracle.adf.share.logging.ADFLogger;

import org.adfemg.datacontrol.xml.design.DesignTimeUtils;
import org.adfemg.datacontrol.xml.design.MessageUtils;
import org.adfemg.datacontrol.xml.provider.ProviderImpl;
import org.adfemg.datacontrol.xml.provider.data.ws.HandlerResolverImpl;
import org.adfemg.datacontrol.xml.provider.data.ws.LoggingSoapHandler;
import org.adfemg.datacontrol.xml.provider.data.ws.WSClient;
import org.adfemg.datacontrol.xml.utils.ClassUtils;
import org.adfemg.datacontrol.xml.utils.DomUtils;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;


/**
 * The WebService DataProvider that invokes a soap webservice using JAX-WS and returns the first org.w3c.dom.Element
 * child of the body of the soap response.
 * <p>Builds up the request based on the configuration of the DataControl in the DataControl.dcx.
 * @see org.adfemg.datacontrol.xml.DataControl
 * @see WSClient
 */
public class WSDataProvider extends ProviderImpl implements DataProvider {

    /**
     * The name of the DataControls.dcx property for the endpoint-url of the WebService.
     */
    public static final String PARAM_END_POINT_URL = "endPointUrl";

    /**
     * The name of the DataControls.dcx property for the URL Endpoint connection of the WebService.
     */
    public static final String PARAM_END_POINT_CONNECTION = "endPointConnection";

    /**
     * The name of the DataControls.dcx property for the (optional) SOAPAction http request header.
     */
    public static final String PARAM_SOAP_ACTION_URI = "soapAction";

    /**
     * The name of the DataControls.dcx property for the (optional) soapVersion (1.1 or 1.2). When
     * not specified the more common 1.1 will be used.
     */
    public static final String PARAM_SOAP_VERSION = "soapVersion";

    /**
     * Default soap version to use if no explicit value for soapVersion parameter is set.
     */
    public static final WSClient.SoapVersion DFLT_SOAP_VERSION = WSClient.SoapVersion.V1_1;

    /**
     * The name of the property to specify an (optional) implementation of javax.xml.ws.handler.HandlerResolver
     * which can be used to further customize the webservice request and/or response.
     */
    public static final String PARAM_HANDLER_RESOLVER = "handlerResolverClass";

    /**
     * Default HandlerResolver if no explicit value for the handlerResolverClass parameter has been set.
     */
    public static final String DFLT_HANDLER_RESOLVER = HandlerResolverImpl.class.getName();

    /**
     * The name of the DataControls.dcx property for the SOAP Body Request Element for the WebService call.
     */
    public static final String PARAM_REQUEST_ELEM = "requestElement";

    /**
     * The name of the DataControls.dcx property for the (optional) SOAP Header Element for the WebService call.
     */
    public static final String PARAM_HEADER_ELEM = "headerElement";

    /**
     * The name of the DataControls.dcx property for the list of SoapHandlers.
     */
    public static final String PARAM_SOAP_HANDLERS = "soapHandlers";

    /**
     * Default list of SoapHandlers to use if no explicit value for the soapHandlers parameter has been set.
     */
    public static final List<String> DFLT_SOAP_HANDLERS = Collections.singletonList(LoggingSoapHandler.class.getName());

    /**
     * The name of the DataControls.dcx property for the invocation-type (oneWay or Synchronous).
     */
    public static final String PARAM_INVOCATION_TYPE = "invocationType";

    /**
     * Name of the DataControls.dcx parameter to specify the connect timeout in milliseconds or -1 to disable timeout
     * and wait forever.
     */
    public static final String PARAM_CONNECT_TIMEOUT = "connectTimeout";

    /**
     * Name of the DataControls.dcx parameter to specify the total request timeout in milliseconds or -1 to disable
     * timeout and wait forever.
     */
    public static final String PARAM_REQUEST_TIMEOUT = "requestTimeout";

    private static final ADFLogger logger = ADFLogger.createADFLogger(WSDataProvider.class);

    /**
     * Invoke the soap web service and return the first org.w3c.dom.Element child of the soap response
     * as result of this DataProvider.
     * @param request the datacontrol request
     * @return The soap response body root element as an org.w3c.dom.Element.
     */
    @Override
    public Element getRootElement(final DataRequest request) {
        try {
            final WSClient client = buildClient(request);
            SOAPMessage response = client.invoke();
            return processResponse(response);
        } catch (WebServiceException e) { // could be connect or request timeout
            logger.severe("web service exception while invoking web service", e);
            throw e;
        } catch (SOAPException e) {
            // TODO: include fully qualified WSDataProvider name so we now which DataControl this is from
            logger.severe("soap exception invoking web service", e);
            throw new AdapterException(e);
        }
    }

    /**
     * Build the WSClient object that will be used to invoke the web service.
     * @param request the datacontrol request
     * @return WSClient instance
     */
    protected WSClient buildClient(final DataRequest request) {
        final WSClient retval = new WSClient().setEndPointUrl(getEndPointUrl(request));
        final String connName = getEndPointConnectionName(request);
        if (!StringUtils.isEmpty(connName)) {
            // prevent invoke with null as that would clear endpointUrl on WSClient
            retval.setEndPointConnectionName(getEndPointConnectionName(request));
        }
        final Integer connectTimeout = getConnectTimeout(request);
        if (connectTimeout != null) {
            retval.setConnectTimeout(connectTimeout);
        }
        final Integer requestTimeout = getRequestTimeout(request);
        if (requestTimeout != null) {
            retval.setRequestTimeout(requestTimeout);
        }
        retval.setSoapActionUri(getSoapActionUri(request));
        retval.setSoapVersion(getSoapVersion(request));
        retval.setInvocationType(getInvocationType(request));
        retval.setHandlerResolver(getHandlerResolver(request));
        retval.setRequestHeaderElements(getRequestHeaderElements(request));
        retval.setRequestBodyElements(getRequestBodyElements(request));
        return retval;
    }

    /**
     * Process the response of the WSClient invocation to an org.w3c.dom.Element that can
     * be returned by this DataProvider to its data control.
     * @param response soap response as returned by WSClient.invoke
     * @return first org.w3c.dom.Element child of the SOAPBody of the given SOAPMessage, or {@code null}
     * if the input response was {@code null}
     * @throws SOAPException
     */
    protected Element processResponse(final SOAPMessage response) throws SOAPException {
        if (response == null) {
            return null;
        }
        SOAPBody soapBody = response.getSOAPBody();
        SOAPElement firstChild = (SOAPElement) DomUtils.findFirstChildElement(soapBody);
        return DomUtils.soapToDomElement(firstChild);
    }

    // ----- Expose WSDataProvider parameters to WSDataProvider.buildClient -----

    /**
     * Returns the endPointUrl parameter.
     * @param request data control request with dynamic parameter values
     * @return value of the endPointUrl dataprovider parameter or {@code null} if this was not set
     * @see #PARAM_END_POINT_URL
     */
    protected String getEndPointUrl(final DataRequest request) {
        return getStringParameter(PARAM_END_POINT_URL, request, WSDataProvider.class, null);
    }

    /**
     * Returns the endPointConnection parameter.
     * @param request data control request with dynamic parameter values
     * @return value of the endPointConnection dataprovider parameter or {@code null} if this was not set
     * @see #PARAM_END_POINT_CONNECTION
     */
    protected String getEndPointConnectionName(final DataRequest request) {
        return getStringParameter(PARAM_END_POINT_CONNECTION, request, WSDataProvider.class, null);
    }

    /**
     * Returns the connectTimeout parameter specifying the timeout for setting up a connection in milliseconds.
     * @param request data control request with dynamic parameter values
     * @return value of the connectTimeout dataprovider parameter or {@code null} if this value was not set
     * @see #PARAM_CONNECT_TIMEOUT
     */
    protected Integer getConnectTimeout(final DataRequest request) {
        final String str = getStringParameter(PARAM_CONNECT_TIMEOUT, request, WSDataProvider.class, null);
        return str == null ? null : Integer.valueOf(str);
    }

    /**
     * Returns the requestTimeout parameter specifying the total request duration timeout in milliseconds.
     * @param request data control request with dynamic parameter values
     * @return value of the requestTimeout dataprovider parameter or {@code null} if this value was not set
     * @see #PARAM_REQUEST_TIMEOUT
     */
    protected Integer getRequestTimeout(final DataRequest request) {
        final String str = getStringParameter(PARAM_REQUEST_TIMEOUT, request, WSDataProvider.class, null);
        return str == null ? null : Integer.valueOf(str);
    }

    /**
     * Returns the soapAction parameter.
     * @param request data control request with dynamic parameter values
     * @return value of the soapAction dataprovider parameter or {@code null} if this was not set
     * @see #PARAM_SOAP_ACTION_URI
     */
    protected String getSoapActionUri(final DataRequest request) {
        return getStringParameter(PARAM_SOAP_ACTION_URI, request, WSDataProvider.class, null);
    }

    /**
     * Returns the soapVersion parameter.
     * @param request data control request with dynamic parameter values
     * @return value of the soapVersion dataprovider parameter or {@code SoapVersion.V1_1} if this was not set
     * @see #PARAM_SOAP_VERSION
     * @see org.adfemg.datacontrol.xml.provider.data.ws.WSClient.SoapVersion
     */
    protected WSClient.SoapVersion getSoapVersion(final DataRequest request) {
        return "1.2".equals(getStringParameter(PARAM_SOAP_VERSION, request, WSDataProvider.class, null)) ?
               WSClient.SoapVersion.V1_2 : WSClient.SoapVersion.V1_1;
    }

    /**
     * Returns the invocationType parameter.
     * @param request data control request with dynamic parameter values
     * @return value of the invocationType dataprovider parameter or {@code InvocationType.ONE_WAY} if this was not set
     * @see #PARAM_INVOCATION_TYPE
     */
    protected WSClient.InvocationType getInvocationType(final DataRequest request) {
        return "oneWay".equalsIgnoreCase(getStringParameter(PARAM_INVOCATION_TYPE, request, WSDataProvider.class,
                                                            null)) ? WSClient.InvocationType.ONE_WAY :
               WSClient.InvocationType.SYNCHRONOUS;
    }

    /**
     * Returns the handlerResolverClass parameter.
     * @param request data control request with dynamic parameter values
     * @return instance of the class specified by the handlerResolverClass dataprovider parameter or HandlerResolverImpl
     * if this was not set
     * @see #PARAM_HANDLER_RESOLVER
     */
    protected HandlerResolver getHandlerResolver(final DataRequest request) {
        String resolver =
            getStringParameter(PARAM_HANDLER_RESOLVER, request, WSDataProvider.class, DFLT_HANDLER_RESOLVER);
        HandlerResolver retval = (HandlerResolver) ClassUtils.newInstance(resolver);
        if (retval instanceof HandlerResolverImpl) {
            ((HandlerResolverImpl) retval).setDataProvider(this);
            ((HandlerResolverImpl) retval).setRequest(request);
        }
        return retval;
    }

    /**
     * Returns the headerElement parameter.
     * @param request data control request with dynamic parameter values
     * @return single-item list with the value of the headerElement dataprovider parameter or an empty list
     * if the headerElement was not set
     * @see #PARAM_HEADER_ELEM
     */
    protected List<Element> getRequestHeaderElements(final DataRequest request) {
        final Node header = getXmlParameter(PARAM_HEADER_ELEM, request, WSDataProvider.class, null);
        return (header != null && header.getNodeType() == Element.ELEMENT_NODE) ?
               Collections.singletonList((Element) header) : Collections.<Element>emptyList();
    }

    /**
     * Returns the requestElement parameter.
     * @param request data control request with dynamic parameter values
     * @return single-item list with the value of the requestElement dataprovider parameter
     * @throws IllegalArgumentException when mandatory requestElement parameter is unknown
     * @see #PARAM_REQUEST_ELEM
     */
    protected List<Element> getRequestBodyElements(final DataRequest request) {
        final Node n = getXmlParameter(PARAM_REQUEST_ELEM, request, WSDataProvider.class, null);
        if (n == null) {
            throw new IllegalArgumentException(PARAM_REQUEST_ELEM + " argument is unknown or empty.");
        }
        return Collections.singletonList((Element) n);
    }

    /**
     * Returns the list of soap handlers specified by the soapHandlers parameter.
     * @param request data control request with dynamic parameter values
     * @return list of instances of the classes specified by the soapHandlers dataprovider parameter or a list with
     * a single LoggingSoapHandler if this was not specified
     * @see #PARAM_SOAP_HANDLERS
     * @see LoggingSoapHandler
     */
    public List<Handler> getHandlers(final DataRequest request) {
        final List<String> handlerClasses =
            getListParameter(PARAM_SOAP_HANDLERS, request, WSDataProvider.class, DFLT_SOAP_HANDLERS);
        final List<Handler> retval = new ArrayList<Handler>(handlerClasses.size());
        for (String className : handlerClasses) {
            try {
                final Class cls = ClassUtils.loadClass(className);
                if (!Handler.class.isAssignableFrom(cls)) {
                    final String errMsg = "class " + className + " does not implement " + Handler.class.getName();
                    if (DesignTimeUtils.isGraphicalDesignTime()) {
                        MessageUtils.showErrorMessage("XML DataControl", errMsg);
                    } else {
                        throw new IllegalArgumentException(errMsg);
                    }
                }
                retval.add((Handler) cls.newInstance());
            } catch (Exception e) {
                if (DesignTimeUtils.isGraphicalDesignTime()) {
                    MessageUtils.showErrorMessage("class load error", e.getMessage());
                } else {
                    throw new RuntimeException(e);
                }
            }
        }
        return retval;
    }

    // ----- END of getters for WSDataProvider parameters -----

}
