package org.adfemg.datacontrol.xml.provider.structure.lazy;

import oracle.binding.meta.DefinitionContainer;
import oracle.binding.meta.EmptyDefinitionContainer;
import oracle.binding.meta.NamedDefinition;

import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

public abstract class AbstractOperationsDefinition extends AbstractStructureDefinition {

    public AbstractOperationsDefinition(NamedDefinition parent, String fullName, TypeMapper typeMapper) {
        super(parent, fullName, typeMapper);
    }

    @Override
    public DefinitionContainer getAttributeDefinitions() {
        return EmptyDefinitionContainer.getInstance();
    }

    @Override
    public DefinitionContainer getAccessorDefinitions() {
        return EmptyDefinitionContainer.getInstance();
    }

    @Override
    public DefinitionContainer getConstructorOperationDefinitions() {
        return EmptyDefinitionContainer.getInstance();
    }

    @Override
    public DefinitionContainer getCriteriaDefinitions() {
        return EmptyDefinitionContainer.getInstance();
    }
}
