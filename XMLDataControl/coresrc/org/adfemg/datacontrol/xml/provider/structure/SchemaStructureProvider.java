package org.adfemg.datacontrol.xml.provider.structure;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.xml.namespace.QName;

import oracle.adf.model.adapter.dataformat.AccessorDef;
import oracle.adf.model.adapter.dataformat.AttributeDef;
import oracle.adf.model.adapter.utils.Utility;
import oracle.adf.model.utils.JSR227Util;
import oracle.adf.model.utils.StandardOperationDef;

import oracle.binding.meta.StructureDefinition;

import oracle.xml.parser.schema.XSDAttribute;
import oracle.xml.parser.schema.XSDComplexType;
import oracle.xml.parser.schema.XSDConstantValues;
import oracle.xml.parser.schema.XSDElement;
import oracle.xml.parser.schema.XSDGroup;
import oracle.xml.parser.schema.XSDNode;
import oracle.xml.parser.schema.XSDSimpleType;

import org.adfemg.datacontrol.xml.DataControlDefinition;
import org.adfemg.datacontrol.xml.LeafNodeType;
import org.adfemg.datacontrol.xml.XMLDCConstants;
import org.adfemg.datacontrol.xml.design.DesignTimeUtils;
import org.adfemg.datacontrol.xml.design.MessageUtils;
import org.adfemg.datacontrol.xml.provider.ProviderImpl;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;


/**
 * inspiratie onder andere van:
 * <ul>
 *   <li>oracle.adf.model.adapter.dataformat.XSDHandler</li>
 *   <li>oracle.adfinternal.model.adapter.bean.BeanDCUtils</li>
 * </ul>.
 */
public final class SchemaStructureProvider extends ProviderImpl implements StructureProvider {

    ////////// constanten voor leesbare code //////////
    private static final boolean READONLY_FALSE = false;
    private static final boolean KEY_FALSE = false;
    private static final boolean COLLECTION_FALSE = false;
    private static final boolean SCALAR_COLLECTION_TRUE = true;
    private static final boolean SCALAR_COLLECTION_FALSE = false;
    private static final StructureDefinition STRUCTURE_NULL = null;

    private static final String SIMPLETYPE_ATTRIBUTE = "_value";
    public static final String SCALAR_COLLECTION_POSTFIX = "Collection";

    private TypeMapper typeMapper; // TypeMapper to translate XSD types to Java types

    // private static final ADFLogger logger = ADFLogger.createADFLogger(XSDProcessor.class);

    //////////////////// Constructor ////////////////////

    private TypeMapper getTypeMapper() {
        return typeMapper;
    }

    protected void resolveElement(final XSDElement element, final MovableStructureDef parent) {
        XSDNode type = element.getType();
        boolean noType = false;
        boolean explicitAnyType = false;
        if (isAnyType(type)) {
            if (hasTypeAttribute(element)) {
                explicitAnyType = true;
            } else { // elementen zonder type krijgen een XSDComplexType anyType. Deze negeren
                // we om onderscheid te kunnen maken met elementen die expliciet op
                // type=anyType zijn gezet
                noType = true;
                type = null;
            }
        }
        final String safeElementName = Utility.normalizeString(element.getName());
        final boolean collection = isCollection(element);

        if (type instanceof XSDSimpleType || noType || explicitAnyType) { // simpletype, geen type (dus string) en anytype moeten allemaal
            // attribuut worden
            AccessorDef collAccessor = null;
            MovableStructureDef attrParent = parent;
            LeafNodeType leafNodeType = LeafNodeType.ELEMENT;
            if (collection) { // collectie van attributen kan niet, dus collection accessor maken met
                // "fake" structure
                // TODO: beter beschrijven scalar collection??
                collAccessor =
                    createDCAccessor(safeElementName + SCALAR_COLLECTION_POSTFIX, parent, collection,
                                     SCALAR_COLLECTION_TRUE);
                addAccessorProperties(collAccessor, element);
                //createCompound(type, accessorDef); // maak StructureDefinition
                // maak StructureDefinition met enkel attribuut
                final MovableStructureDef collStructure = createDCStructure(collAccessor);
                // attribute voor element laten aanmaken in nieuwe structureDef en niet
                // in gegeven parent (waar nu een accessor en andere structure in
                // geplaatst zijn)
                attrParent = collStructure;
                leafNodeType = LeafNodeType.SCALAR_COLLECTION_ELEMENT;
            }
            // TODO: kunnen we code delen met resolveElement bij een simpleType???
            // TODO: kunnen we code delen met collectie van simpleType???
            // attribuut maken en deze toe voegen aan gegeven parent of "fake"
            // parent die we net gemaakt hebben igv collectie
            // wanneer expliciet anyType is opgegeven dan hele XML element exposen
            if (explicitAnyType) {
                // hardcode this to expose xsd:anyType as org.w3c.dom.Element to java. The reverse is
                // hardcode in XMLDCElement.get that does not use TypeMapper to make this conversion but simply
                // clones the source anyType element and returns that to the binding layer.
                final AttributeDef attrDef = createDCAttribute(safeElementName, attrParent, "org.w3c.dom.Element");
                addAttributeProperties(attrDef, element, type.getQName(), leafNodeType);
            } else {
                final XSDSimpleType simpleType = (XSDSimpleType) type;
                final AttributeDef attrDef =
                    createDCAttribute(safeElementName, attrParent, getTypeMapper().getJavaType(simpleType));
                addAttributeProperties(attrDef, element, getTypeMapper().getMappableType(simpleType), leafNodeType);
            }
        } else if (type instanceof XSDComplexType) { // complexType (niet anyType) als accessor met structure maken
            final XSDComplexType complexType = (XSDComplexType) type;
            final AccessorDef accessorDef =
                createDCAccessor(safeElementName, parent, collection, SCALAR_COLLECTION_FALSE);
            addAccessorProperties(accessorDef, element);
            final MovableStructureDef structDef =
                resolveComplexType(complexType, accessorDef); // maak StructureDefinition
            addStructureProperties(structDef, complexType);
        } else {
            throw new UnsupportedOperationException("XSDElement with type " + type.getClass().getName());
        }

    }

    /**
     * Maak een StructureDef voor gegeven XSDComplexType, plaats deze in gegeven
     * parent AccessorDef en maak attributen/accessors voor inhoud van de
     * XSDComplexType.
     */
    protected MovableStructureDef resolveComplexType(final XSDComplexType type, final AccessorDef accessor) {
        // naam is belangrijk want daarmee moeten we een StructureDefinition kunnen
        // terugvinden binnen de gehele datacontrol
        final MovableStructureDef structDef = createDCStructure(accessor);
        addStructureProperties(structDef, type);

        if (type.isSimpleContent()) {
            // Fake attribute _value for the SimpleContent for this complexType
            final XSDSimpleType simpleType = type.getSimpleType();
            final AttributeDef attrDef =
                createDCAttribute(SIMPLETYPE_ATTRIBUTE, structDef, getTypeMapper().getJavaType(simpleType));
            addAttributeProperties(attrDef, /*attrOrElem*/null, getTypeMapper().getMappableType(simpleType),
                                   LeafNodeType.ELEMENT);
        }
        for (XSDAttribute attribute : type.getAttributeDeclarations()) {
            createScalar(attribute, structDef);
        }

        XSDGroup group = type.getTypeGroup();
        if (group != null) {
            createScalar(group, structDef);
        } else {
            final XSDNode[] elements = type.getElementSet();
            if (elements != null) {
                for (XSDNode node : elements) {
                    createScalar(node, structDef);
                }
            }
        }

        return structDef;
    }

    protected AttributeDef resolveAttribute(final XSDAttribute attribute, final MovableStructureDef parent) {
        // TODO: kunnen we code delen met resolveElement bij een simpleType???

        XSDSimpleType simpleType = (XSDSimpleType) attribute.getType();
        if (isAnyType(simpleType) && !hasTypeAttribute(attribute)) {
            simpleType = null;
        }
        final AttributeDef attrDef =
            createDCAttribute(Utility.normalizeString(attribute.getName()), parent,
                              getTypeMapper().getJavaType(simpleType));
        addAttributeProperties(attrDef, attribute, getTypeMapper().getMappableType(simpleType), LeafNodeType.ATTRIBUTE);
        return attrDef;
    }

    protected MovableStructureDef createDCStructure(final AccessorDef parent, final String fqName) {
        final MovableStructureDef structDef = new MovableStructureDef(fqName, parent);
        if (parent != null) {
            parent.setStructure(structDef);
        }
        return structDef;
    }

    protected MovableStructureDef createDCStructure(final AccessorDef parent) {
        return createDCStructure(parent, parent.getFullName());
    }

    protected AttributeDef createDCAttribute(final String name, final MovableStructureDef parent,
                                             final String javaType) {
        final AttributeDef attrDef = new AttributeDef(name, parent, javaType, READONLY_FALSE, KEY_FALSE);
        parent.addAttribute(attrDef);
        return attrDef;
    }

    protected AccessorDef createDCAccessor(final String name, final MovableStructureDef parent,
                                           final boolean collection, final boolean scalarCollection) {
        // maak initieel aan zonder structure. Deze wordt later toegevoegd door aanroeper
        final AccessorDef accessorDef = new AccessorDef(name, parent, STRUCTURE_NULL, collection, scalarCollection);
        accessorDef.setReadOnly(READONLY_FALSE);
        parent.addAccessor(accessorDef);
        // operations toevoegen
        if (collection) {
            buildCollectionOperations(accessorDef, READONLY_FALSE);
        } else {
            buildSingleValueOperations(accessorDef, READONLY_FALSE);
        }
        return accessorDef;
    }

    protected void buildSingleValueOperations(final AccessorDef accessor, final boolean readOnly) {
        final MovableStructureDef operations = new MovableStructureDef("Operations", accessor);
        accessor.setCollectionStructure(operations);
        if (!readOnly) {
            addParentedOperation(operations, JSR227Util.createCreateAction());
            addParentedOperation(operations, JSR227Util.createRemoveAction());
        }
        // bewust geen execute bij een 'live' datacontrol die gelijk in source kijkt
    }

    protected void buildCollectionOperations(final AccessorDef accessor, final boolean readOnly) {
        final MovableStructureDef operations = new MovableStructureDef("CollectionOperations", accessor);
        accessor.setCollectionStructure(operations);
        if (!readOnly) {
            addParentedOperation(operations, JSR227Util.createCreateAction());
            addParentedOperation(operations, JSR227Util.createRemoveAction());
        }
        addParentedOperation(operations, JSR227Util.createFirstAction());
        addParentedOperation(operations, JSR227Util.createLastAction());
        addParentedOperation(operations, JSR227Util.createPreviousAction());
        addParentedOperation(operations, JSR227Util.createNextAction());
        //We didn't see any need to support:
        //execute, createRemoveRowWithKeyAction, createSetCurrentRowWithKeyAction, createSetCurrentRowWithKeyValueAction
        //Because we didn't see any usecase for this (yet).
    }

    protected void addParentedOperation(final MovableStructureDef operationsDef, final StandardOperationDef operation) {
        operationsDef.addMethod(operation);
        operation.setParent(operationsDef);
    }

    protected void addAttributeProperties(final AttributeDef attribute, final XSDNode attrOrElem,
                                          final QName mappableType, final LeafNodeType leafType) {
        // TODO: moeten we iets doen enumerations zoals bijvoorbeeld de mogelijke waardes opnemen
        if (attrOrElem != null) {
            attribute.addProperty(DataControlDefinition.ATTRPROP_NAME, attrOrElem.getName());
            attribute.addProperty(DataControlDefinition.ATTRPROP_NAMESPACE, attrOrElem.getTargetNS());
        }
        attribute.addProperty(DataControlDefinition.ATTRPROP_TYPE, mappableType);
        attribute.addProperty(DataControlDefinition.ATTRPROP_LEAFNODETYPE, leafType);
    }

    protected void addAccessorProperties(final AccessorDef accessor, final XSDElement element) {
        if (element != null) {
            accessor.addProperty(DataControlDefinition.ACCPROP_NAME, element.getName());
            accessor.addProperty(DataControlDefinition.ACCPROP_NAMESPACE, element.getTargetNS());
            accessor.addProperty(DataControlDefinition.ACCPROP_MINOCCURS, element.getMinOccurs());
            accessor.addProperty(DataControlDefinition.ACCPROP_MAXOCCURS, element.getMaxOccurs());
            accessor.addProperty(DataControlDefinition.ACCPROP_NILLABLE, element.isNillable());
        }
    }

    protected void addStructureProperties(final MovableStructureDef structure, final XSDComplexType complexType) {
        if (complexType != null) {
            structure.addProperty(DataControlDefinition.STRUCTPROP_TYPE, complexType.getName());
            structure.addProperty(DataControlDefinition.STRUCTPROP_NAMESPACE, complexType.getTargetNS());
            final List<QName> childElemNames = new ArrayList<QName>();
            final XSDNode[] children = complexType.getChildElements();
            if (children != null) {
                for (XSDNode child : children) {
                    if (child instanceof XSDElement) {
                        childElemNames.add(child.getQName());
                    }
                }
            }
            structure.addProperty(DataControlDefinition.STRUCTPROP_CHILD_ELEMS, childElemNames);
        }
    }

    /**
     * Maakt attribuut of accessor voor alle elementen uit een XSDGroup
     * (xsd:all, xsd:sequence, xsd:choice).
     * @param group
     * @param parent
     */
    protected void resolveGroup(final XSDGroup group, final MovableStructureDef parent) {
        if (isCollection(group)) {
            String errorMessage = "A group with MaxOccurs > 1 is not supported in the XML DataControl.";
            if (DesignTimeUtils.isGraphicalDesignTime()) {
                MessageUtils.showErrorMessage("Not supported XSD configuration", errorMessage);
            } else {
                throw new UnsupportedOperationException(errorMessage);
            }
        }
        // TODO: Accessor for non-anonymous group??
        final Vector children = group.getNodeVector();
        if (children != null) {
            for (Object child : children) {
                createScalar((XSDNode) child, parent);
            }
        }
    }

    protected void createScalar(final XSDNode node, final MovableStructureDef parent) {
        // TODO: zouden we hier ook niet kunnen of moeten komen bij een ComplexType
        //       met SimpleContent??
        if (node instanceof XSDElement) {
            resolveElement((XSDElement) node, parent);
        } else if (node instanceof XSDAttribute) {
            resolveAttribute((XSDAttribute) node, parent);
        } else if (node instanceof XSDGroup) {
            resolveGroup((XSDGroup) node, parent);
        } else {
            throw new UnsupportedOperationException("createScalar cannot handle " + node.getClass().getName() + " " +
                                                    node.getQName() + " with parent " + parent.getClass().getName());
        }
    }

    @Override
    public MovableStructureDefinition buildStructure(XSDNode node, String structName, TypeMapper typeMapper) {
        this.typeMapper = typeMapper;
        // resolveComplexType requires an accessor as parent. As a workaround we create a temporary parent StructureDef
        // with a temporary AccessorDef. We can use these as parents for calling resolveComplexType. Once that is done
        // we remove the temporary AccessorDef as parent of the created StructureDef before returning it.
        final XSDComplexType complexType =
            (XSDComplexType) (node instanceof XSDElement ? ((XSDElement) node).getType() : node);
        final String parentName = structName.substring(0, structName.lastIndexOf('.'));
        final String accessorName = structName.substring(structName.lastIndexOf('.') + 1);
        final MovableStructureDef tempStruct = createDCStructure(null, parentName);
        final boolean collection = node instanceof XSDElement ? isCollection((XSDElement) node) : false;
        final AccessorDef tempAccessor =
            createDCAccessor(accessorName, tempStruct, collection, SCALAR_COLLECTION_FALSE);
        MovableStructureDefinition retval = (MovableStructureDefinition) resolveComplexType(complexType, tempAccessor);
        retval.setParent(null);
        return retval;
    }

    protected boolean isAnonymous(final XSDNode node) {
        return node.getQName() == null;
    }

    protected boolean isCollection(final XSDElement node) {
        return node.getMaxOccurs() > 1;
    }

    protected boolean isCollection(final XSDGroup group) {
        return group.getMaxOccurs() > 1;
    }

    protected boolean isGlobal(final XSDElement element) {
        return element.isElementGlobal();
    }

    protected boolean hasTypeAttribute(final XSDNode node) {
        final String type = node.getDomNode().getAttribute(XSDConstantValues._type);
        return (type != null && !type.isEmpty());
    }

    protected boolean isAnyType(final XSDNode type) {
        return (type instanceof XSDComplexType && XMLDCConstants.QNAME_SCHEMA_ANYTYPE.equals(type.getQName())) ||
               (type instanceof XSDSimpleType && XMLDCConstants.QNAME_SCHEMA_ANYSIMPLETYPE.equals(type.getQName()));
    }

}
