package org.adfemg.datacontrol.xml.provider.structure.lazy;

import oracle.binding.meta.NamedDefinition;
import oracle.binding.meta.StructureDefinition;

import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

public class RepeatingScalarAccessor extends AbstractAccessorDefinition {

    public static final String COLLECTION_POSTFIX = "Collection";

    private StructureDefinition structDef;

    public RepeatingScalarAccessor(NamedDefinition parent, TypeMapper typeMapper, AbstractAttributeDefinition attrDef) {
        super(parent, attrDef.getName() + COLLECTION_POSTFIX, typeMapper);
        structDef = new RepeatingScalarStructure(this, getTypeMapper(), attrDef);
    }

    @Override
    public boolean isCollection() {
        return true;
    }

    @Override
    public StructureDefinition getStructure() {
        return structDef;
    }

}
