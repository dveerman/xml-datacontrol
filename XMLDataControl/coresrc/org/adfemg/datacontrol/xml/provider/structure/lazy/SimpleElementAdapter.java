package org.adfemg.datacontrol.xml.provider.structure.lazy;

import oracle.binding.meta.NamedDefinition;

import oracle.xml.parser.schema.XSDElement;
import oracle.xml.parser.schema.XSDSimpleType;

import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

public class SimpleElementAdapter extends AbstractAttributeDefinition {

    final public XSDElement element;

    public SimpleElementAdapter(NamedDefinition parent, String simpleName, TypeMapper typeMapper, XSDElement element) {
        super(parent, simpleName, typeMapper);
        this.element = element;
    }

    @Override
    public String getJavaTypeString() {
        return getTypeMapper().getJavaType((XSDSimpleType) element.getType());
    }

}
