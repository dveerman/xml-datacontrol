package org.adfemg.datacontrol.xml.provider.structure.lazy;

import oracle.binding.meta.ArrayListDefinitionContainer;
import oracle.binding.meta.DefinitionContainer;
import oracle.binding.meta.EmptyDefinitionContainer;
import oracle.binding.meta.NamedDefinition;

import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

public class RepeatingScalarStructure extends AbstractStructureDefinition {

    private DefinitionContainer attributes;

    public RepeatingScalarStructure(NamedDefinition parent, TypeMapper typeMapper, AbstractAttributeDefinition attrDef) {
        super(parent, attrDef.getFullName(), typeMapper);
        ArrayListDefinitionContainer attrs = new ArrayListDefinitionContainer();
        attrs.add(attrDef);
        attrDef.setParent(this);
        this.attributes = attrs;
    }

    @Override
    public DefinitionContainer getAttributeDefinitions() {
        return attributes;
    }

    @Override
    public DefinitionContainer getAccessorDefinitions() {
        return EmptyDefinitionContainer.getInstance();
    }

    @Override
    public DefinitionContainer getOperationDefinitions() {
        return EmptyDefinitionContainer.getInstance();
    }

    @Override
    public DefinitionContainer getConstructorOperationDefinitions() {
        return EmptyDefinitionContainer.getInstance();
    }

    @Override
    public DefinitionContainer getCriteriaDefinitions() {
        return EmptyDefinitionContainer.getInstance();
    }

}
