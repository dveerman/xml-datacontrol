package org.adfemg.datacontrol.xml.provider.customization;

import oracle.adf.model.adapter.dataformat.MethodDef;

import org.adfemg.datacontrol.xml.provider.ProviderImpl;


/**
 * The Implementation of a null CustomizationProvider.
 * Will always returns a emptySet for the customizers.
 *
 * @see CustomizationProvider
 * @see ProviderImpl
 */
public class NullCustomizationProvider extends ProviderImpl implements CustomizationProvider {

    @Override
    public void customize(MethodDef method) {
        // do nothing
    }

}
