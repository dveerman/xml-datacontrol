package org.adfemg.datacontrol.xml.provider.customization;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import oracle.adf.model.adapter.dataformat.MethodDef;
import oracle.adf.share.logging.ADFLogger;

import org.adfemg.datacontrol.xml.annotation.ElementCustomization;
import org.adfemg.datacontrol.xml.cust.Customizer;
import org.adfemg.datacontrol.xml.java.JClass;
import org.adfemg.datacontrol.xml.java.JClassFactory;
import org.adfemg.datacontrol.xml.provider.ProviderImpl;

/**
 * CustomizationProvider that scans classpath for classes with @ElementCustomization annotation so
 * users don't have to list them explicitly. The actual scanning is performed by
 * JClassFactory.getAnnotatedClasses as that know how to do this at design time (within JDeveloper)
 * as well as run time (in WebLogic or any other environment). Read that documentation for the
 * details on how the scanning works as there are some restriction for efficiency, such that only
 * JAR files that contain a /META-INF/adfm.xml file are considered for scanning.
 * <p>The applying of the customizations is performed by invoking Customizer.customize so this can
 * be shared with other CustomizationProviders
 * <p><b>**** WARNING ****: this is still experimental and may lead to slow performance due to excessive
 * class path scanning. Somehow in larger applications this seems to continuously scan the classpath for
 * annotations. Until this is fixed the advice is to continue using CustomizationProviderImpl</b>
 * @see JClassFactory#getAnnotatedClasses
 * @see Customizer#customize
 * @see CustomizationProviderImpl
 */
public class CustomizationScanner extends ProviderImpl implements CustomizationProvider {

    /**
     * Name of the DataControls.dcx parameter that contains the resource that should exist in a JAR
     * to consider it for classpath scanning. We only scan JARs on the classpath that contain this
     * resource to prevent time consuming scanning of all JARs.
     * If you have JARs with XML DataControl customization classes that do not include an META-INF/adfm.xml file then
     * please include such a file just for this optimized scanning behavior or change the jar-trigger-resource parameter
     * to something known to be present in all JARs with customization classes.
     */
    public static final String PARAM_TRIGGER_RESOURCE = "jar-trigger-resource";

    /**
     * Default value for the jar-trigger-resource parameter which means we only scan JAR files on the
     * classpath that are known to contain ADF Model information.
     */
    public static final String DFLT_TRIGGER_RESOURCE = "META-INF/adfm.xml";

    /**
     * Name of the DataControls.dcx list-parameter with the JAR files known not to contain XML DataControl customization
     * classes so we can skip these during JAR scanning for enhanced performance.
     */
    public static final String PARAM_SKIP_JARS = "skip-jars";

    /**
     * Default value for the skip-jars list-parameter with some core Oracle ADF JAR files that do contain an
     * META-INF/adfm.xml which means they would be included in JAR scanning, but since we know they won't contain
     * XML DataControl customization classes we exclude them from this scanning process for increased performance.
     */
    public static final List<String> DFLT_SKIP_JARS =
        Arrays.asList("adf-customizationset-ui.jar", "adf-richclient-bootstrap.jar", "adf-businesseditor-model.jar",
                      "adf-sec-idm-dc.jar", "mds-dc.jar");

    // annotation class to scan for on each java class
    private static final JClass ANNOTATION = JClassFactory.forClass(ElementCustomization.class.getName());

    private static final ADFLogger logger = ADFLogger.createADFLogger(CustomizationScanner.class);

    /**
     * Perform any necessary customization on the return structure of a datacontrol operation and all of its
     * nested structures by scanning for any class on the classpath with the @ElementCustomization
     * annotation that targets a structure beneath the given MethodDef.
     * @param method the datacontrol operation (and all of its children) to be customized
     * @see Customizer#customize
     */
    @Override
    public void customize(final MethodDef method) {
        final Collection<JClass> classes = getClasses();
        new Customizer(classes).customize(method);
    }

    /**
     * Scan the design time or run time classpath for any java classes with the @ElementCustomization
     * annotation.
     * @return collection of classes that have been verified to have the @ElementCustomization annotation
     * @see JClassFactory#getAnnotatedClasses
     */
    protected Collection<JClass> getClasses() {
        logger.finer("start scanning for classes with @ElementCustomization annotation");
        Set<JClass> classes = JClassFactory.findAnnotatedClasses(ANNOTATION, getJarTriggerResource(), getSkipJars());
        logger.finer("found customization classes {0}", classes);
        return classes;
    }

    /**
     * Get the resource that should be present in a JAR file on the classpath to consider it for scanning for
     * XML DataControl customization classes. For performance reasons, we don't scan each JAR on the classpath
     * but only the ones that contain this resource, unless they are on the skip list (see getSkipJars)
     * @return value of the jar-trigger-resource parameter from DataControls.dcx or otherwise the global default
     * or the normal default value DFLT_TRIGGER_RESOURCE.
     * @see #ProviderImpl#getRawStringParameter(String, Class, String)
     * @see #getSkipJars
     * @see #PARAM_TRIGGER_RESOURCE
     * @see #DFLT_TRIGGER_RESOURCE
     */
    protected String getJarTriggerResource() {
        return getRawStringParameter(PARAM_TRIGGER_RESOURCE, CustomizationScanner.class, DFLT_TRIGGER_RESOURCE);
    }

    /**
     * Get the JAR files know to have the trigger resource but also known not to have any XML DataControl customization
     * classes so we can skip these during JAR scanning for optimized performance.
     * @return value of the skip-jars parameter from DataControls.dcx or otherwise the global default
     * or the normal default value DFLT_SKIP_JARS.
     * @see #ProviderImpl#getRawStringParameter(String, Class, String)
     * @see #getJarTriggerResource
     * @see #PARAM_SKIP_JARS
     * @see #DFLT_SKIP_JARS
     */
    protected Set<String> getSkipJars() {
        return new HashSet<String>(getRawListParameter(PARAM_SKIP_JARS, CustomizationScanner.class, DFLT_SKIP_JARS));
    }

}
