package org.adfemg.datacontrol.xml.utils;

import java.io.Serializable;

import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class XPointer implements Serializable {

    @SuppressWarnings("compatibility:-7618435233974390464")
    private static final long serialVersionUID = 1L;

    private final String xpath;
    @SuppressWarnings("oracle.jdeveloper.java.field-not-serializable")
    private final NamespaceContext namespaces;

    public XPointer(final String xpath, final NamespaceContext namespaces) {
        if (!(namespaces instanceof Serializable)) {
            throw new IllegalArgumentException("NamespaceContext must be Serializable");
        }
        this.xpath = xpath;
        this.namespaces = namespaces;
    }

    public static XPointer forElement(final Element element) {
        NamespaceContextBuilder namespaces = new NamespaceContextBuilder();
        StringBuilder path = new StringBuilder();
        // traverse up the tree from given Node until document root
        for (Element traverseElement = element, docRootElem = element.getOwnerDocument().getDocumentElement();;) {
            // construct xpath to locate current node in its parent
            StringBuilder pathInParent = new StringBuilder("/");
            String nsURI = traverseElement.getNamespaceURI();
            if (nsURI != null) {
                pathInParent.append(namespaces.getPrefix(nsURI)).append(":");
            }
            pathInParent.append(traverseElement.getLocalName());
            if (!traverseElement.isSameNode(docRootElem)) {
                // add [index] to xpath expression unless this is document root-element which there is only one
                pathInParent.append("[").append(xpathIndexInParent(traverseElement)).append("]");
            }
            path.insert(0, pathInParent);
            // traverse up
            Node parent = traverseElement.getParentNode();
            if (parent == null) {
                // traverseElement is not part of the actual DOM tree
                // FIXME: this means we can't use a org.w3c.dom.Element as dynamic-parameter which itself is not
                // part of a document. This is not uncommon in a managed bean creating the payload. We could
                // build special support for elements that are not part of a document such that XPointer (or its owner)
                // registers this so we can restore this later on.
                throw new IllegalArgumentException("element " + element.getNodeName() + " is not part of a DOM");
            } else if (parent.getNodeType() == Document.DOCUMENT_NODE) {
                break; // found document node (root) so exit loop
            } else if (parent.getNodeType() == Element.ELEMENT_NODE) {
                traverseElement = (Element) parent;
            } else {
                throw new IllegalArgumentException("found unsupported " + parent +
                                                   " while traversing from element to root");
            }
        }
        return new XPointer(path.toString(), namespaces.build());
    }

    public String getXpath() {
        return xpath;
    }

    public NamespaceContext getNamespaces() {
        return namespaces;
    }

    public Element resolve(final Document document) {
        try {
            return (Element) XmlFactory.newXPath(namespaces).evaluate(xpath, document, XPathConstants.NODE);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private static int xpathIndexInParent(Element node) {
        int index = 1;
        // traverse previous siblings and count the ones with same QName
        Node sibling = node.getPreviousSibling();
        while (sibling != null) {
            if (DomUtils.isElementNS(sibling, node.getNamespaceURI(), node.getLocalName())) {
                index++;
            }
            sibling = sibling.getPreviousSibling();
        }
        return index;
    }

    // TODO: equals and hashCode so we can safely use this as key in a HashMap
    // TODO: and toString :-)
}
