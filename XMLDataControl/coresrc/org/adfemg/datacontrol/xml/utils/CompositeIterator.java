package org.adfemg.datacontrol.xml.utils;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class CompositeIterator<E> implements Iterator<E> {

    private Iterator<E>[] iterators;
    private int iterIndex = 0;

    public CompositeIterator(Iterator<E>... iterators) {
        this.iterators = iterators;
    }

    @Override
    public boolean hasNext() {
        if (iterIndex > iterators.length) { // all iterators have been used
            return false;
        } else if (iterators[iterIndex].hasNext()) { // current iterator has more elements
            return true;
        } else {
            // see if any of the remaining iterators have elements
            for (int i = iterIndex + 1; i < iterators.length; i++) {
                if (iterators[i].hasNext()) {
                    return true;
                }
            }
            return false; // nothing found
        }
    }

    @Override
    public E next() {
        while (iterIndex < iterators.length) {
            if (iterators[iterIndex].hasNext()) {
                return iterators[iterIndex].next();
            } else {
                iterIndex++;
            }
        }
        throw new NoSuchElementException();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

}
