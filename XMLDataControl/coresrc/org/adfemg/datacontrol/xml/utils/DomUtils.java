package org.adfemg.datacontrol.xml.utils;

import java.io.StringReader;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;

import oracle.adf.model.adapter.AdapterException;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Text;

/**
 * Utility methods for working with a XML Document Object Model.
 */
public final class DomUtils {

    private static final String STRIPPING_XSLT =
        "<stylesheet version='1.0' xmlns='http://www.w3.org/1999/XSL/Transform'><output method='xml' indent='no'/><strip-space elements='*'/><template match='@*|node()'><copy><apply-templates select='@*|node()'/></copy></template></stylesheet>";
    private static final Templates WHITESPACE_STRIPPER =
        XmlFactory.newTemplates(new StreamSource(new StringReader(STRIPPING_XSLT)));
    private static final String EMPTY_PI_DATA = "";

    /**
     * Private constructor to prevent instances being created.
     */
    @SuppressWarnings("unused")
    private DomUtils() {
    }

    /**
     * Find the first child element of a parent with the given name and namespace.
     *
     * @param parent The element in which we search for the child.
     * @param childNamespaceURI The namespace of the child to find.
     * @param childLocalName The name of the child to find without any namespace prefix.
     * @return The first found child element or {@code null} if none found.
     *
     * @see #findFirstChildElement(Element,String)
     * @see #findChildElements(Element,String,String)
     */
    public static Element findFirstChildElement(final Element parent, final String childNamespaceURI,
                                                final String childLocalName) {
        NodeList children = parent.getChildNodes();
        for (int i = 0, n = children.getLength(); i < n; i++) {
            Node child = children.item(i);
            if (isElementNS(child, childNamespaceURI, childLocalName)) {
                return (Element) child;
            }
        }
        return null;
    }

    /**
     * Same as {@link #findFirstChildElement(Element, String, String)} but without the need for a
     * caller to get the namespaceURI and local name from a QName in situations where the caller
     * (already) has a QName.
     * @param parent The element in which we search for the child
     * @param child namespace and tag name of the child to find
     * @return The first found child element or {@code null} if none found.
     */
    public static Element findFirstChildElement(final Element parent, final QName child) {
        return findFirstChildElement(parent, child.getNamespaceURI(), child.getLocalPart());
    }

    /**
     * Find the first child element of a parent with the given name and with
     * the same namespace as the parent.
     *
     * @param parent The element in which we search for the child.
     * @param childLocalName The name of the child to find without any namespace prefix.
     * @return The first found child element or {@code null} if none found.
     *
     * @see #findFirstChildElement(Element,String,String)
     */
    public static Element findFirstChildElement(final Element parent, final String childLocalName) {
        return findFirstChildElement(parent, parent.getNamespaceURI(), childLocalName);
    }

    /**
     * Find the first child node of a given parent that is a org.w3c.dom.Element.
     * Wll skip over all other children like whitespace, comments, etc.
     * @param parent parent element of the child we're looking for
     * @return first child Node of type Element or {@code null} if none was found
     */
    public static Element findFirstChildElement(final Element parent) {
        NodeList children = parent.getChildNodes();
        for (int i = 0, n = children.getLength(); i < n; i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                return (Element) child;
            }
        }
        return null;
    }

    /**
     * Finds all the child elements of a parent with the given name and namespace.
     *
     * @param parent The element in which we search for the child.
     * @param childNamespaceURI The namespace of the child to find.
     * @param childLocalName The name of the child to find without any namespace prefix.
     * @return A list of the found child elements or an empty List, but never {@code null}.
     *
     * @see #findFirstChildElement(Element,String)
     * @see #findChildElements(Element,String,String)
     */
    public static List<Element> findChildElements(final Element parent, final String childNamespaceURI,
                                                  final String childLocalName) {
        List<Element> retval = new ArrayList<Element>(5);
        NodeList children = parent.getChildNodes();
        for (int i = 0, n = children.getLength(); i < n; i++) {
            Node child = children.item(i);
            if (isElementNS(child, childNamespaceURI, childLocalName)) {
                retval.add((Element) child);
            }
        }
        return retval;
    }

    /**
     * Same as {@link #findChildElements(Element, String, String)} but without the need for a
     * caller to get the namespaceURI and local name from a QName in situations where the caller
     * (already) has a QName.
     * @param parent The element in which we search for the child
     * @param child namespace and tag name of the children to find
     * @return A list of the found child elements or an empty List, but never {@code null}.
     */
    public static List<Element> findChildElements(final Element parent, final QName child) {
        return findChildElements(parent, child.getNamespaceURI(), child.getLocalPart());
    }

    /**
     * Finds all the child elements of a parent with the given name and with
     * the same namespace as the parent.
     *
     * @param parent The element in which we search for the child.
     * @param childLocalName The name of the child to find without any namespace prefix.
     * @return A list of the found child elements or an empty List, but never {@code null}.
     *
     * @see #findFirstChildElement(Element,String,String)
     */
    public static List<Element> findChildElements(final Element parent, final String childLocalName) {
        return findChildElements(parent, parent.getNamespaceURI(), childLocalName);
    }

    /**
     * Convenience method for constructing a child Element.
     * @param parent parent Element that should contain the new child
     * @param namespaceURI the namespace URI of the child element to be created
     * @param tagName the name of the child element to be created which might contain a namespace prefix
     * like pfx:elemName. Without a prefix the element will get a local default namespace declaration for the
     * given namespace.
     * @param value optional String value of the new child that will be used as its text content. See
     * org.adfemg.datacontrol.xml.provider.typemap.TypeMapperImpl.toJava to perform proper conversion
     * for non-string objects to XML safe strings.
     * @return Child element that was added as last child to the given parent
     */
    public static Element addChildElement(final Element parent, final String namespaceURI, final String tagName,
                                          final String value) {
        Element child = parent.getOwnerDocument().createElementNS(namespaceURI, tagName);
        if (value != null) {
            child.setTextContent(value);
        }
        parent.appendChild(child);
        return child;
    }

    /**
     * Create a child Element without text content.
     * @param parent parent Element that should contain the new child
     * @param namespaceURI the namespace URI of the child element to be created
     * @param tagName the name of the child element to be created which might contain a namespace prefix
     * like pfx:elemName. Without a prefix the element will get a local default namespace declaration for the
     * given namespace.
     * @return Child element that was added as last child to the given parent
     * @see #addChildElement(Element, String, String, String)
     */
    public static Element addChildElement(final Element parent, final String namespaceURI, final String tagName) {
        return addChildElement(parent, namespaceURI, tagName, null);
    }

    /**
     * Finds all the child processing instructions of a parent with the given target (aka name).
     * @param parent The element in which we search for the child processing instructions
     * @param target the target (aka name) of the processing instructions to find
     * @return A list of the found processing instructions or an empty List, but never {@code null}
     */
    public static List<ProcessingInstruction> findChildProcessingInstructions(final Element parent,
                                                                              final String target) {
        List<ProcessingInstruction> retval = new ArrayList<ProcessingInstruction>(1);
        NodeList children = parent.getChildNodes();
        for (int i = 0, n = children.getLength(); i < n; i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.PROCESSING_INSTRUCTION_NODE) {
                ProcessingInstruction pi = (ProcessingInstruction) child;
                if (pi.getTarget().equals(target)) {
                    retval.add(pi);
                }
            }
        }
        return retval;
    }

    /**
     * Adds a given processing instruction as first child to a given element.
     * @param parent The org.w3c.dom.Element to add the processing instruction to
     * @param target name of the processing-instruction, also known as its target
     * @return the ProcessingInstruction that was added as first child to the given parent
     */
    public static ProcessingInstruction addChildProcessingInstruction(final Element parent, final String target) {
        final ProcessingInstruction pi = parent.getOwnerDocument().createProcessingInstruction(target, EMPTY_PI_DATA);
        parent.insertBefore(pi, parent.getFirstChild());
        return pi;
    }

    /**
     * Replaces or sets the text content of a given element without replacing any existing non-text children of the
     * given element such as other elements or processing instructions. If the given element has a single existing text
     * child the content of that text child will be replaced. If the given element has no children of type text a new
     * text child will be appended (as last child). If the given element currently has multiple existing children of
     * type text an exception is thrown.
     * @param element Element to replace or set the text content for
     * @param text new text content which could be {@code null} or empty string
     * @throws IllegalArgumentException if given element already has multiple children of type Text
     */
    public static void setTextContent(final Element element, final String text) {
        NodeList children = element.getChildNodes();
        Text textNode = null;
        for (int i = 0, n = children.getLength(); i < n; i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.TEXT_NODE) {
                if (textNode != null) {
                    throw new IllegalArgumentException("element has multiple text child nodes");
                }
                textNode = (Text) child;
            }
        }
        if (StringUtils.isEmpty(text)) {
            if (textNode != null) {
                textNode.getParentNode().removeChild(textNode);
            } else {
                // nothing to do
            }
        } else {
            if (textNode != null) {
                textNode.replaceWholeText(text);
            } else {
                element.appendChild(element.getOwnerDocument().createTextNode(text));
            }
        }
    }

    /**
     * Get the QName (namespace and localName) for a given XML Node.
     * @param node Node to get the QName for
     * @return a valid QName instance with the namespaceURI and localName of the given Node or
     * {@code null} if node was null. It does not retain any namespace-prefix information from the given node.
     * @see QName
     */
    public static QName getQName(Node node) {
        return node == null ? null : new QName(node.getNamespaceURI(), node.getLocalName());
    }

    /**
     * Determines if a node is a XML element and it has the correct namespace and tagName.
     * As a workaround for a Oracle JAX-WS bug this also considers a null namespace on the element to
     * be equal to the empty ("") namespace.
     * @param node node to test
     * @param namespaceURI namespace the node should have, which cannot be {@code null} but can be empty string. When
     * testing for empty namespace (""), this method will also accept nodes with a null namespace to be equal.
     * @param localName tagName the node should have without any namespace prefix
     * @return {@code true} if the supplied node is an element with the correct namespace and tagName.
     * otherwise {@code false}
     */
    public static boolean isElementNS(final Node node, final String namespaceURI, final String localName) {
        return node instanceof Element && equalNamespace(node.getNamespaceURI(), namespaceURI) &&
               localName.equals(node.getLocalName());
    }

    /**
     * Compares two namespace URIs with special leniancy for empty/default namespaces.
     * @param namespaceURI1 first namespace URI to compare
     * @param namespaceURI2 second namespace URI to compare
     * @return {@code true} if both namespaces are equal where {@code null} and empty
     * string are considered equal.
     */
    public static boolean equalNamespace(final String namespaceURI1, final String namespaceURI2) {
        final String ns1 = namespaceURI1 == null ? "" : namespaceURI1;
        final String ns2 = namespaceURI2 == null ? "" : namespaceURI2;
        return ns1.equals(ns2);
    }

    /**
     * Converts javax.xml.soap.SOAPElement to a new org.w3c.dom.Element so it can be safely used in
     * a javax.xml.transform.Transformer which the default SOAPElement doesn't seem to support.
     * @param soapelem SOAPElement to convert
     * @return org.w3c.dom.Element which is equal to the given SOAPElement including all its children and
     * parents and thus the owning Document.
     */
    public static Element soapToDomElement(SOAPElement soapelem) {
        if (soapelem == null) {
            return null;
        }
        XPointer xpointer = XPointer.forElement(soapelem); // get xpath to soapelement in its document
        Document soapdoc = soapelem.getOwnerDocument();
        Document domdoc = XmlFactory.newDocument();
        // importNode copies and leaves source node alone
        Node domdocElem = domdoc.importNode(soapdoc.getDocumentElement(), true);
        domdoc.appendChild(domdocElem);
        return xpointer.resolve(domdoc);
    }

    /**
     * Remove all whitespace-only Text nodes from given Node and any of its children. Empty nodes will be collapes
     * from {@code <element></element>} to {@code <element/>}
     * <p>Example:
     * <pre>{@literal <element>  <child>
     * </child>
     *     </element>}</pre>
     * will be transformed to {@code <element><child/></element>}
     * @param src source Node to strip all whitespace from. Only guaranteed to work with Document and Element.
     * @return Copy of the given node with all whitespace-only Text nodes removed. The original source Node is not
     * altered in any way. If the given source Node is a full Document, the result will also be a Document while a
     * source Element will just returned a (stripped) Element.
     */
    public static Node stripWhitespace(final Node src) {
        try {
            Transformer stripper = WHITESPACE_STRIPPER.newTransformer();
            DOMResult strippedDom = new DOMResult();
            stripper.transform(new DOMSource(src), strippedDom);
            Document strippedDoc = (Document) strippedDom.getNode();
            return Node.DOCUMENT_NODE == src.getNodeType() ? strippedDoc : strippedDoc.getDocumentElement();
        } catch (TransformerConfigurationException e) {
            // strange as XSLT parsing already happened when creating the WHITESPACE_STRIPPER
            throw new AdapterException(e);
        } catch (TransformerException e) {
            throw new AdapterException(e);
        }
    }

}
