package org.adfemg.datacontrol.xml.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import oracle.adf.model.adapter.AdapterException;
import oracle.adf.share.logging.ADFLogger;

import org.adfemg.datacontrol.xml.design.DesignTimeUtils;

/**
 * Utility methods for handling Java classes, serialization and reflection.
 */
public final class ClassUtils {

    private static final ADFLogger logger = ADFLogger.createADFLogger(ClassUtils.class);

    /**
     * Private constructor to prevent instances being created.
     */
    private ClassUtils() {
    }

    /**
     * Load a class from the context classloader (at runtime) or the current design time datacontrol project's
     * classpath (at design time).
     * @param clsName fully qualified name of the class to load
     * @return loaded class or {@code null} if class could not be found in design time environment and a dialog has
     * already been shown to the user
     * @see DesignTimeUtils#isDesignTime
     * @see org.adfemg.datacontrol.xml.design.ProjectUtils#loadClass
     */
    public static Class<?> loadClass(final String clsName) {
        try {
            if (DesignTimeUtils.isDesignTime()) {
                return org.adfemg.datacontrol.xml.design.ProjectUtils.loadClass(clsName);
            } else {
                return Thread.currentThread().getContextClassLoader().loadClass(clsName);
            }
        } catch (ClassNotFoundException e) {
            throw new AdapterException(e);
        }
    }

    /**
     * Instantiate a new object of the given class name.
     * @param clsName fully qualified name of the class to instantiate. Relies on loadClass for loading the
     * class at design time and run time and on newInstance(Class) to do th actuall instantiation.
     * @return instance of the requested class or {@code null} if class could not be found in design time environment
     * and a dialog has already been shown to the user
     * @throws AdapterException when an error occured instantiation the class, usch as InstantiationException or
     * IllegalAccessException
     * @see #loadClass
     */
    public static Object newInstance(final String clsName) {
        Class<?> cls = loadClass(clsName);
        return cls == null ? null : newInstance(cls);
    }

    /**
     * Create a new instance of the given Class and wrap exceptions in a {@link AdapterException}.
     * @param <T> The Type of the object to instantiate.
     * @param cls The class of the object to instantiate.
     * @return The object of the type {@code T}.
     */
    public static <T> T newInstance(final Class<T> cls) {
        try {
            return cls.newInstance();
        } catch (InstantiationException e) {
            throw new AdapterException(e);
        } catch (IllegalAccessException e) {
            throw new AdapterException(e);
        }
    }

    /**
     * Invoke a method on a given object using reflection.
     * @param obj object to invoke the method on
     * @param method method to invoke
     * @param args method arguments
     * @return value as returned by the invoked method
     * @throws RuntimeException if the invoked method threw one
     * @throws AdapterException wrapping IllegalAccessException if that occured or wrapping an Exception thrown by the
     * invoked method
     */
    public static Object invokeMethod(Object obj, Method method, Object... args) {
        try {
            return method.invoke(obj, args);
        } catch (IllegalAccessException e) {
            logger.severe(e);
            throw new AdapterException(e);
        } catch (InvocationTargetException e) {
            if (e.getCause() instanceof RuntimeException) {
                throw (RuntimeException) e.getCause();
            }
            logger.severe(e);
            throw new AdapterException(e);
        }
    }

    /**
     * Get an instance of the given interface {@code interfaceClass}.
     * All methods will throw an {@code UnsupportedOperationException}.
     * <p>This is usefull to create a marker instance of a specific interface.
     * @param <T> The interface-type of the to be created object.
     * @param interfaceClass A {@code Class} object from the type {@code <T>}
     * @return an empty object of the type {@code <T>}
     */
    public static <T> T emptyInterfaceInstance(final Class<T> interfaceClass) {
        return interfaceClass.cast(Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class<?>[] {
                                                          interfaceClass }, new InvocationHandler() {
            public Object invoke(Object proxy, Method method, Object[] args) {
                throw new UnsupportedOperationException();
            }
        }));
    }

    /**
     * Clone an object by serializing and deserializing with normal Java Serialization.
     * @param <T> type of source object and hence also the return object
     * @param object source object which should be serializable
     * @return cloned objects
     * @throws IOException
     * @throws ClassNotFoundException
     * @see java.io.Serializable
     */
    public static <T> T cloneUsingSerialization(T object) throws IOException, ClassNotFoundException {
        // serialize object to byte[]
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(object);
        oos.close();
        // reconstruct object from byte[]
        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bais);
        @SuppressWarnings({
                          "oracle.jdeveloper.java.unchecked-conversion-or-cast",
                          "oracle.jdeveloper.java.unnecessary-cast" })
        T retval = (T) ois.readObject();
        return retval;
    }

}
