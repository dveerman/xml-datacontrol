package org.adfemg.datacontrol.xml.utils;

import java.net.URL;

import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;

import oracle.adf.model.adapter.AdapterException;
import oracle.adf.share.logging.ADFLogger;

import oracle.xml.parser.v2.XMLNode;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import org.xml.sax.SAXException;


@Deprecated
public class Utils {

    private static final ADFLogger logger = ADFLogger.createADFLogger(Utils.class);

    /**
     * Private constructor for non-instantiatable class.
     */
    private Utils() {
    }

    /**
     * @deprecated moved to ClassUtils.newInstance
     */
    @Deprecated
    public static <T> T newInstance(final Class<T> cls) {
        return ClassUtils.newInstance(cls);
    }

    /**
     * @deprecated moved to ClassUtils.emptyInterfaceInstance
     */
    @Deprecated
    public static <T> T emptyInterfaceInstance(final Class<T> interfaceClass) {
        return ClassUtils.emptyInterfaceInstance(interfaceClass);
    }

    /**
     * @deprecated moved to org.adfemg.datacontrol.xml.utils.URLUtils
     */
    @Deprecated
    public static void ensureMdsHandlers() {
        URLUtils.ensureMdsHandlers();
    }

    /**
     * @deprecated was mostly used together with xmlNodeToString which has been replaced with faster XmlUtils.nodeToString
     */
    @Deprecated
    public static XMLNode toXMLNode(final Node node) {
        if (node == null) {
            return null;
        }
        if (node instanceof XMLNode) {
            return (XMLNode) node;
        }
        Document doc = XmlFactory.newDocument();
        doc.appendChild(doc.importNode(node, true)); // importNode copies from source doc and leaves src node alone
        return (XMLNode) doc.getDocumentElement();
    }

    /**
     * @deprecated see org.adfemg.datacontrol.xml.utils.XmlParseUtils.nodeToString
     */
    @Deprecated
    public static String xmlNodeToString(final XMLNode node) {
        return XmlParseUtils.nodeToString(node);
    }

    /**
     * @deprecated as this is moved to org.adfemg.datacontrol.xml.utils.XmlParseUtils.parse
     */
    @Deprecated
    public static Document parse(final String text) {
        try {
            return XmlParseUtils.parse(text);
        } catch (SAXException e) {
            throw new AdapterException(e);
        }
    }

    /**
     * @deprecated moved to org.adfemg.datacontrol.xml.utils.DomUtils
     */
    @Deprecated
    public static Element findFirstChildElement(final Element parent, final String childNamespaceURI,
                                                final String childTagName) {
        return DomUtils.findFirstChildElement(parent, childNamespaceURI, childTagName);
    }

    /**
     * @deprecated moved to org.adfemg.datacontrol.xml.utils.DomUtils
     */
    @Deprecated
    public static Element findFirstChildElement(final Element parent, final QName child) {
        return DomUtils.findFirstChildElement(parent, child);
    }

    /**
     * @deprecated moved to org.adfemg.datacontrol.xml.utils.DomUtils
     */
    @Deprecated
    public static Element findFirstChildElement(final Element parent, final String childTagName) {
        return DomUtils.findFirstChildElement(parent, childTagName);
    }

    /**
     * @deprecated moved to org.adfemg.datacontrol.xml.utils.DomUtils
     */
    @Deprecated
    public static List<Element> findChildElements(final Element parent, final String childNamespaceURI,
                                                  final String childTagName) {
        return DomUtils.findChildElements(parent, childNamespaceURI, childTagName);
    }

    /**
     * @deprecated moved to org.adfemg.datacontrol.xml.utils.DomUtils
     */
    @Deprecated
    public static List<Element> findChildElements(final Element parent, final QName child) {
        return DomUtils.findChildElements(parent, child);
    }

    /**
     * @deprecated moved to org.adfemg.datacontrol.xml.utils.DomUtils
     */
    @Deprecated
    public static List<Element> findChildElements(final Element parent, final String childTagName) {
        return DomUtils.findChildElements(parent, childTagName);
    }

    /**
     * @deprecated use org.adfemg.datacontrol.xml.utils.XmlFactory.newDocumentBuilder()
     */
    @Deprecated
    public static DocumentBuilder getBuilder() {
        return XmlFactory.newDocumentBuilder();
    }

    /**
     * @deprecated moved to ClassUtils.getWorkspaceClassLoader()
     */
    @Deprecated
    public static ClassLoader getWorkspaceClassLoader() {
        logger.severe("Utils.getWorkspaceClassLoader no longer supported. It used to return design time classloader, no simply the current thread classloader");
        return Thread.currentThread().getContextClassLoader();
    }

    /**
     * @deprecated moved to org.adfemg.datacontrol.xml.utils.URLUtils
     */
    @Deprecated
    public static URL findResource(String resource) {
        return URLUtils.findResource(resource);
    }

}
