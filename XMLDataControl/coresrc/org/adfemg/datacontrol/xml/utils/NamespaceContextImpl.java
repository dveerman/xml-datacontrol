package org.adfemg.datacontrol.xml.utils;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;

/**
 * Serializable NamespaceContext that can be used to lookup namespace URIs by their prefix and the
 * other way around.
 */
public class NamespaceContextImpl implements NamespaceContext, Serializable {

    @SuppressWarnings("compatibility:6089673787765435537")
    private static final long serialVersionUID = 2L;

    // BiDi map might be faster but usually we have very small sets of namespaces
    private final Map<String, String> prefix2ns; // key=prefix, value=namespaceURI

    /**
     * The three default namespace prefixes ("", xml and xmlns) that always need to have the same namespace URI
     * mapped to it (and vice versa).
     * @see NamespaceContext#getNamespaceURI
     * @see NamespaceContext#getPrefix
     */
    public static final Map<String, String> DEFAULT_NAMESPACES;
    static {
        Map<String, String> map = new HashMap<String, String>(3);
        map.put(XMLConstants.XML_NS_PREFIX, XMLConstants.XML_NS_URI); // "xml"
        map.put(XMLConstants.XMLNS_ATTRIBUTE, XMLConstants.XMLNS_ATTRIBUTE_NS_URI); // "xmlns"
        map.put(XMLConstants.DEFAULT_NS_PREFIX, XMLConstants.NULL_NS_URI); // ""
        DEFAULT_NAMESPACES = Collections.unmodifiableMap(map);
    }

    /**
     * Default constructor.
     * @param prefix2ns Map of all the namespaces this NamspaceContext should be able to resolve where the
     * key is the namespace prefix and the value is the namespace URI. This does not have to include the
     * two XML default namespace prefixes "xml", "xmlns" and "" (default namespace) as these will automatically
     * be added/overwritten.
     */
    public NamespaceContextImpl(final Map<String, String> prefix2ns) {
        this.prefix2ns = new HashMap<String, String>(prefix2ns.size() + DEFAULT_NAMESPACES.size());
        this.prefix2ns.putAll(DEFAULT_NAMESPACES);
        for (final Map.Entry<String, String> entry : prefix2ns.entrySet()) {
            final String prefix = entry.getKey();
            final String namespaceURI = entry.getValue();
            if (DEFAULT_NAMESPACES.containsKey(prefix)) {
                throw new IllegalArgumentException("cannot override default prefix " + prefix);
            }
            if (DEFAULT_NAMESPACES.containsValue(namespaceURI)) {
                throw new IllegalArgumentException("cannot override default namespace " + namespaceURI);
            }
            this.prefix2ns.put(prefix, namespaceURI);
        }
    }

    /**
     * Get namespace URI for the given prefix.
     * @param prefix prefix to lookup
     * @return Namespace URI
     * @throws IllegalArgumentException when prefix is null
     */
    @Override
    public String getNamespaceURI(final String prefix) {
        if (prefix == null) {
            throw new IllegalArgumentException("prefix is null");
        }
        return prefix2ns.get(prefix);
    }

    /**
     * Get the prefix for the given namespace URI.
     * @param namespaceURI namespace URI
     * @return prefix or {@code null} when the given namespace URI is unknown to this NamespaceContext
     * @throws IllegalArgumentException when namespaceURI is null
     */
    @Override
    public String getPrefix(final String namespaceURI) {
        if (namespaceURI == null) {
            throw new IllegalArgumentException("namespace URI is null");
        }
        for (final Map.Entry<String, String> entry : prefix2ns.entrySet()) {
            if (entry.getValue().equals(namespaceURI)) {
                return entry.getKey();
            }
        }
        return null; // returning null means we don't know
    }

    /**
     * Gets all the prefixes for the given namespace URI.
     * @param namespaceURI namespace URI
     * @return prefixes or empty iterator when the given namespace URI is unknown to this NamespaceContext
     * @throws IllegalArgumentException when namespaceURI is null
     */
    @Override
    public Iterator getPrefixes(final String namespaceURI) {
        if (namespaceURI == null) {
            throw new IllegalArgumentException("namespace URI is null");
        }
        final List<String> prefixes = new ArrayList<String>(2);
        for (final Map.Entry<String, String> entry : prefix2ns.entrySet()) {
            if (entry.getValue().equals(namespaceURI)) {
                prefixes.add(entry.getKey());
            }
        }
        return Collections.unmodifiableList(prefixes).iterator(); // could be empty
    }

}
