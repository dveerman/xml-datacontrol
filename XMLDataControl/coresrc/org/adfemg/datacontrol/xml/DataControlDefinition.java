package org.adfemg.datacontrol.xml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import oracle.adf.model.adapter.AbstractDefinition;
import oracle.adf.model.adapter.AdapterException;
import oracle.adf.model.adapter.dataformat.MethodDef;
import oracle.adf.model.adapter.dataformat.MethodReturnDef;
import oracle.adf.model.adapter.dataformat.StructureDef;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;

import oracle.binding.meta.AccessorDefinition;
import oracle.binding.meta.OperationDefinition;
import oracle.binding.meta.OperationReturnDefinition;
import oracle.binding.meta.StructureDefinition;

import oracle.xml.parser.schema.XMLSchema;
import oracle.xml.parser.schema.XSDConstantValues;
import oracle.xml.parser.schema.XSDException;
import oracle.xml.parser.schema.XSDNode;

import org.adfemg.datacontrol.xml.design.DesignTimeUtils;
import org.adfemg.datacontrol.xml.design.MessageUtils;
import org.adfemg.datacontrol.xml.design.ProjectUtils;
import org.adfemg.datacontrol.xml.provider.DynamicParameter;
import org.adfemg.datacontrol.xml.provider.Provider;
import org.adfemg.datacontrol.xml.provider.customization.CustomizationProvider;
import org.adfemg.datacontrol.xml.provider.structure.MovableStructureDefinition;
import org.adfemg.datacontrol.xml.provider.structure.StructureProvider;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;
import org.adfemg.datacontrol.xml.utils.DomUtils;
import org.adfemg.datacontrol.xml.utils.LoggerUtils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;


/**
 * The DataControl Definition class. After creation this should be initialized by
 * calling {@link #loadFromMetadata} to load the datacontrol configuration from the
 * DataControls.dcx file. Once this is done, {@link #getStructure} can be used to
 * construct (or retrieve the cached) datacontrol structure that is exposed in the
 * JDeveloper datacontrol palette. This class also instantiates the runtime
 * datacontrols using {@link #createDataControl}
 * <p>
 * Inspired on oracle.adfinternal.model.adapter.url.xml.XMLDCDef
 * and oracle.adf.model.adapter.dataformat.XSDHandler
 * <p>
 * oracle.adf.model.adapter.DataControlFactoryImpl is responsible for creating
 * datacontrols and (at least in version 12.1.3) it creates a new
 * DataControlDefinition instance each time it wants to create a new DataControl
 * instance. This means a DataControlDefinition instance is not shared between
 * DataControls
 * @see AbstractDefinition
 * @see oracle.adfinternal.model.adapter.url.xml.XMLDCDef
 * @see oracle.adf.model.adapter.dataformat.XSDHandler
 */
public class DataControlDefinition extends AbstractDefinition {

    // names of custom properties we store in StructureDef's and/or AttributeDef's
    public static final String STRUCTPROP_TYPE = XSDConstantValues._type; // complexType name
    public static final String STRUCTPROP_NAMESPACE = XSDConstantValues._namespace; // complexType namespace
    public static final String STRUCTPROP_CHILD_ELEMS = "childElements"; // ordered list of children QNames

    public static final String ATTRPROP_NAME = XSDConstantValues._name; // name xml element/attribute
    public static final String ATTRPROP_NAMESPACE = STRUCTPROP_NAMESPACE; // ns xml element/attribute
    public static final String ATTRPROP_TYPE = XSDConstantValues._type; // qname of mappable xsd type
    public static final String ATTRPROP_LEAFNODETYPE = "leafNodeType"; // elem/attr/scalCollElem

    public static final String ACCPROP_NAME = ATTRPROP_NAME; // name xml element
    public static final String ACCPROP_NAMESPACE = ATTRPROP_NAMESPACE; // ns xml element
    public static final String ACCPROP_MINOCCURS = XSDConstantValues._minOccurs;
    public static final String ACCPROP_MAXOCCURS = XSDConstantValues._maxOccurs;
    public static final String ACCPROP_NILLABLE = XSDConstantValues._nillable;

    private List<DataControlDefinitionNode> defNodes =
        Collections.emptyList(); // Configurations (filled by the loadFromMetadata)

    private static final String KEY_STRUCTS_CACHE = DataControlDefinition.class.getName() + "_structsCache";
    private static final String KEY_NAMEDSTRUCTS_CACHE = DataControlDefinition.class.getName() + "_namedStructsCache";

    private static final Object APPL_SCOPE_MUTEX = new Object();
    private static final ADFLogger logger = ADFLogger.createADFLogger(DataControlDefinition.class);

    ////////// constanten voor leesbare code //////////
    //private static final boolean READONLY_FALSE = false;
    //private static final boolean KEY_FALSE = false;
    private static final boolean COLLECTION_FALSE = false;
    //private static final boolean SCALAR_COLLECTION_TRUE = true;
    private static final boolean SCALAR_COLLECTION_FALSE = false;

    private boolean createdDatacontrol; // sanity check flag that this def was used once for creating DC
    private Object designTimeV11Project = null;

    //////////////////// Constructor ////////////////////

    /**
     * Default no-args constructor.
     */
    public DataControlDefinition() {
        super();
        setCachingMode(DONT_CACHE);
        setCompileProjectOnCreate(true);
    }

    //////////////////// Abstract methodes from AbstractDefinition ////////////////////

    /**
     * Returns the name of the data control.
     * @return name of the data control
     */
    @Override
    protected String getDCName() {
        String fullName = this.getQualifiedName();
        return fullName.substring(fullName.lastIndexOf('.') + 1);
    }

    /**
     * Returns the element that defines the metadata for the
     * data control which should be the content of the {@literal <Source>} node in
     * the DataControls.dcx file. Note that we cannot fully reconstruct this from
     * DataControlDefinitionNode as the ${} placeholders in the provider parameters
     * are already replaced when parsing the original configuration file.
     * Currently there is no way to reconstruct the original XML.
     * @return Metadata from {@link #setMetadata} at design time when using the wizard to
     * create a new datacontrol
     * @throws UnsupportedOperationException at runtime as we can't fully reconstruct the
     * metadata
     * @see #loadFromMetadata
     * @see DataControlDefinitionNode#createMetadata
     * @see #setMetadata
     */
    @Override
    public Node getMetadata() {
        if (metadata == null || !DesignTimeUtils.isGraphicalDesignTime()) {
            throw new UnsupportedOperationException();
        }
        return metadata;
    }

    private Node metadata;

    /**
     * Set the metadata (which is the Source node in the DataControls.dcx) when creating a
     * new XML datacontrol through the wizard in JDeveloper design time. The framework will
     * subsequently ask this with getMetadata to persist it to DataControls.dcx
     * @param metadata future Source node as it should be created in the DataControls.dcx file
     * for this datacontrol
     */
    public void setMetadata(Node metadata) {
        this.metadata = metadata;
    }

    /**
     * Loads the definition from a metadata {@code Node} as specified in
     * the {@literal <Source>} node in the DataControls.dcx file.
     *
     * @param node The {@literal <Source>} node from the DataControls.dcx file. It can be
     *             null if no metadata is defined.
     * @param params Context parameters.
     */
    @Override
    public void loadFromMetadata(final Node node, final Map params) {
        // try to locate Project this datacontrol belongs to
        designTimeV11Project = DesignTimeUtils.isDesignTime() ? ProjectUtils.findDataControlProject(node) : null;
        // Get the information from the definition.
        defNodes = new ArrayList<DataControlDefinitionNode>();
        if (!(node instanceof Element)) {
            return;
        }
        for (Element definition :
             DomUtils.findChildElements((Element) node, DataControlDefinitionNode.NS_DEFINITION,
                                        DataControlDefinitionNode.ELEM_DEFINITION)) {
            defNodes.add(new DataControlDefinitionNode(this, definition));
        }
    }

    /**
     * Given a StructureDefinition find the DataControlDefinitionNode that is able to return this
     * structure. This means the given StructureDefinition has to be the structure returned by the
     * datacontrol operation itself or has to be one of its children. Can be used for a reverse lookup
     * for which datacontrol operation is/was responsible for returning a certain structure.
     * @param structDef StructureDefinition returned by one of the operations of this datacontrol
     * @return DataControlDefinitionNode responsible for the datacontrol method that is able to return
     * the given structure, or {@code null} if none was found
     */
    public DataControlDefinitionNode findDefinitionNode(StructureDefinition structDef) {
        for (DataControlDefinitionNode defNode : defNodes) {
            if (structDef.getFullName().startsWith(this.getQualifiedName() + "." + defNode.getDatacontrolOperation() +
                                                   ".")) {
                return defNode;
            }
        }
        return null;
    }

    /**
     * Gets a list of all DataControlDefinitionNodes as they were present in the DataControls.dcx file.
     * Each DataControlDefinitionNode represents an operation in this datacontrol.
     * @return list of all DataControlDefinitionNodes (aka datacontrol operations)
     */
    public List<DataControlDefinitionNode> getDefinitionNodes() {
        return Collections.unmodifiableList(defNodes);
    }

    /**
     * Returns the root structure definition.
     * @return StructureDefinition which are used by DC palette or runtime.
     */
    @Override
    public StructureDefinition getStructure() {
        // use fully qualified name of datacontrol as key in the application scope caches
        final String cacheKey = getQualifiedName();
        if (DesignTimeUtils.isDesignTime()) {
            // caching of structures is only for performance optimization at runtime. Always
            // clear cache at design time as structure might be changing.
            if (!(getStructsCache().isEmpty() && getNamedStructsCache().isEmpty())) {
                logger.fine("clearing non-empty structure cache in design time");
                getStructsCache().clear();
                getNamedStructsCache().clear();
            }
        }
        if (!getStructsCache().containsKey(cacheKey)) {
            // this datacontrol hasn't processed (and cached) its structures yet
            logger.finer("building structure for " + getDCName());
            LoggerUtils.begin(logger, "building XML StructureDefinition");
            try {
                // store root StructureDefinition and cached StructureDefinitions in application scope cache
                final StructureDef rootStruct = buildStructure();
                if (rootStruct == null) {
                    return null;
                }
                // Build cache (map) of all StructureDefinitions by their full name
                Map<String, StructureDefinition> structCache = new HashMap<String, StructureDefinition>();
                buildStructCache(rootStruct, structCache);
                // store root structure (and other structures) in cache
                getStructsCache().put(cacheKey, rootStruct);
                getNamedStructsCache().put(cacheKey, structCache);
            } catch (Exception e) {
                // logging the Error causes JDeveloper to throw the 'Unexpected Error Window'.
                logger.severe(e.getMessage(), e);
                throw new AdapterException(e);
            } finally {
                logger.end("building XML StructureDefinition");
            }
        }
        return getStructsCache().get(cacheKey);
    }

    private StructureDef buildStructure() throws XSDException {
        // build StructureDefinition
        if (DesignTimeUtils.isDesignTime()) {
            // set current datacontrol project in version 11 so invoked code knows which classpath to use
            ProjectUtils.setDataControlProjectContext(getDesignTimeProject());
        }
        try {
            final StructureDef rootStruct = new StructureDef(getQualifiedName());
            for (DataControlDefinitionNode defNode : defNodes) {
                // try to find root data node as XSD Element or otherwise as XSD ComplexType
                XSDNode rootDataNode = loadRootDataNode(defNode);
                if (rootDataNode == null) {
                    // failure at design time has already been shown as dialog. Quit silently
                    return null;
                }

                // have StructureProvider build a StructureDef for root data node (XSD Element or ComplexType)
                StructureProvider structProv = defNode.getProviderInstance(StructureProvider.class);
                TypeMapper typeMapper = defNode.getProviderInstance(TypeMapper.class);
                MovableStructureDefinition structDef =
                    structProv.buildStructure(rootDataNode, getReturnStructName(rootStruct, defNode), typeMapper);

                // create MethodDef with StructureDef from StructureProvider as return value
                final MethodDef method = new MethodDef(defNode.getDatacontrolOperation(), rootStruct);
                rootStruct.addMethod(method);

                // add parameters to method-definition if data provider has dynamic parameters
                {
                    final Set<DynamicParameter> dynamicParams = defNode.getDynamicParams();
                    if (dynamicParams != null) {
                        for (DynamicParameter dynpar : dynamicParams) {
                            method.addParameter(dynpar.getName(), dynpar.getJavaType());
                        }
                    }
                }

                // link StructureDef to MethodDef using a MethodReturnDef
                final MethodReturnDef methodReturn =
                    new MethodReturnDef(rootDataNode.getName(), structDef, method, COLLECTION_FALSE,
                                        SCALAR_COLLECTION_FALSE);
                structDef.setParent(methodReturn);
                method.setReturnType(methodReturn);

                // apply customizations to the structure
                CustomizationProvider custProv = defNode.getProviderInstance(CustomizationProvider.class);
                if (custProv != null) {
                    custProv.customize(method);
                }

            }
            return rootStruct;
        } finally {
            if (DesignTimeUtils.isDesignTime()) {
                // clear current datacontrol project in version 11
                ProjectUtils.clearDataControlProjectContext();
            }
        }
    }

    /**
     * Builds the fully qualified name for the StructureDef returned by the method operation.
     * @param parent StructureDef of the datacontrol itself which is the parent of the MethodDef
     * @param defNode DataControlDefinitionNode that contains the configuration for this dc
     * @return parentFullName.defNodeDataControlOperationName.defNodeRootElementName
     */
    public String getReturnStructName(final StructureDefinition parent, final DataControlDefinitionNode defNode) {
        return parent.getFullName() + "." + defNode.getDatacontrolOperation() + "." + defNode.getRoot();
    }

    private Map<String, StructureDefinition> getStructsCache() {
        synchronized (APPL_SCOPE_MUTEX) {
            Map<String, Object> applScope = ADFContext.getCurrent().getApplicationScope();
            if (!applScope.containsKey(KEY_STRUCTS_CACHE)) {
                applScope.put(KEY_STRUCTS_CACHE, new ConcurrentHashMap<String, StructureDefinition>());
            }
            @SuppressWarnings("unchecked")
            Map<String, StructureDefinition> retval =
                (Map<String, StructureDefinition>) applScope.get(KEY_STRUCTS_CACHE);
            return retval;
        }
    }

    private XSDNode loadRootDataNode(DataControlDefinitionNode defNode) throws XSDException {
        final XMLSchema schema = defNode.getSchema();
        // try to find root data node as XSD Element or otherwise as XSD ComplexType
        XSDNode rootDataNode = schema == null ? null : schema.getElement(schema.getSchemaTargetNS(), defNode.getRoot());
        if (rootDataNode == null) {
            rootDataNode = schema == null ? null : schema.getType(schema.getSchemaTargetNS(), defNode.getRoot());
        }
        if (rootDataNode == null) {
            // both XSD Element and ComplexType not found. Reporting to the user.
            if (DesignTimeUtils.isGraphicalDesignTime()) {
                MessageUtils.showErrorMessage("Element or ComplexType not Found.",
                                              "Not able to find Element or ComplexType with the name '" +
                                              defNode.getRoot() + "' in " + defNode.getSchemaUrl());
                return null;
            } else {
                throw new IllegalArgumentException("Element or ComplexType '" + defNode.getRoot() + "' not found.");
            }
        }
        return rootDataNode;
    }

    private Map<String, Map<String, StructureDefinition>> getNamedStructsCache() {
        synchronized (APPL_SCOPE_MUTEX) {
            Map<String, Object> applScope = ADFContext.getCurrent().getApplicationScope();
            if (!applScope.containsKey(KEY_NAMEDSTRUCTS_CACHE)) {
                applScope.put(KEY_NAMEDSTRUCTS_CACHE,
                              new ConcurrentHashMap<String, Map<String, StructureDefinition>>());
            }
            @SuppressWarnings("unchecked")
            Map<String, Map<String, StructureDefinition>> retval =
                (Map<String, Map<String, StructureDefinition>>) applScope.get(KEY_NAMEDSTRUCTS_CACHE);
            return retval;
        }
    }

    // FIXME XMLDC-62 will cause issues with circular references in XSD as it will do depth-first tree walk
    @Deprecated
    private void buildStructCache(StructureDefinition struct, Map<String, StructureDefinition> cache) {
        cache.put(struct.getFullName(), struct);
        // recursive call of buildStructCache for operation results
        for (Iterator iter = struct.getOperationDefinitions().iterator(); iter.hasNext();) {
            Object obj = iter.next();
            if (obj instanceof OperationDefinition) {
                OperationReturnDefinition returnDef = ((OperationDefinition) obj).getOperationReturnType();
                if (returnDef.isAccessor()) {
                    buildStructCache(((AccessorDefinition) returnDef).getStructure(), cache);
                }
            }
        }
        // recursive call of buildStructCache for all structures in accessors
        for (Iterator iter = struct.getAccessorDefinitions().iterator(); iter.hasNext();) {
            Object obj = iter.next();
            if (obj instanceof AccessorDefinition) {
                buildStructCache(((AccessorDefinition) obj).getStructure(), cache);
            }
        }
    }

    /**
     * Creates an instance of data control generated from the metadata definition.
     * Each invoke of this method will create a new data control instance that
     * is not cached within this definition.
     *
     * @return the {@link DataControl} instance.
     */
    @Override
    public DataControl createDataControl() {
        if (DesignTimeUtils.isDesignTime()) {
            // set current datacontrol project in version 11 so invoked code knows which classpath to use
            ProjectUtils.setDataControlProjectContext(getDesignTimeProject());
        }
        try {
            final DataControl dc = new DataControl(this);
            // Notify all providers that the new DataControl is created.
            for (DataControlDefinitionNode defNode : defNodes) {
                for (Provider provider : defNode.getProviderInstances()) {
                    provider.dataControlCreated(dc);
                }
            }
            if (createdDatacontrol) {
                // sanity check as re-use of DataControlDefinition would mean re-use of
                // DataControlDefinitionNode and thus instances of Providers across DataControls
                // At least SnapshotManagerImpl (and perhaps other Providers) have state
                // state and might not support being shared
                throw new IllegalStateException("DataControlDefinition created multiple DataControls");
            }
            createdDatacontrol = true;
            return dc;
        } finally {
            if (DesignTimeUtils.isDesignTime()) {
                // clear current datacontrol project in version 11
                ProjectUtils.clearDataControlProjectContext();
            }
        }
    }

    /**
     * Gets the token that points to the JDeveloper design time project this datacontrol belongs to.
     * Can be used with ProjectUtils.setDataControlProjectContext and ProjectUtils.clearDataControlProjectContext
     * around code that needs the ProjectUtils.getCurrentDataControlProject for example to load java classes
     * or file resources (XSD, XML, etc) from the project a datacontrol belongs to.
     * @return token pointing to the design time JDeveloper project or {@code null} if not known or not running
     * in design time
     */
    public Object getDesignTimeProject() {
        return designTimeV11Project;
    }

    //////////////////// Overriden methods ////////////////////

    /**
     * Indicates whether the definition delegates to the framework the task of
     * deserializing its StructureDefinition (for example, from bean .xml
     * files on disk).
     *
     * @return by default {@code false} as this is a dynamic data control
     *         definition that parses the XSD at runtime.
     */
    @Override
    public boolean usePersistedStructure() {
        return false;
    }

    /**
     * Tells the framework whether the Data control structure is dirty and needs
     * to be refreshed on the palette.
     *
     * @param refresh flag to indicate if the refresh is requested for the
     *        structure.
     * @return this does not check for any changes in the underlying XSD but
     *         simply returns the value of {@code refresh} to only refresh when
     *         requested by the user
     */
    @Override
    public boolean isStructureDirty(final boolean refresh) {
        logger.finer("isStructureDirty: {0}", refresh);
        return refresh;
    }

    /**
     * Gets the Fully Qualified Name as set by {@link #setFullName}.
     * The default implementation of getFullName from the superclass returns only
     * the last part of the full name.
     * For example: The super returns {@code DataControl} if the Fully
     * Qualified Name is {@code com.example.DataControl}.
     *
     * @return Fully qualified name as set by {@link #setFullName}
     */
    public String getQualifiedName() {
        return mFullName;
    }

    //////////////////// own methods ////////////////////

    /**
     * Look for a StructureDefinition within the DataControl.
     * This can be the root StructureDefinition, a XSD ComplexType or a
     * XSD Element.
     *
     * @param definitionId Unique ID of the StructureDefinition to look for.
     * @return StructureDefinition or {@code null} if none found.
     */
    public StructureDefinition findStructure(final String definitionId) {
        StructureDefinition strDef = getStructure();

        // The root definition, don't need to look any further.
        String name = strDef.getFullName();
        if (definitionId.equals(name)) {
            return strDef;
        }

        final Map<String, Map<String, StructureDefinition>> cache = getNamedStructsCache();
        final Map<String, StructureDefinition> namedStructs = cache.get(this.mFullName);
        StructureDefinition def = namedStructs.get(definitionId);
        return def;
    }

}
