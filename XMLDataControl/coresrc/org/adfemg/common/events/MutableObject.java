package org.adfemg.common.events;


/**
 * This interface indicates that the Object is mutable.
 * A mutable Object can be changed, unlike the immutable Objects
 * (such as String and Integer).
 */
public interface MutableObject {
    /**
     * Register a {@code listener} to this Object.
     *
     * @param listener the listener.
     */
    void addChangeListener(ChangeListener listener);

    /**
     * Remove a registration of a {@code listener} on this Object.
     *
     * @param listener the listener
     */
    void removeChangeListener(ChangeListener listener);
}
