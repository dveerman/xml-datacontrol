package org.adfemg.common.events;

import java.util.EventObject;


/**
 * A ChangeEvent is used to notify Listeners about a change in the {@code source}.
 */
public class ChangeEvent extends EventObject {
    @SuppressWarnings("compatibility:1612540172106911113")
    private static final long serialVersionUID = 1L;

    /**
     * Create a new ChangeEvent using the input argument {@code source}.
     *
     * @param source the changed Object.
     */
    public ChangeEvent(final Object source) {
        super(source);
    }
}
