package org.adfemg.common.reflect;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import java.util.Collection;


/**
 * The BroadcastProxy helps to call the same methods of different implementation of
 * an interface.
 * Usefull for the implementation of listeners.
 */
public final class BroadcastProxy {
    /**
     * Private constructure to prevent the BroadcastProxy from being instantiated.
     */
    private BroadcastProxy() {
        super();
    }

    /**
     * Create a broadcast proxy based on the given {@code forInterface}
     * and the list of {@code listeners}.
     *
     * All calls on the proxy will also be called on all the listeners, in the same
     * order as the Collection-iterator returns them.
     * The result of the last call will be returned.
     * The reference to the Collection {@code listeners}. will be saved, so
     * changes in the collection will be known.
     *
     * @param <T> The interface that implements the proxy, the same as {@code forInterface}
     * @param forInterface the interface that needs to be implemented by the interface and the proxy.
     * @param listeners the listeners
     * @return the result off the method from the last listener.
     */
    public static <T> T create(final Class<T> forInterface, final Collection<? extends T> listeners) {
        return forInterface.cast(Proxy.newProxyInstance(forInterface.getClassLoader(), new Class[] { forInterface },
                                                        new BroadcastHandler(listeners)));
    }

    /**
     * The handler, makes sure that every call to a method will call the methods
     * on the listeners.
     */
    private static final class BroadcastHandler implements InvocationHandler {
        private final Collection<?> listeners;

        /**
         * Package private constructor, accepts an active list off listeners.
         *
         * @param listeners the list of listeners.
         */
        BroadcastHandler(final Collection<?> listeners) {
            this.listeners = listeners;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Object invoke(final Object proxy, final Method method,
                             final Object[] args) throws IllegalAccessException, InvocationTargetException {
            Object result = null;
            for (Object listener : listeners) {
                result = method.invoke(listener, args);
            }
            return result;
        }
    }
}
