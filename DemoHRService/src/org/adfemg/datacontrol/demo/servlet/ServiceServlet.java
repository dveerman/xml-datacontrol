package org.adfemg.datacontrol.demo.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.bind.JAXB;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SAAJResult;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.dom.DOMSource;

import org.adfemg.hr.DepartmentEmployeesRequest;
import org.adfemg.hr.DepartmentEmployeesResponse;
import org.adfemg.hr.HumanResourcesImpl;
import org.adfemg.hr.ListAllDepartmentsRequest;
import org.adfemg.hr.ListAllDepartmentsResponse;

public class ServiceServlet extends HttpServlet {

    public static final String NS_REQUEST = "http://adfemg.org/HR";
    public static final QName QNAME_LIST_ALL_DEPARTMENTS = new QName(NS_REQUEST, "ListAllDepartmentsRequest");
    public static final QName QNAME_DEPT_EMPS = new QName(NS_REQUEST, "DepartmentEmployeesRequest");

    private final HumanResourcesImpl serviceImpl = new HumanResourcesImpl();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
                                                                                           IOException {
        try {
            MimeHeaders headers = getHeaders(request);
            InputStream in = request.getInputStream();
            SOAPMessage soapReq = messageFactory.createMessage(headers, in);
            SOAPMessage soapResp = handleSOAPRequest(soapReq);
            response.setStatus(HttpServletResponse.SC_OK);
            response.setContentType("text/xml;charset=\"UTF-8\"");
            OutputStream out = response.getOutputStream();
            soapResp.writeTo(out);
            out.flush();
        } catch (SOAPException e) {
            throw new IOException("exception while creating SOAP message", e);
        }
    }

    public MimeHeaders getHeaders(HttpServletRequest request) {
        MimeHeaders retval = new MimeHeaders();
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String name = headerNames.nextElement();
            String value = request.getHeader(name);
            StringTokenizer values = new StringTokenizer(value, ",");
            while (values.hasMoreTokens()) {
                retval.addHeader(name, values.nextToken().trim());
            }
        }
        return retval;
    }

    protected SOAPMessage handleSOAPRequest(SOAPMessage request) throws SOAPException {
        Iterator iter = request.getSOAPBody().getChildElements();
        SOAPElement reqElem = null;
        while (iter.hasNext()) {
            // find first Element child
            Object child = iter.next();
            if (child instanceof SOAPElement) {
                reqElem = (SOAPElement) child;
                break;
            }
        }

        Object respPojo = handleSOAPRequestElement(reqElem);

        SOAPMessage soapResp = messageFactory.createMessage();
        SOAPBody respBody = soapResp.getSOAPBody();
        if (respPojo != null) {
            JAXB.marshal(respPojo, new SAAJResult(respBody));
        } else {
            SOAPFault fault = respBody.addFault();
            fault.setFaultString("Unknown SOAP request: " + (reqElem != null ? reqElem.getElementQName() : ""));
        }
        return soapResp;
    }

    protected Object handleSOAPRequestElement(SOAPElement reqElem) {
        QName reqName = reqElem.getElementQName();
        if (QNAME_LIST_ALL_DEPARTMENTS.equals(reqName)) {
            return handleListAllDepartments(JAXB.unmarshal(new DOMSource(reqElem), ListAllDepartmentsRequest.class));
        } else if (QNAME_DEPT_EMPS.equals(reqName)) {
            return handleDepartmentEmployees(JAXB.unmarshal(new DOMSource(reqElem), DepartmentEmployeesRequest.class));
        }
        return null;
    }

    protected ListAllDepartmentsResponse handleListAllDepartments(ListAllDepartmentsRequest request) {
        return serviceImpl.listAllDepartments(request);
    }

    protected DepartmentEmployeesResponse handleDepartmentEmployees(DepartmentEmployeesRequest request) {
        return serviceImpl.departmentEmployees(request);
    }

    private static final MessageFactory messageFactory;

    static {
        try {
            messageFactory = MessageFactory.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
