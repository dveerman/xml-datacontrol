package org.adfemg.datacontrol.extension12;

import oracle.adf.share.common.ClassUtils;

import oracle.adfdt.model.datacontrols.AdapterDataControlObjectFactory;

public class XMLDCDataControlObjectFactory implements AdapterDataControlObjectFactory {

    public XMLDCDataControlObjectFactory() {
        super();
    }

    @Override
    public Object create(String className) {
        try {
            Class cls = ClassUtils.forName(className, XMLDCDataControlObjectFactory.class);
            if (cls != null) {
                return cls.newInstance();
            }
        } catch (Exception e) {            
        }
        return null;
    }
}
