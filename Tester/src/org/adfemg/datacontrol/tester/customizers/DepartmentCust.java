package org.adfemg.datacontrol.tester.customizers;

import org.adfemg.datacontrol.xml.annotation.CalculatedAttr;
import org.adfemg.datacontrol.xml.annotation.ElementCustomization;
import org.adfemg.datacontrol.xml.annotation.ElementValidation;
import org.adfemg.datacontrol.xml.annotation.TransientAttr;
import org.adfemg.datacontrol.xml.data.XMLDCElement;

@ElementCustomization(target =
                      "org.adfemg.datacontrol.tester.HRDataControl.getXML.DepartmentList.Department")
public class DepartmentCust {
    public DepartmentCust() {
        super();
    }

    @CalculatedAttr
    public String getNameId(XMLDCElement element)
    {
      return element.get("name") + " : " + element.get("id");
    }

    @TransientAttr
    public String initComment(XMLDCElement element)
    {
      return "Place your comments in here";
    }
    
    @ElementValidation(attr="name")
    public void check(XMLDCElement department) {
        
    }
}
