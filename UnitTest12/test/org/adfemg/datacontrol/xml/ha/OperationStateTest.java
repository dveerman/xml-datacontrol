package org.adfemg.datacontrol.xml.ha;

import java.io.IOException;
import java.io.NotSerializableException;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oracle.adf.model.adapter.dataformat.StructureDef;

import oracle.binding.meta.StructureDefinition;

import org.adfemg.datacontrol.xml.DataControl;
import org.adfemg.datacontrol.xml.data.XMLDCElement;
import org.adfemg.datacontrol.xml.utils.ClassUtils;
import org.adfemg.datacontrol.xml.utils.XmlParseUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class OperationStateTest {

    @Test
    public void paramsSurviveSerialization() throws Exception {
        Document srcdoc = XmlParseUtils.parse("<element1/>");
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("key", "value");

        OperationState os = new OperationState(params, srcdoc.getDocumentElement());
        OperationState clone = ClassUtils.cloneUsingSerialization(os);
        assertNotNull("cloned OperationState should exist", clone);
        assertTrue("clonedOperationState.isSameParamValues(originalOperationState) should be true",
                   clone.isSameParamValues(params));
    }

    @Test
    public void unserializableParamsMapSurviveSerialization() throws Exception {
        Map<String, Object> unserializableMap = new UnserializableMap<String, Object>();
        unserializableMap.put("key", "value");
        OperationState state =
            new OperationState(unserializableMap, XmlParseUtils.parse("<element/>").getDocumentElement());
        OperationState clonedState = ClassUtils.cloneUsingSerialization(state);
        assertNotNull("cloned OperationState should exist", clonedState);
        assertTrue("clonedOperationState.isSameParamValues(originalOperationState) should be true",
                   clonedState.isSameParamValues(unserializableMap));
    }

    @Test(expected = IllegalArgumentException.class)
    public void cannotCompareParamValuesToNull() throws Exception {
        OperationState state = new OperationState(null, XmlParseUtils.parse("<element/>").getDocumentElement());
        state.isSameParamValues(null);
    }

    @Test
    public void emptyParamsMapSurviveSerialization() throws Exception {
        Map<String, Object> params = new HashMap<String, Object>();
        OperationState clonedState =
            ClassUtils.cloneUsingSerialization(new OperationState(params,
                                                                  XmlParseUtils.parse("<element/>").getDocumentElement()));
        assertNotNull("cloned OperationState should exist", clonedState);
        assertTrue("clonedOperationState.isSameParamValues(Collections.emptyMap()) should be true",
                   clonedState.isSameParamValues(Collections.<String, Object>emptyMap()));
    }

    @Test
    public void xmlElementParamsSurviveSerialization() throws Exception {
        Map<String, Object> params = new HashMap<String, Object>();
        Element paramElem = XmlParseUtils.parse("<element/>").getDocumentElement();
        params.put("elem", paramElem);
        OperationState clonedState =
            ClassUtils.cloneUsingSerialization(new OperationState(params,
                                                                  XmlParseUtils.parse("<element/>").getDocumentElement()));
        assertNotNull("cloned OperationState should exist", clonedState);
        Map<String, Object> secondParams =
            Collections.singletonMap("elem", (Object) XmlParseUtils.parse("<element/>").getDocumentElement());
        assertTrue("clonedOperationState.paramValues should have equal element",
                   ((Element) params.get("elem")).isEqualNode((Element) clonedState.paramValues.get("elem")));
        // after cloning the new OperationState has a new instance of org.w3c.dom.Element
        // so clonedState.isSameParamValues can't use equality (==) comparison
        assertTrue("clonedOperationState.isSameParamValues(map with org.w3c.dom.Element) should be true",
                   clonedState.isSameParamValues(secondParams));
    }

    @Test(expected = NotSerializableException.class)
    public void xmldcelementParamShouldFailSerialization() throws Exception {
        Element element = XmlParseUtils.parse("<element/>").getDocumentElement();
        DataControl dataControl = new DataControl();
        StructureDefinition structureDef = new StructureDef("fullname");
        XMLDCElement xmldce = new XMLDCElement(dataControl, structureDef, element);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("xmldce", xmldce);
        ClassUtils.cloneUsingSerialization(new OperationState(params, xmldce.getElement()));
    }

    @Test
    public void failoverFlag() throws Exception {
        OperationState state =
            new OperationState(Collections.<String, Object>emptyMap(),
                               XmlParseUtils.parse("<element/>").getDocumentElement());
        assertFalse("initial OperationState should not be marked as failover", state.isFailover());
        OperationState clone = ClassUtils.cloneUsingSerialization(state);
        assertTrue("cloned OperationState should be marked failover after deserialization", clone.isFailover());
        assertFalse("original OperationState should not be marked failover after serialization", state.isFailover());
        OperationState clone2 =
            ClassUtils.cloneUsingSerialization(clone); // clone again when failover=true on source object
        assertTrue("two times cloned OperationState with failover=true should be marked as failover",
                    clone2.isFailover());
        assertTrue("one time cloned OperationState with failover=true should be marked as failover",
                    clone.isFailover());
        assertFalse("original OperationState should not be marked failover after serialization", state.isFailover());
    }

    @Test
    public void elementSurvivesSerialization() throws Exception {
        Element elem = XmlParseUtils.parse("<element xmlns='example.com/namespace'/>").getDocumentElement();
        OperationState state = new OperationState(Collections.<String, Object>emptyMap(), elem);
        OperationState clone = ClassUtils.cloneUsingSerialization(state);
        assertEquals("element should survive serialization", XmlParseUtils.nodeToString(elem),
                     XmlParseUtils.nodeToString(clone.getElement()));
        assertEquals("document should survive serialization", XmlParseUtils.nodeToString(elem.getOwnerDocument()),
                     XmlParseUtils.nodeToString(clone.getElement().getOwnerDocument()));
    }

    @Test
    public void subElementSurvivesSerialization() throws Exception {
        Document doc = XmlParseUtils.parse("<rootelem xmlns='example.com/namespace'><subelem/></rootelem>");
        Element subelem = (Element) doc.getDocumentElement().getChildNodes().item(0);
        OperationState state = new OperationState(Collections.<String, Object>emptyMap(), subelem);
        OperationState clone = ClassUtils.cloneUsingSerialization(state);
        assertEquals("sub-element should survive serialization", XmlParseUtils.nodeToString(subelem),
                     XmlParseUtils.nodeToString(clone.getElement()));
        assertEquals("document should survive serialization", XmlParseUtils.nodeToString(subelem.getOwnerDocument()),
                     XmlParseUtils.nodeToString(clone.getElement().getOwnerDocument()));
    }

    @Test
    public void elementWithUserDataSurvivesSerialization() throws Exception {
        Element elem = XmlParseUtils.parse("<element xmlns='example.com/namespace'/>").getDocumentElement();
        SerializableUserData.forNode(elem).put("key", "value");
        OperationState state = new OperationState(Collections.<String, Object>emptyMap(), elem);
        OperationState clone = ClassUtils.cloneUsingSerialization(state);
        assertEquals("element should survive serialization", XmlParseUtils.nodeToString(elem),
                     XmlParseUtils.nodeToString(clone.getElement()));
        assertEquals("document should survive serialization", XmlParseUtils.nodeToString(elem.getOwnerDocument()),
                     XmlParseUtils.nodeToString(clone.getElement().getOwnerDocument()));
        assertEquals("element UserData should survive serialization", "value",
                     SerializableUserData.forNode(clone.getElement()).get("key"));
    }

    @Test
    public void subElementWithUserDataSurvivesSubElementSerialization() throws Exception {
        Document doc = XmlParseUtils.parse("<rootelem xmlns='example.com/namespace'><subelem/></rootelem>");
        Element subelem = (Element) doc.getDocumentElement().getChildNodes().item(0);
        SerializableUserData.forNode(subelem).put("key", "value");
        OperationState state = new OperationState(Collections.<String, Object>emptyMap(), subelem);
        OperationState clone = ClassUtils.cloneUsingSerialization(state);
        assertEquals("sub-element should survive serialization", XmlParseUtils.nodeToString(subelem),
                     XmlParseUtils.nodeToString(clone.getElement()));
        assertEquals("document should survive serialization", XmlParseUtils.nodeToString(subelem.getOwnerDocument()),
                     XmlParseUtils.nodeToString(clone.getElement().getOwnerDocument()));
        assertEquals("sub-element UserData should survive serialization", "value",
                     SerializableUserData.forNode(clone.getElement()).get("key"));
    }

    @Test
    public void sameElementInMultipleOperationStatesShouldRetainIdentityOnSerialization() throws Exception {
        Document doc =
            XmlParseUtils.parse("<rootelem xmlns='example.com/namespace'><subelem><subsub/></subelem></rootelem>");
        Element subelem = (Element) doc.getDocumentElement().getChildNodes().item(0);
        Element subsubelem = (Element) subelem.getChildNodes().item(0);
        OperationState substate = new OperationState(Collections.<String, Object>emptyMap(), subelem);
        OperationState subsubstate = new OperationState(Collections.<String, Object>emptyMap(), subsubelem);
        List<OperationState> clonedStates = ClassUtils.cloneUsingSerialization(Arrays.asList(substate, subsubstate));
        assertTrue("same element in multiple OperationStates should remain equal",
                   clonedStates.get(0).getElement().getOwnerDocument().isEqualNode(clonedStates.get(1).getElement().getOwnerDocument()));
        assertTrue("same element in multiple OperationStates should remain same",
                   clonedStates.get(0).getElement().getOwnerDocument().isSameNode(clonedStates.get(1).getElement().getOwnerDocument()));
        assertTrue("same element in multiple OperationStates should retain identity",
                   clonedStates.get(0).getElement().getChildNodes().item(0) == clonedStates.get(1).getElement());
    }

    private static class UnserializableMap<K, V> extends AbstractMap<K, V> {
        private Map<K, V> nestedMap = new HashMap<K, V>();

        @Override
        public Set<Map.Entry<K, V>> entrySet() {
            return nestedMap.entrySet();
        }

        @Override
        public V put(K key, V value) {
            return nestedMap.put(key, value);
        }

        private void writeObject(java.io.ObjectOutputStream out) throws IOException {
            throw new NotSerializableException();
        }

        private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
            throw new NotSerializableException();
        }
    }
}
