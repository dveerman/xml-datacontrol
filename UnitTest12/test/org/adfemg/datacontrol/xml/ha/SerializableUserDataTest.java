package org.adfemg.datacontrol.xml.ha;

import java.util.Map;

import org.adfemg.datacontrol.xml.utils.XmlFactory;
import org.adfemg.datacontrol.xml.utils.XmlParseUtils;

import static org.hamcrest.CoreMatchers.instanceOf;

import static org.junit.Assert.*;
import org.junit.Test;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.SAXException;
//import static org.hamcrest.CoreMatchers.*;
//import static org.junit.Assert.*;

public class SerializableUserDataTest {

    @Test
    public void getUserDataMapForSimpleElement() throws SAXException {
        Document document = XmlParseUtils.parse("<element/>");
        Element element = document.getDocumentElement();
        SerializableUserData dataMap = SerializableUserData.forNode(element);
        assertThat(dataMap, instanceOf(Map.class));
        assertThat(dataMap, instanceOf(SerializableUserData.class));
    }

    @Test
    public void getUserDataMapForDocument() throws SAXException {
        Document document = XmlParseUtils.parse("<element/>");
        SerializableUserData dataMap = SerializableUserData.forNode(document);
        assertThat(dataMap, instanceOf(Map.class));
        assertThat(dataMap, instanceOf(SerializableUserData.class));
    }

    @Test
    public void userDataMapShouldRetainState() throws SAXException {
        Document document = XmlParseUtils.parse("<element/>");
        Element element = document.getDocumentElement();
        SerializableUserData dataMap = SerializableUserData.forNode(element);
        dataMap.put("key", "value");
        assertEquals("UserData.get should return value from UserData.put", "value", dataMap.get("key"));
    }

    @Test
    public void userDataMapWithStateShouldBeRegisteredWithSerializableDocument() throws SAXException {
        Document document = XmlParseUtils.parse("<element/>");
        Element element = document.getDocumentElement();
        SerializableUserData dataMap = SerializableUserData.forNode(element);
        dataMap.put("key", "value");
        assertTrue("Element registered with UserDataRegistry after creating UserDataMap",
                   SerializableDocument.forDocument(document).nodeHasState(element));
    }

    @Test
    public void clonedNodeShouldGetSameUserData() throws SAXException {
        Document document = XmlParseUtils.parse("<element/>");
        Element element = document.getDocumentElement();
        SerializableUserData dataMap = SerializableUserData.forNode(element);
        dataMap.put("key", "value");
        Element clone = (Element) element.cloneNode(true);
        assertEquals("Cloned node should get same UserData", "value", SerializableUserData.forNode(clone).get("key"));
    }

    @Test
    public void clonedNodeShouldGetClonedUserData() throws SAXException {
        Document document = XmlParseUtils.parse("<element/>");
        Element element = document.getDocumentElement();
        SerializableUserData dataMap = SerializableUserData.forNode(element);
        dataMap.put("key", "value");
        Element clone = (Element) element.cloneNode(true);
        // change source node UserData after cloning it
        dataMap.put("key", "newValue");
        assertEquals("Cloned node should get same UserData", "value", SerializableUserData.forNode(clone).get("key"));
    }

    @Test
    public void deletedNodeShouldBeRemovedFromUserDataRegistry() {
        // this one is hard to test since we can't look into the nodes in the UserDataRegistry
        // we'll test this with UserDataRegistry itself using (de)serialization to see if the node data was really gone
    }

    @Test
    public void importedNodeShouldNotGetUserData() throws SAXException {
        Document document = XmlParseUtils.parse("<element/>");
        Element element = document.getDocumentElement();
        SerializableUserData dataMap = SerializableUserData.forNode(element);
        dataMap.put("key", "value");
        Document doc2 = XmlFactory.newDocument();
        // copies node from source document without altering/removing source node
        Element el2 = (Element) doc2.importNode(element, true);
        assertNull("Imported Node should not have data", SerializableUserData.forNode(el2).get("key"));
        assertEquals("Source Node should still have data", "value", SerializableUserData.forNode(element).get("key"));
    }

    @Test
    public void adoptedNodeShouldNotGetUserData() throws SAXException {
        Document document = XmlParseUtils.parse("<element/>");
        Element element = document.getDocumentElement();
        SerializableUserData dataMap = SerializableUserData.forNode(element);
        dataMap.put("key", "value");
        Document doc2 = XmlFactory.newDocument();
        // move node from source document so it is removed from source
        Element el2 = (Element) doc2.adoptNode(element);
        assertNull("Adopted Node should not have data", SerializableUserData.forNode(el2).get("key"));
        assertNull("Source element should no longer exist", document.getDocumentElement());
    }

    @Test
    public void renamedNodeShouldKeepUserData() throws SAXException {
        Document document = XmlParseUtils.parse("<element/>");
        Element element = document.getDocumentElement();
        SerializableUserData dataMap = SerializableUserData.forNode(element);
        dataMap.put("key", "value");
        document.renameNode(element, "newnamespace", "newname");
        assertEquals("Moved Node should have data", "value",
                     SerializableUserData.forNode(document.getDocumentElement()).get("key"));
        assertEquals("newnamespace", document.getDocumentElement().getNamespaceURI());
        assertEquals("newname", document.getDocumentElement().getLocalName());
    }

}
