package org.adfemg.datacontrol.xml.provider.structure;

import java.io.Reader;
import java.io.StringReader;

import java.net.URL;

import oracle.binding.meta.AttributeDefinition;
import oracle.binding.meta.Definition;
import oracle.binding.meta.DefinitionContainer;

import oracle.xml.parser.schema.XMLSchema;
import oracle.xml.parser.schema.XSDBuilder;
import oracle.xml.parser.schema.XSDException;

import org.adfemg.datacontrol.xml.XMLBuilderHelper;
import org.adfemg.datacontrol.xml.data.XMLDCCollection;
import org.adfemg.datacontrol.xml.data.XMLDCElement;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;
import org.adfemg.datacontrol.xml.utils.XmlParseUtils;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class SchemaStructureProviderTest {

    private XMLDCElement xmldcDepartmentElement;
    private SchemaStructureProvider structureProvider;
    private TypeMapper typeMapper;

    @Before
    public void setUp() throws Exception {
        XMLBuilderHelper xmlBuilder = new XMLBuilderHelper();
        xmldcDepartmentElement = xmlBuilder.buildDepartmentXMLDCElement(null, false);
        structureProvider = xmlBuilder.getStructureProvider();
        typeMapper = xmlBuilder.getTypeMapper();
    }

    private XMLSchema parseSchemaDocument(String xsd) throws XSDException {
        Reader reader = new StringReader(xsd);
        URL baseUrl = null;
        XSDBuilder builder = new XSDBuilder();
        return builder.build(reader, baseUrl);
    }

    @Test
    public void testRoundtrip() throws Exception {
        assertEquals("initial root element", "<DepartmentList xmlns=\"http://xmlns.example.com\"/>",
                     XmlParseUtils.nodeToString(xmldcDepartmentElement.getElement()));
        XMLDCCollection departments = (XMLDCCollection) xmldcDepartmentElement.get("Department");
        XMLDCElement dept1 = departments.createElement(-1, xmldcDepartmentElement);
        assertEquals("added single nameless department",
                     "<DepartmentList xmlns=\"http://xmlns.example.com\"><Department/></DepartmentList>",
                     XmlParseUtils.nodeToString(xmldcDepartmentElement.getElement()));
        dept1.put("name", "department1");
        assertEquals("named first department",
                     "<DepartmentList xmlns=\"http://xmlns.example.com\"><Department><name>department1</name></Department></DepartmentList>",
                     XmlParseUtils.nodeToString(xmldcDepartmentElement.getElement()));
        XMLDCElement dept2 = departments.createElement(0, xmldcDepartmentElement);
        dept2.put("name", "department2-before1");
        assertEquals("added second department before initial department",
                     "<DepartmentList xmlns=\"http://xmlns.example.com\"><Department><name>department2-before1</name></Department><Department><name>department1</name></Department></DepartmentList>",
                     XmlParseUtils.nodeToString(xmldcDepartmentElement.getElement()));
    }

    /**
     * @see SchemaStructureProvider#buildStructure(oracle.xml.parser.schema.XSDNode,String,org.adfemg.datacontrol.xml.provider.typemap.TypeMapper)
     */
    @Test
    public void testBuildStructure() throws Exception {
        String xsd = XMLBuilderHelper.SCHEMA_ROOT_ELEM;
        xsd += "  <xsd:element name='TestElement'>";
        xsd += "    <xsd:complexType>";
        xsd += "      <xsd:sequence>";
        xsd += "        <xsd:element name='id' type='xsd:string'/>";
        xsd += "      </xsd:sequence>";
        xsd += "    </xsd:complexType>";
        xsd += "  </xsd:element>";
        xsd += "</xsd:schema>";

        String structName = "datacontrol.method";
        // TODO: why fail when no . in rootName
        MovableStructureDefinition struct =
            structureProvider.buildStructure(parseSchemaDocument(xsd).getElement(XMLBuilderHelper.NAMESPACE_URI,
                                                                                 "TestElement"), structName,
                                             typeMapper);

        assertEquals("full name of created structure should match requested name", structName, struct.getFullName());
        assertEquals("simple name of created structure should match requested name", "method", struct.getName());

        DefinitionContainer attrs = struct.getAttributeDefinitions();
        AttributeDefinition id = (AttributeDefinition) attrs.find("id");
        assertNotNull("id attribute should exist", id);
        assertEquals("id attribute full name", structName + ".id", id.getFullName());
        assertEquals("id attribute name", "id", id.getName());
        assertEquals("id javaTypeString", String.class.getName(), id.getJavaTypeString());
        assertEquals("id sourceTypeString", String.class.getName(), id.getSourceTypeString());
        assertEquals("id definitionType", Definition.TYPE_ATTRIBUTE, id.getDefinitionType());
    }

}
