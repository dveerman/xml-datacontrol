package org.adfemg.datacontrol.xml.provider.structure;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ SchemaStructureProviderTest.class })
public class StructureProviderSuite {
}
