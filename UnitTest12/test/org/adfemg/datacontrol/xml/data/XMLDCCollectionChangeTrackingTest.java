package org.adfemg.datacontrol.xml.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import oracle.xml.parser.schema.XSDException;

import org.adfemg.datacontrol.xml.XMLBuilderHelper;
import org.adfemg.datacontrol.xml.utils.ChangeTrackingUtils;
import org.adfemg.datacontrol.xml.utils.XmlParseUtils;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import org.w3c.dom.Element;

public class XMLDCCollectionChangeTrackingTest {

    private XMLDCElement xmldcDepartmentList;
    private XMLDCCollection departments;
    private XMLDCElement departmentFinance;

    @Before
    public void setUp() throws Exception {
        XMLBuilderHelper xmlBuilder = new XMLBuilderHelper();

        // create new department Element with name 'finance'
        List<Element> elements = new ArrayList<>();
        Element finance = xmlBuilder.createDepartment("finance", "The Hague");
        elements.add(finance);

        // create new DepartmentList Root Element with changtracking on and initial finance department
        xmldcDepartmentList = xmlBuilder.buildDepartmentXMLDCElement(elements, true);
        departments = (XMLDCCollection) xmldcDepartmentList.get("Department");
        departmentFinance = departments.get(0);
    }

    @Test
    public void testInitialCollection() {
        assertTrue(xmldcDepartmentList.isChangeTracking());
        // assert that the departmentlist is created with one sales with Processing Instruction and one finance department without PI
        String xmlString = XmlParseUtils.nodeToString(xmldcDepartmentList.getElement());
        String expectedString =
            "<DepartmentList xmlns=\"http://xmlns.example.com\"><Department location=\"The Hague\"><name>finance</name></Department></DepartmentList>";
        assertEquals("created departmentlist is not correct, expecting single 'finance' department", expectedString,
                     xmlString);
        assertSame("Department 'finance' references are not the same", departmentFinance, departments.get(0));
        assertFalse("finance isInserted", departmentFinance.isInserted());
        assertFalse("finance isDeleted", departmentFinance.isDeleted());
    }

    @Test
    public void testCreateElement() {
        // create a new marketing department and add it to the departmentlist
        XMLDCElement departmentMarketing = departments.createElement(0, xmldcDepartmentList);
        departmentMarketing.put("name", "marketing");

        // assert that the departmentlist has two departments
        String xmlString = XmlParseUtils.nodeToString(xmldcDepartmentList.getElement());
        String expectedString =
            "<DepartmentList xmlns=\"http://xmlns.example.com\"><Department><?change-inserted ?><name><?change-value old=\"\"?>marketing</name></Department><Department location=\"The Hague\"><name>finance</name></Department></DepartmentList>";
        assertEquals("created departmentlist is not correct,  expecting a 'marketing' and 'finance' department",
                     expectedString, xmlString);
        assertSame("Department 'finance' references are not the same", departmentFinance, departments.get(1));
        assertSame("Department 'marketing' references are not the same", departmentMarketing, departments.get(0));
        assertTrue("marketing isInserted", departmentMarketing.isInserted());
    }

    @Test
    public void testRemove() {
        // remove the sales department
        XMLDCElement olddept = departments.remove(0);

        // assert that the finance department has processing instruction change-deleted
        String xmlString = XmlParseUtils.nodeToString(xmldcDepartmentList.getElement());
        String expectedString =
            "<DepartmentList xmlns=\"http://xmlns.example.com\"><Department location=\"The Hague\"><?change-deleted ?><name>finance</name></Department></DepartmentList>";
        assertEquals("departmentlist is not correct, finance department must have processing instruction 'change-deleted'",
                     expectedString, xmlString);
        assertEquals("departmentList should contain zero items", 0, departments.size());
        assertTrue("finance isDeleted", departmentFinance.isDeleted());
        assertTrue("remove result isDeleted", olddept.isDeleted());
    }

    @Test
    public void testIgnoreDeletedElement() throws XSDException {
        XMLBuilderHelper xmlBuilder = new XMLBuilderHelper();
        // create new department Element with name 'finance' and change-deleted processing instruction
        Element finance = xmlBuilder.createDepartment("finance", "The Hague");
        ChangeTrackingUtils.markDeleted(finance);

        // create new DepartmentList Root Element with changtracking on and initial finance department
        xmldcDepartmentList = xmlBuilder.buildDepartmentXMLDCElement(Collections.singletonList(finance), true);
        departments = (XMLDCCollection) xmldcDepartmentList.get("Department");
        departmentFinance = null;

        assertEquals("departmentList should contain zero items", 0, departments.size());
    }

    @Test
    public void testCreateAndRemove() {
        // create new marketing department and remove it again
        XMLDCElement departmentMarketing = departments.createElement(0, xmldcDepartmentList);
        departmentMarketing.put("name", "marketing");
        departments.remove(0);
        // assert that the marketing department has processing instruction change-deleted
        String xmlString = XmlParseUtils.nodeToString(xmldcDepartmentList.getElement());
        String expectedString =
            "<DepartmentList xmlns=\"http://xmlns.example.com\"><Department><?change-deleted ?><?change-inserted ?><name><?change-value old=\"\"?>marketing</name></Department><Department location=\"The Hague\"><name>finance</name></Department></DepartmentList>";
        assertEquals("departmentlist is not correct, finance department must have processing instructions 'change-inserter' and 'change-deleted'",
                     expectedString, xmlString);
        assertEquals("departmentList should contain one item", 1, departments.size());
        assertSame("departmentList should contain only finance department", departmentFinance, departments.get(0));
        assertTrue("marketing isInserted", departmentMarketing.isInserted());
        assertTrue("marketing isDeleted", departmentMarketing.isDeleted());
    }

}
