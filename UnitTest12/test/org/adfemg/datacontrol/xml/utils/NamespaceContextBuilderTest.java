package org.adfemg.datacontrol.xml.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;

import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Test;


public class NamespaceContextBuilderTest {

    @Test
    public void generatesPrefix() {
        assertEquals("ns0", new NamespaceContextBuilder().getPrefix("xmlns.example.com/test"));
    }

    @Test
    public void generatesUniquePrefix() {
        NamespaceContextBuilder builder = new NamespaceContextBuilder();
        String prefix1 = builder.getPrefix("xmlns.example.com/test");
        String prefix2 = builder.getPrefix("xmlns.example.com/anotherTest");
        assertNotEquals("prefixes for unique namespaces should be unique", prefix1, prefix2);
    }

    @Test
    public void retainsPrefix() {
        NamespaceContextBuilder builder = new NamespaceContextBuilder();
        String prefix1 = builder.getPrefix("xmlns.example.com/test");
        String prefix2 = builder.getPrefix("xmlns.example.com/test");
        assertEquals("prefixes for same namespaces should be same", prefix1, prefix2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void noEmptyNamespace() {
        new NamespaceContextBuilder().getPrefix(null);
    }

    @Test
    public void defaultXmlNamespace() {
        assertEquals(XMLConstants.XML_NS_PREFIX, new NamespaceContextBuilder().getPrefix(XMLConstants.XML_NS_URI));
    }

    @Test
    public void defaultEmptyNamespace() {
        assertEquals(XMLConstants.DEFAULT_NS_PREFIX, new NamespaceContextBuilder().getPrefix(XMLConstants.NULL_NS_URI));
    }

    @Test
    public void defaultNamespacesFromEmptyBuilder() {
        NamespaceContext ctx = new NamespaceContextBuilder().build();
        assertEquals("lookup xml prefix", XMLConstants.XML_NS_URI, ctx.getNamespaceURI(XMLConstants.XML_NS_PREFIX));
        assertEquals("lookup xmlns prefix", XMLConstants.XMLNS_ATTRIBUTE_NS_URI,
                     ctx.getNamespaceURI(XMLConstants.XMLNS_ATTRIBUTE));
        assertEquals("lookup default prefix", XMLConstants.NULL_NS_URI,
                     ctx.getNamespaceURI(XMLConstants.DEFAULT_NS_PREFIX));
    }

    @Test(expected = IllegalArgumentException.class)
    public void notAddEmptyNamespace() {
        new NamespaceContextBuilder().add(null, "ns0");
    }

    @Test(expected = IllegalArgumentException.class)
    public void notAddEmptyPrefix() {
        new NamespaceContextBuilder().add("xmlns.example.com", null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void notAddDefaultNamespace() {
        new NamespaceContextBuilder().add(XMLConstants.XML_NS_URI, "ns0");
    }

    @Test
    public void addCustomPrefix() {
        NamespaceContextBuilder builder = new NamespaceContextBuilder();
        builder.add("xmlns.example.com/test", "xyz");
        assertEquals("xyz", builder.getPrefix("xmlns.example.com/test"));
    }

    @Test
    public void builderPattern() {
        NamespaceContextBuilder builder = new NamespaceContextBuilder();
        builder.add("xmlns.example.com/test", "xyz");
        assertSame("builder should return same instance", builder, builder.add("xmlns.example.com/otherTest", "zzz"));
    }

    @Test
    public void customPrefixToContext() {
        final NamespaceContext ctx = new NamespaceContextBuilder().add("xmlns.example.com/test", "xyz").build();
        assertEquals("xmlns.example.com/test", ctx.getNamespaceURI("xyz"));
    }

    @Test
    public void customNamespaceToContext() {
        final NamespaceContext ctx = new NamespaceContextBuilder().add("xmlns.example.com/test", "xyz").build();
        assertEquals("xyz", ctx.getPrefix("xmlns.example.com/test"));
    }

    @Test(expected=IllegalArgumentException.class)
    public void singlePrefixPerNamespace() {
        new NamespaceContextBuilder().add("xmlns.example.com/test", "xyz").add("xmlns.example.com/test", "zzz");
    }

    @Test
    @Ignore // we don't support multiple prefixes per namespace (yet, might be added in future)
    public void customMultiPrefixNamespaceToContext() {
        final NamespaceContext ctx =
            new NamespaceContextBuilder().add("xmlns.example.com/test", "xyz").add("xmlns.example.com/test",
                                                                                   "zzz").build();
        // compare as Set as order is not relevant
        assertEquals("reverse lookups namespace", new HashSet(Arrays.asList("xyz", "zzz")),
                     new HashSet(iterToList(ctx.getPrefixes("xmlns.example.com/test"))));

        assertEquals("xyz", ctx.getPrefix("xmlns.example.com/test"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void noPrefixRemap() {
        new NamespaceContextBuilder().add("xmlns.example.com/test", "ns0").add("xmlns.example.com/otherTest", "ns0");
    }

    @Test(expected = IllegalArgumentException.class)
    public void notAddDefaultNamespacePrefix() {
        new NamespaceContextBuilder().add("xmlns.example.com", XMLConstants.XML_NS_PREFIX);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void getNamespaceUnsupported() {
        new NamespaceContextBuilder().getNamespaceURI("ns0");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void getPrefixes() {
        new NamespaceContextBuilder().getPrefixes("xmlns.example.com");
    }

    private List iterToList(final Iterator iter) {
        List retval = new ArrayList(10);
        while (iter.hasNext()) {
            retval.add(iter.next());
        }
        return retval;
    }

}
