package org.adfemg.datacontrol.xml.utils;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
                    NamespaceContextBuilderTest.class, NamespaceContextTest.class, XmlParseUtilsTest.class,
                    ChangeTrackingUtilsTest.class, DomUtilsTest.class
    })
public class UtilsSuite {
}
