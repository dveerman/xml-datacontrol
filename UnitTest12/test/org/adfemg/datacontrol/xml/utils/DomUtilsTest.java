package org.adfemg.datacontrol.xml.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import java.util.List;

import javax.xml.namespace.QName;

import static org.junit.Assert.*;
import org.junit.Test;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.ProcessingInstruction;

import org.xml.sax.SAXException;

public class DomUtilsTest {

    // *** private constructor ***

    @Test
    public void privateConstructorTest() throws InstantiationException, IllegalAccessException,
                                                InvocationTargetException {
        Constructor<?>[] constructors = DomUtils.class.getDeclaredConstructors();
        assertEquals(1, constructors.length);
        Constructor<?> constructor = constructors[0];
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        // instantiate once for code coverage reporting
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    // *** findFirstChildElement(Element, String, String) ***

    @Test
    public void findFirstChildElementDefaultNamespaceTest() throws SAXException {
        Element dept =
            XmlParseUtils.parse("<Department><name xmlns=\"xmlns.example.com/test\">sales</name></Department>").getDocumentElement();
        Element name = DomUtils.findFirstChildElement(dept, "xmlns.example.com/test", "name");
        assertNotNull("name element with xmlns=\"xmlns.example.com/test\" should be found", name);
    }

    @Test
    public void findFirstChildElementEmptyNamespaceTest() throws SAXException {
        Element dept = XmlParseUtils.parse("<Department><name>sales</name></Department>").getDocumentElement();
        Element name = DomUtils.findFirstChildElement(dept, "", "name");
        assertNotNull("name element with xmlns=\"\" should be found", name);
    }

    @Test
    public void findFirstChildElementLocalNamespaceTest() throws SAXException {
        Element dept =
            XmlParseUtils.parse("<Department><x:name xmlns:x=\"xmlns.example.com/test\">sales</x:name></Department>").getDocumentElement();
        Element name = DomUtils.findFirstChildElement(dept, "xmlns.example.com/test", "name");
        assertNotNull("name element with xmlns:x=\"xmlns.example.com/test\" should be found", name);
    }

    @Test
    public void findFirstChildElementLocalNamespaceDifferentTest() throws SAXException {
        Element dept =
            XmlParseUtils.parse("<Department xmlns=\"xmlns.example.com/test\"><x:name xmlns:x=\"xmlns.example.com/test2\">sales</x:name></Department>").getDocumentElement();
        Element name = DomUtils.findFirstChildElement(dept, "xmlns.example.com/test", "name");
        assertNull("name element with xmlns:x=\"xmlns.example.com/test\" should not be found", name);
    }

    @Test
    public void findFirstChildElementEmptyParentTest() throws SAXException {
        Element dept = XmlParseUtils.parse("<Department/>").getDocumentElement();
        Element name = DomUtils.findFirstChildElement(dept, "xmlns.example.com/test", "name");
        assertNull("no child should be found in empty parent element", name);
    }

    // *** findFirstChildElement(Element, QName) ***

    @Test
    public void findFirstChildElementDefaultNamespaceQNameTest() throws SAXException {
        Element dept =
            XmlParseUtils.parse("<Department><name xmlns=\"xmlns.example.com/test\">sales</name></Department>").getDocumentElement();
        Element name = DomUtils.findFirstChildElement(dept, new QName("xmlns.example.com/test", "name"));
        assertNotNull("name element with xmlns=\"xmlns.example.com/test\" should be found", name);
    }

    @Test
    public void findFirstChildElementEmptyNamespaceQNameTest() throws SAXException {
        Element dept = XmlParseUtils.parse("<Department><name>sales</name></Department>").getDocumentElement();
        Element name = DomUtils.findFirstChildElement(dept, new QName("", "name"));
        assertNotNull("name element with xmlns=\"\" should be found", name);
    }

    @Test
    public void findFirstChildElementLocalNamespaceQNameTest() throws SAXException {
        Element dept =
            XmlParseUtils.parse("<Department><x:name xmlns:x=\"xmlns.example.com/test\">sales</x:name></Department>").getDocumentElement();
        Element name = DomUtils.findFirstChildElement(dept, new QName("xmlns.example.com/test", "name"));
        assertNotNull("name element with xmlns:x=\"xmlns.example.com/test\" should be found", name);
    }

    @Test
    public void findFirstChildElementLocalNamespaceQNameDifferentTest() throws SAXException {
        Element dept =
            XmlParseUtils.parse("<Department xmlns=\"xmlns.example.com/test\"><x:name xmlns:x=\"xmlns.example.com/test2\">sales</x:name></Department>").getDocumentElement();
        Element name = DomUtils.findFirstChildElement(dept, new QName("xmlns.example.com/test", "name"));
        assertNull("name element with xmlns:x=\"xmlns.example.com/test\" should not be found", name);
    }

    // *** findFirstChildElement(Element, String, String) ***

    @Test
    public void findFirstChildElementSingleChildTest() throws SAXException {
        Element dept = XmlParseUtils.parse("<Department><name>sales</name></Department>").getDocumentElement();
        Element name = DomUtils.findFirstChildElement(dept);
        assertNotNull("name element should be found", name);
        assertEquals("name", name.getLocalName());
    }

    @Test
    public void findFirstChildElementNoChildTest() throws SAXException {
        Element dept = XmlParseUtils.parse("<Department></Department>").getDocumentElement();
        assertNull("no child element should be found", DomUtils.findFirstChildElement(dept));
    }

    @Test
    public void findFirstChildElementMultiChildTest() throws SAXException {
        Element dept =
            XmlParseUtils.parse("<Department><name>sales</name><location>Amsterdam</location></Department>").getDocumentElement();
        Element name = DomUtils.findFirstChildElement(dept);
        assertNotNull("name element should be found", name);
        assertEquals("name", name.getLocalName());
    }

    @Test
    public void findFirstChildElementWhitespaceTest() throws SAXException {
        Element dept = XmlParseUtils.parse("<Department>  \n  <name>sales</name></Department>").getDocumentElement();
        Element name = DomUtils.findFirstChildElement(dept);
        assertNotNull("name element should be found after whitespace", name);
        assertEquals("name", name.getLocalName());
    }

    @Test
    public void findFirstChildElementTextTest() throws SAXException {
        Element dept = XmlParseUtils.parse("<Department>some text<name>sales</name></Department>").getDocumentElement();
        Element name = DomUtils.findFirstChildElement(dept);
        assertNotNull("name element should be found after text", name);
        assertEquals("name", name.getLocalName());
    }

    @Test
    public void findFirstChildElementProcessingInstructionTest() throws SAXException {
        Element dept =
            XmlParseUtils.parse("<Department><?some-pi ?><name>sales</name></Department>").getDocumentElement();
        Element name = DomUtils.findFirstChildElement(dept);
        assertNotNull("name element should be found after processing instruction", name);
        assertEquals("name", name.getLocalName());
    }

    // *** findChildProcessingInstructions ***

    @Test
    public void findChildProcessingInstructionsTest() throws SAXException {
        Element dept = XmlParseUtils.parse("<Department><?pi-target picontent?></Department>").getDocumentElement();
        List<ProcessingInstruction> pis = DomUtils.findChildProcessingInstructions(dept, "pi-target");
        assertEquals(1, pis.size());
    }

    @Test
    public void findChildProcessingInstructionsTargetTest() throws SAXException {
        Element dept =
            XmlParseUtils.parse("<Department><?pi-other-target?><?pi-target picontent?></Department>").getDocumentElement();
        List<ProcessingInstruction> pis = DomUtils.findChildProcessingInstructions(dept, "pi-target");
        assertEquals(1, pis.size());
        assertEquals("pi-target", pis.get(0).getTarget());
        assertEquals("picontent", pis.get(0).getData());
    }

    // *** addChildProcessingInstruction ***

    @Test
    public void addChildProcessingInstructionTest() throws SAXException {
        Element dept = XmlParseUtils.parse("<Department/>").getDocumentElement();
        ProcessingInstruction pi = DomUtils.addChildProcessingInstruction(dept, "pi-target");
        assertNotNull("processinginstruction should be created", pi);
        List<ProcessingInstruction> pis = DomUtils.findChildProcessingInstructions(dept, "pi-target");
        assertEquals(1, pis.size());
    }

    @Test
    public void addChildProcessingInstructionSimpleContentTest() throws SAXException {
        Element dept = XmlParseUtils.parse("<Department>Sales</Department>").getDocumentElement();
        ProcessingInstruction pi = DomUtils.addChildProcessingInstruction(dept, "pi-target");
        assertNotNull("processinginstruction should be created", pi);
        List<ProcessingInstruction> pis = DomUtils.findChildProcessingInstructions(dept, "pi-target");
        assertEquals(1, pis.size());
    }

    // *** setTextContent ***

    @Test
    public void setTextContentTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee><FirstName>John</FirstName></Employee>").getDocumentElement();
        Element firstname = DomUtils.findFirstChildElement(employee, "FirstName");
        DomUtils.setTextContent(firstname, "Mike");
        assertEquals("<Employee><FirstName>Mike</FirstName></Employee>", XmlParseUtils.nodeToString(employee));
    }

    @Test
    public void setTextContentTextToEmptyTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee><FirstName>John</FirstName></Employee>").getDocumentElement();
        Element firstname = DomUtils.findFirstChildElement(employee, "FirstName");
        DomUtils.setTextContent(firstname, "");
        assertEquals("<Employee><FirstName/></Employee>", XmlParseUtils.nodeToString(employee));
    }

    @Test
    public void setTextContentTextToNullTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee><FirstName>John</FirstName></Employee>").getDocumentElement();
        Element firstname = DomUtils.findFirstChildElement(employee, "FirstName");
        DomUtils.setTextContent(firstname, null);
        assertEquals("<Employee><FirstName/></Employee>", XmlParseUtils.nodeToString(employee));
    }

    @Test
    public void setTextContentEmptyToTextTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee><FirstName/></Employee>").getDocumentElement();
        Element firstname = DomUtils.findFirstChildElement(employee, "FirstName");
        DomUtils.setTextContent(firstname, "John");
        assertEquals("<Employee><FirstName>John</FirstName></Employee>", XmlParseUtils.nodeToString(employee));
    }

    @Test
    public void setTextContentEmptyToEmptyTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee><FirstName/></Employee>").getDocumentElement();
        Element firstname = DomUtils.findFirstChildElement(employee, "FirstName");
        DomUtils.setTextContent(firstname, "");
        assertEquals("<Employee><FirstName/></Employee>", XmlParseUtils.nodeToString(employee));
    }

    @Test
    public void setTextContentEmptyToNullTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee><FirstName/></Employee>").getDocumentElement();
        Element firstname = DomUtils.findFirstChildElement(employee, "FirstName");
        DomUtils.setTextContent(firstname, null);
        assertEquals("<Employee><FirstName/></Employee>", XmlParseUtils.nodeToString(employee));
    }

    @Test
    public void setTextContentWithProcessingInstructionTest() throws SAXException {
        Element employee =
            XmlParseUtils.parse("<Employee><FirstName><?pi-target ?>John</FirstName></Employee>").getDocumentElement();
        Element firstname = DomUtils.findFirstChildElement(employee, "FirstName");
        DomUtils.setTextContent(firstname, "Mike");
        assertEquals("<Employee><FirstName><?pi-target ?>Mike</FirstName></Employee>",
                     XmlParseUtils.nodeToString(employee));
        // normal DOM manipulation does replace all children, including PI
        firstname.setTextContent("Steven");
        assertEquals("<Employee><FirstName>Steven</FirstName></Employee>", XmlParseUtils.nodeToString(employee));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setTextContentSplitProcessingInstructionTest() throws SAXException {
        Element employee =
            XmlParseUtils.parse("<Employee><FirstName>Jo<?pi-target ?>hn</FirstName></Employee>").getDocumentElement();
        Element firstname = DomUtils.findFirstChildElement(employee, "FirstName");
        DomUtils.setTextContent(firstname, "Mike");
    }

    // *** getQName ***

    @Test
    public void getQNameTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<e:Employee xmlns:e=\"xmlns.example.com/emp\"/>").getDocumentElement();
        QName qname = DomUtils.getQName(employee);
        assertEquals("Employee", qname.getLocalPart());
        assertEquals("xmlns.example.com/emp", qname.getNamespaceURI());
    }

    @Test
    public void getQNameDefaultNSTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee xmlns=\"xmlns.example.com/emp\"/>").getDocumentElement();
        QName qname = DomUtils.getQName(employee);
        assertEquals("Employee", qname.getLocalPart());
        assertEquals("xmlns.example.com/emp", qname.getNamespaceURI());
    }

    @Test
    public void getQNameEmptyNSTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee/>").getDocumentElement();
        QName qname = DomUtils.getQName(employee);
        assertEquals("Employee", qname.getLocalPart());
        assertEquals("", qname.getNamespaceURI());
    }

    @Test
    public void getQNameNullTest() throws SAXException {
        assertNull("null Node should return null QName", DomUtils.getQName(null));
    }

    // *** isElementNS ***

    @Test
    public void isElementNSTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<e:Employee xmlns:e=\"xmlns.example.com/emp\"/>").getDocumentElement();
        assertTrue(DomUtils.isElementNS(employee, "xmlns.example.com/emp", "Employee"));
        assertFalse(DomUtils.isElementNS(employee, "xmlns.example.com/emp", "e:Employee"));
        assertFalse(DomUtils.isElementNS(employee, "", "Employee"));
        assertFalse(DomUtils.isElementNS(employee, "", "e:Employee"));
        assertFalse(DomUtils.isElementNS(employee, null, "Employee"));
        assertFalse(DomUtils.isElementNS(employee, null, "e:Employee"));
    }

    @Test
    public void isElementNSDefaultNSTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee xmlns=\"xmlns.example.com/emp\"/>").getDocumentElement();
        assertTrue(DomUtils.isElementNS(employee, "xmlns.example.com/emp", "Employee"));
        assertFalse(DomUtils.isElementNS(employee, "", "Employee"));
        assertFalse(DomUtils.isElementNS(employee, null, "Employee"));
    }

    @Test
    public void isElementNSEmptyNSTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee/>").getDocumentElement();
        assertTrue(DomUtils.isElementNS(employee, "", "Employee"));
    }

    @Test
    public void isElementNSNonElementTest() throws SAXException {
        Element employee =
            XmlParseUtils.parse("<x:Employee x:name=\"john\" xmlns:x=\"xmlns.example.com/emp\"/>").getDocumentElement();
        Attr nameAttr = employee.getAttributeNodeNS("xmlns.example.com/emp", "name");
        assertNotNull(nameAttr);
        assertEquals("xmlns.example.com/emp", nameAttr.getNamespaceURI());
        assertEquals("name", nameAttr.getLocalName());
        assertFalse(DomUtils.isElementNS(nameAttr, nameAttr.getNamespaceURI(), nameAttr.getLocalName()));
    }

    // *** END OF TEST ***

}
