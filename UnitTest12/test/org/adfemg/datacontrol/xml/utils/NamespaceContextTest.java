package org.adfemg.datacontrol.xml.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class NamespaceContextTest {

    private static final String PREFIX0 = "ns0";
    private static final String PREFIX1 = "ns1";
    private static final String PREFIX_FAKE = "xxx";
    private static final String NAMESPACE0 = "xmlns.example.com/namespace";
    private static final String NAMESPACE_FAKE = "fake-namespace";
    private static final Map<String, String> MAP0 = Collections.singletonMap(PREFIX0, NAMESPACE0);

    private NamespaceContextImpl emptyContext;

    @Before
    public void setUp() throws Exception {
        emptyContext = new NamespaceContextImpl(Collections.<String, String>emptyMap());
    }

    @Test
    public void defaultNamespaces() {
        // prefix to namespace lookup
        assertEquals("lookup xml prefix", XMLConstants.XML_NS_URI,
                     emptyContext.getNamespaceURI(XMLConstants.XML_NS_PREFIX));
        assertEquals("lookup xmlns prefix", XMLConstants.XMLNS_ATTRIBUTE_NS_URI,
                     emptyContext.getNamespaceURI(XMLConstants.XMLNS_ATTRIBUTE));
        assertEquals("lookup default prefix", XMLConstants.NULL_NS_URI,
                     emptyContext.getNamespaceURI(XMLConstants.DEFAULT_NS_PREFIX));
        // namespace to prefix lookup
        assertEquals("reverse lookup xml prefix", XMLConstants.XML_NS_PREFIX,
                     emptyContext.getPrefix(XMLConstants.XML_NS_URI));
        assertEquals("reverse lookup xmlns prefix", XMLConstants.XML_NS_PREFIX,
                     emptyContext.getPrefix(XMLConstants.XML_NS_URI));
        assertEquals("reverse lookup default prefix", XMLConstants.XML_NS_PREFIX,
                     emptyContext.getPrefix(XMLConstants.XML_NS_URI));
        // namespace to prefixes lookup
        assertEquals("reverse lookup xml prefix", Collections.singletonList(XMLConstants.XML_NS_PREFIX),
                     iterToList(emptyContext.getPrefixes(XMLConstants.XML_NS_URI)));
        assertEquals("reverse lookup xmlns prefix", Collections.singletonList(XMLConstants.XML_NS_PREFIX),
                     iterToList(emptyContext.getPrefixes(XMLConstants.XML_NS_URI)));
        assertEquals("reverse lookup default prefix", Collections.singletonList(XMLConstants.XML_NS_PREFIX),
                     iterToList(emptyContext.getPrefixes(XMLConstants.XML_NS_URI)));
    }

    @Test
    public void nonExistingLookups() {
        assertEquals("lookup non-existing prefix", null, emptyContext.getNamespaceURI(PREFIX_FAKE));
        assertEquals("reverse lookup non-existing namespace", null, emptyContext.getPrefix(NAMESPACE_FAKE));
        assertEquals("reverse lookup non-existing namespace", Collections.emptyList(),
                     iterToList(emptyContext.getPrefixes(NAMESPACE_FAKE)));
    }

    @Test
    public void namespaceSinglePrefix() {
        NamespaceContextImpl subject = new NamespaceContextImpl(MAP0);
        assertEquals("lookup by prefix", NAMESPACE0, subject.getNamespaceURI(PREFIX0));
        assertEquals("reverse lookup by namespace", PREFIX0, subject.getPrefix(NAMESPACE0));
        assertEquals("reverse lookups by namespace", Collections.singletonList(PREFIX0),
                     iterToList(subject.getPrefixes(NAMESPACE0)));
    }

    @Test
    public void namespaceMultiplePrefixes() {
        // use both prefix0 and prefix1 to map to namespace0
        Map<String, String> map = new HashMap<String, String>(2);
        map.put(PREFIX0, NAMESPACE0);
        map.put(PREFIX1, NAMESPACE0);
        NamespaceContextImpl subject = new NamespaceContextImpl(map);
        assertEquals("lookup by first prefix", NAMESPACE0, subject.getNamespaceURI(PREFIX0));
        assertEquals("lookup by second prefix", NAMESPACE0, subject.getNamespaceURI(PREFIX1));
        assertTrue("reverse lookup by namespace",
                   PREFIX0.equals(subject.getPrefix(NAMESPACE0)) || PREFIX1.equals(subject.getPrefix(NAMESPACE0)));
        // compare as Set as order is not relevant
        assertEquals("reverse lookups namespace", new HashSet(Arrays.asList(PREFIX0, PREFIX1)),
                     new HashSet(iterToList(subject.getPrefixes(NAMESPACE0))));
    }

    @Test(expected = IllegalArgumentException.class)
    public void defaultXmlPrefixReadOnly() {
        new NamespaceContextImpl(Collections.singletonMap(XMLConstants.XML_NS_PREFIX, NAMESPACE_FAKE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void defaultXmlnsPrefixReadOnly() {
        new NamespaceContextImpl(Collections.singletonMap(XMLConstants.XMLNS_ATTRIBUTE, NAMESPACE_FAKE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void defaultEmptyPrefixReadOnly() {
        new NamespaceContextImpl(Collections.singletonMap(XMLConstants.DEFAULT_NS_PREFIX, NAMESPACE_FAKE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void defaultXmlNamespaceReadOnly() {
        new NamespaceContextImpl(Collections.singletonMap(PREFIX0, XMLConstants.XML_NS_URI));
    }

    @Test(expected = IllegalArgumentException.class)
    public void defaultXmlnsNamespaceReadOnly() {
        new NamespaceContextImpl(Collections.singletonMap(PREFIX0, XMLConstants.XMLNS_ATTRIBUTE_NS_URI));
    }

    @Test(expected = IllegalArgumentException.class)
    public void defaultEmptyNamespaceReadOnly() {
        new NamespaceContextImpl(Collections.singletonMap(PREFIX0, XMLConstants.NULL_NS_URI));
    }

    @Test(expected = IllegalArgumentException.class)
    public void lookupNullPrefix() {
        emptyContext.getNamespaceURI(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void lookupPrefixByNullNamespace() {
        emptyContext.getPrefix(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void lookupPrefixesByNullNamespace() {
        emptyContext.getPrefixes(null);
    }

    private List iterToList(final Iterator iter) {
        List retval = new ArrayList(10);
        while (iter.hasNext()) {
            retval.add(iter.next());
        }
        return retval;
    }

    private String qualifiedNamespace(final String namespaceURI, final String prefix) {
        return "{" + namespaceURI + "}" + prefix;
    }


}
