package org.adfemg.datacontrol.xml.utils;

import org.adfemg.datacontrol.xml.XMLBuilderHelper;
import org.adfemg.datacontrol.xml.data.XMLDCCollection;
import org.adfemg.datacontrol.xml.data.XMLDCElement;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import org.xml.sax.SAXException;

public class ChangeTrackingUtilsTest {

    private XMLDCElement xmldcDepartmentRootElement;

    @Before
    public void setUp() throws Exception {
        XMLBuilderHelper xmlBuilder = new XMLBuilderHelper();
        xmldcDepartmentRootElement = xmlBuilder.buildDepartmentXMLDCElement(null, true);
    }

    @Test
    public void markChangingElementTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee><FirstName>John</FirstName></Employee>").getDocumentElement();
        Element firstname = DomUtils.findFirstChildElement(employee, "FirstName");
        assertEquals("isChanged", false, ChangeTrackingUtils.isChanged(firstname));
        assertNull(ChangeTrackingUtils.getOldValue(firstname));
        ChangeTrackingUtils.markChanging(firstname);
        assertEquals("John", ChangeTrackingUtils.getOldValue(firstname));
        assertEquals("<Employee><FirstName><?change-value old=\"John\"?>John</FirstName></Employee>",
                     XmlParseUtils.nodeToString(employee));
        assertEquals("isChanged", true, ChangeTrackingUtils.isChanged(firstname));
    }

    @Test
    public void markChangingElementSpecialCharsTest() throws SAXException {
        // only thing we know will fail is having ?> in the original value as this is invalid to use in a
        // processing instruction
        Element employee =
            XmlParseUtils.parse("<Employee><FirstName>Jo&quot;&lt;hn</FirstName></Employee>").getDocumentElement();
        Element firstname = DomUtils.findFirstChildElement(employee, "FirstName");
        assertNull(ChangeTrackingUtils.getOldValue(firstname));
        ChangeTrackingUtils.markChanging(firstname);
        assertEquals("Jo\"<hn", ChangeTrackingUtils.getOldValue(firstname));
        assertTrue("changing special characters",
                   XmlParseUtils.parse("<Employee><FirstName><?change-value old=\"Jo\"<hn\"?>Jo&quot;&lt;hn</FirstName></Employee>").getDocumentElement().isEqualNode(employee));
    }

    @Test
    public void markChangingElementMultiTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee><FirstName>John</FirstName></Employee>").getDocumentElement();
        Element firstname = DomUtils.findFirstChildElement(employee, "FirstName");
        assertEquals("isChanged", false, ChangeTrackingUtils.isChanged(firstname));
        assertNull(ChangeTrackingUtils.getOldValue(firstname));
        ChangeTrackingUtils.markChanging(firstname);
        DomUtils.setTextContent(firstname, "Mike");
        ChangeTrackingUtils.markChanging(firstname);
        // should only have a single processing instruction, not two
        // should also contain old value from initial test, not the second one
        assertEquals("John", ChangeTrackingUtils.getOldValue(firstname));
        assertEquals("<Employee><FirstName><?change-value old=\"John\"?>Mike</FirstName></Employee>",
                     XmlParseUtils.nodeToString(employee));
        assertEquals("isChanged", true, ChangeTrackingUtils.isChanged(firstname));
    }

    @Test
    public void markChangingEmptyElementTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee><FirstName/></Employee>").getDocumentElement();
        Element firstname = DomUtils.findFirstChildElement(employee, "FirstName");
        assertEquals("isChanged", false, ChangeTrackingUtils.isChanged(firstname));
        assertNull(ChangeTrackingUtils.getOldValue(firstname));
        ChangeTrackingUtils.markChanging(firstname);
        assertEquals("", ChangeTrackingUtils.getOldValue(firstname));
        assertEquals("<Employee><FirstName><?change-value old=\"\"?></FirstName></Employee>",
                     XmlParseUtils.nodeToString(employee));
        assertEquals("isChanged", true, ChangeTrackingUtils.isChanged(firstname));
    }

    @Test
    public void markChangingAttributeTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee firstname=\"John\"/>").getDocumentElement();
        Attr firstname = employee.getAttributeNode("firstname");
        assertEquals("isChanged", false, ChangeTrackingUtils.isChanged(firstname));
        assertNull(ChangeTrackingUtils.getOldValue(firstname));
        ChangeTrackingUtils.markChanging(firstname);
        assertEquals("John", ChangeTrackingUtils.getOldValue(firstname));
        assertEquals("<Employee firstname=\"John\"><?change-attribute-value old=\"John\" name=\"firstname\"?></Employee>",
                     XmlParseUtils.nodeToString(employee));
        assertEquals("isChanged", true, ChangeTrackingUtils.isChanged(firstname));
    }

    @Test
    public void markChangingAttributeSpecialCharsTest() throws SAXException {
        // only thing we know will fail is having ?> in the original value as this is invalid to use in a
        // processing instruction
        Element employee = XmlParseUtils.parse("<Employee firstname=\"Jo&quot;&lt;hn\"/>").getDocumentElement();
        Attr firstname = employee.getAttributeNode("firstname");
        assertNull(ChangeTrackingUtils.getOldValue(firstname));
        ChangeTrackingUtils.markChanging(firstname);
        assertEquals("Jo\"<hn", ChangeTrackingUtils.getOldValue(firstname));
        assertTrue("changing special characters",
                   XmlParseUtils.parse("<Employee firstname=\"Jo&quot;&lt;hn\"><?change-attribute-value old=\"Jo\"<hn\" name=\"firstname\"?></Employee>").getDocumentElement().isEqualNode(employee));
    }

    @Test
    public void markChangingAttributeMultiTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee firstname=\"John\"/>").getDocumentElement();
        Attr firstname = employee.getAttributeNode("firstname");
        assertEquals("isChanged", false, ChangeTrackingUtils.isChanged(firstname));
        assertNull(ChangeTrackingUtils.getOldValue(firstname));
        ChangeTrackingUtils.markChanging(firstname);
        firstname.setValue("Mike");
        ChangeTrackingUtils.markChanging(firstname);
        // should only have a single processing instruction, not two
        // should also contain old value from initial test, not the second one
        assertEquals("John", ChangeTrackingUtils.getOldValue(firstname));
        assertEquals("<Employee firstname=\"Mike\"><?change-attribute-value old=\"John\" name=\"firstname\"?></Employee>",
                     XmlParseUtils.nodeToString(employee));
        assertEquals("isChanged", true, ChangeTrackingUtils.isChanged(firstname));
    }

    @Test
    public void markChangingMultiAttributeTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee firstname=\"John\" lastname=\"Doe\"/>").getDocumentElement();
        Attr firstname = employee.getAttributeNode("firstname");
        Attr lastname = employee.getAttributeNode("lastname");
        assertEquals("isChanged", false, ChangeTrackingUtils.isChanged(firstname));
        assertEquals("isChanged", false, ChangeTrackingUtils.isChanged(lastname));
        assertNull(ChangeTrackingUtils.getOldValue(firstname));
        assertNull(ChangeTrackingUtils.getOldValue(lastname));
        ChangeTrackingUtils.markChanging(firstname);
        assertEquals("isChanged", true, ChangeTrackingUtils.isChanged(firstname));
        assertEquals("John", ChangeTrackingUtils.getOldValue(firstname));
        ChangeTrackingUtils.markChanging(lastname);
        assertEquals("John", ChangeTrackingUtils.getOldValue(firstname));
        assertEquals("Doe", ChangeTrackingUtils.getOldValue(lastname));
        assertEquals("<Employee firstname=\"John\" lastname=\"Doe\"><?change-attribute-value old=\"Doe\" name=\"lastname\"?><?change-attribute-value old=\"John\" name=\"firstname\"?></Employee>",
                     XmlParseUtils.nodeToString(employee));
        assertEquals("isChanged", true, ChangeTrackingUtils.isChanged(lastname));
    }

    @Test
    public void markDeletedTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee><FirstName/></Employee>").getDocumentElement();
        assertEquals("isDeleted", false, ChangeTrackingUtils.isDeleted(employee));
        ChangeTrackingUtils.markDeleted(employee);
        assertEquals("<Employee><?change-deleted ?><FirstName/></Employee>", XmlParseUtils.nodeToString(employee));
        assertEquals("isDeleted", true, ChangeTrackingUtils.isDeleted(employee));
    }

    @Test
    public void markDeletedMultiTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee><FirstName/></Employee>").getDocumentElement();
        assertEquals("isDeleted", false, ChangeTrackingUtils.isDeleted(employee));
        ChangeTrackingUtils.markDeleted(employee);
        ChangeTrackingUtils.markDeleted(employee);
        assertEquals("<Employee><?change-deleted ?><FirstName/></Employee>", XmlParseUtils.nodeToString(employee));
        assertEquals("isDeleted", true, ChangeTrackingUtils.isDeleted(employee));
    }

    @Test
    public void markInsertedTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee><FirstName/></Employee>").getDocumentElement();
        assertEquals("isInserted", false, ChangeTrackingUtils.isInserted(employee));
        ChangeTrackingUtils.markInserted(employee);
        assertEquals("<Employee><?change-inserted ?><FirstName/></Employee>", XmlParseUtils.nodeToString(employee));
        assertEquals("isInserted", true, ChangeTrackingUtils.isInserted(employee));
    }

    @Test
    public void markInsertedMultiTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee><FirstName/></Employee>").getDocumentElement();
        assertEquals("isInserted", false, ChangeTrackingUtils.isInserted(employee));
        ChangeTrackingUtils.markInserted(employee);
        ChangeTrackingUtils.markInserted(employee);
        assertEquals("<Employee><?change-inserted ?><FirstName/></Employee>", XmlParseUtils.nodeToString(employee));
        assertEquals("isInserted", true, ChangeTrackingUtils.isInserted(employee));
    }

    @Test
    public void markNewTextContentElementTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee><FirstName>John</FirstName></Employee>").getDocumentElement();
        Element firstname = DomUtils.findFirstChildElement(employee, "FirstName");
        assertEquals("isChanged", false, ChangeTrackingUtils.isChanged(firstname));
        assertNull(ChangeTrackingUtils.getOldValue(firstname));
        ChangeTrackingUtils.markNewTextContent(firstname);
        assertEquals("", ChangeTrackingUtils.getOldValue(firstname));
        assertEquals("<Employee><FirstName><?change-value old=\"\"?>John</FirstName></Employee>",
                     XmlParseUtils.nodeToString(employee));
        assertEquals("isChanged", true, ChangeTrackingUtils.isChanged(firstname));
    }

    @Test
    public void markNewTextContentElementMultiTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee><FirstName>John</FirstName></Employee>").getDocumentElement();
        Element firstname = DomUtils.findFirstChildElement(employee, "FirstName");
        assertEquals("isChanged", false, ChangeTrackingUtils.isChanged(firstname));
        assertNull(ChangeTrackingUtils.getOldValue(firstname));
        ChangeTrackingUtils.markNewTextContent(firstname);
        assertEquals("", ChangeTrackingUtils.getOldValue(firstname));
        firstname.setTextContent("Mike");
        ChangeTrackingUtils.markNewTextContent(firstname);
        // should only have a single processing instruction, not two
        // should also contain old value from initial test, not the second one
        assertEquals("", ChangeTrackingUtils.getOldValue(firstname));
        assertEquals("<Employee><FirstName><?change-value old=\"\"?>Mike</FirstName></Employee>",
                     XmlParseUtils.nodeToString(employee));
        assertEquals("isChanged", true, ChangeTrackingUtils.isChanged(firstname));
    }

    @Test
    public void markNewTextContentEmptyElementTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee><FirstName/></Employee>").getDocumentElement();
        Element firstname = DomUtils.findFirstChildElement(employee, "FirstName");
        assertEquals("isChanged", false, ChangeTrackingUtils.isChanged(firstname));
        assertNull(ChangeTrackingUtils.getOldValue(firstname));
        ChangeTrackingUtils.markNewTextContent(firstname);
        assertEquals("", ChangeTrackingUtils.getOldValue(firstname));
        assertEquals("<Employee><FirstName><?change-value old=\"\"?></FirstName></Employee>",
                     XmlParseUtils.nodeToString(employee));
        assertEquals("isChanged", true, ChangeTrackingUtils.isChanged(firstname));
    }

    @Test
    public void markNewTextContentAttributeTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee firstname=\"John\"/>").getDocumentElement();
        Attr firstname = employee.getAttributeNode("firstname");
        assertEquals("isChanged", false, ChangeTrackingUtils.isChanged(firstname));
        assertNull(ChangeTrackingUtils.getOldValue(firstname));
        ChangeTrackingUtils.markNewTextContent(firstname);
        assertEquals("", ChangeTrackingUtils.getOldValue(firstname));
        assertEquals("<Employee firstname=\"John\"><?change-attribute-value old=\"\" name=\"firstname\"?></Employee>",
                     XmlParseUtils.nodeToString(employee));
        assertEquals("isChanged", true, ChangeTrackingUtils.isChanged(firstname));
    }

    @Test
    public void markNewTextContentAttributeMultiTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee firstname=\"John\"/>").getDocumentElement();
        Attr firstname = employee.getAttributeNode("firstname");
        assertEquals("isChanged", false, ChangeTrackingUtils.isChanged(firstname));
        assertNull(ChangeTrackingUtils.getOldValue(firstname));
        ChangeTrackingUtils.markNewTextContent(firstname);
        firstname.setValue("Mike");
        ChangeTrackingUtils.markNewTextContent(firstname);
        // should only have a single processing instruction, not two
        // should also contain old value from initial test, not the second one
        assertEquals("", ChangeTrackingUtils.getOldValue(firstname));
        assertEquals("<Employee firstname=\"Mike\"><?change-attribute-value old=\"\" name=\"firstname\"?></Employee>",
                     XmlParseUtils.nodeToString(employee));
        assertEquals("isChanged", true, ChangeTrackingUtils.isChanged(firstname));
    }

    @Test
    public void markNewTextContentMultiAttributeTest() throws SAXException {
        Element employee = XmlParseUtils.parse("<Employee firstname=\"John\" lastname=\"Doe\"/>").getDocumentElement();
        Attr firstname = employee.getAttributeNode("firstname");
        Attr lastname = employee.getAttributeNode("lastname");
        assertEquals("isChanged", false, ChangeTrackingUtils.isChanged(firstname));
        assertEquals("isChanged", false, ChangeTrackingUtils.isChanged(lastname));
        assertNull(ChangeTrackingUtils.getOldValue(firstname));
        assertNull(ChangeTrackingUtils.getOldValue(lastname));
        ChangeTrackingUtils.markNewTextContent(firstname);
        assertEquals("isChanged", true, ChangeTrackingUtils.isChanged(firstname));
        ChangeTrackingUtils.markNewTextContent(lastname);
        assertEquals("", ChangeTrackingUtils.getOldValue(firstname));
        assertEquals("", ChangeTrackingUtils.getOldValue(lastname));
        assertEquals("<Employee firstname=\"John\" lastname=\"Doe\"><?change-attribute-value old=\"\" name=\"lastname\"?><?change-attribute-value old=\"\" name=\"firstname\"?></Employee>",
                     XmlParseUtils.nodeToString(employee));
        assertEquals("isChanged", true, ChangeTrackingUtils.isChanged(lastname));
    }

}
