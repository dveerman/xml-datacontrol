package org.adfemg.datacontrol.xml;

import org.adfemg.datacontrol.xml.data.DataSuite;
import org.adfemg.datacontrol.xml.ha.HaSuite;
import org.adfemg.datacontrol.xml.provider.ProviderSuite;
import org.adfemg.datacontrol.xml.utils.UtilsSuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ HaSuite.class, ProviderSuite.class, UtilsSuite.class, DataSuite.class })
public class AllSuite {
}
