package org.adfemg.datacontrol.lov;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oracle.adf.view.rich.model.AttributeDescriptor;
import oracle.adf.view.rich.model.ColumnDescriptor;
import oracle.adf.view.rich.model.TableModel;

import oracle.jbo.AttributeDef;
import oracle.jbo.AttributeHints;
import oracle.jbo.Key;
import oracle.jbo.LocaleContext;
import oracle.jbo.Row;
import oracle.jbo.ViewObject;

import org.apache.myfaces.trinidad.model.CollectionModel;


public class TableModelImpl extends TableModel {

    private final ViewObject viewObject;
    private final CollectionModel collectionModel;
    private final List<AttributeDescriptor> attributes = new ArrayList<AttributeDescriptor>();
    private final List<ColumnDescriptor> columns = new ArrayList<ColumnDescriptor>();

    /**
     * The constructor with mandatory fields.
     * @param viewObject the ViewObject.
     */
    public TableModelImpl(final ViewObject viewObject) {
        this.viewObject = viewObject;
        this.collectionModel = new CollectionModelImpl(viewObject);
        final LocaleContext locale = viewObject.getApplicationModule().getSession().getLocaleContext();
        for (AttributeDef adef : viewObject.getAttributeDefs()) {
            if (adef.getUIHelper().getDisplayHint(locale) == AttributeHints.ATTRIBUTE_DISPLAY_HINT_DISPLAY) {
                final AttributeDescriptorImpl ad = new AttributeDescriptorImpl(adef, locale);
                attributes.add(ad);
                columns.add(new AttributeColumnDescriptorImpl(ad));
            }
        }
    }

    public TableModelImpl(final ViewObject viewObject, final String... columnNames) {
        this(viewObject);
        orderColumns(columnNames);
    }

    @Override
    public CollectionModel getCollectionModel() {
        return collectionModel;
    }

    public ViewObject getViewOject() {
        return viewObject;
    }

    @Override
    public List<ColumnDescriptor> getColumnDescriptors() {
        return Collections.unmodifiableList(columns);
    }

    public AttributeDescriptor getAttribute(final String name) {
        for (AttributeDescriptor ad : attributes) {
            if (ad.getName().equals(name)) {
                return ad;
            }
        }
        return null;
    }

    public List<AttributeDescriptor> getAttributes() {
        return Collections.unmodifiableList(attributes);
    }

    public final void orderColumns(final String... columnNames) {
        for (int i = 0; i < columnNames.length; i++) { // Move attribute with the name attrNames[i] to position i.
            final String col = columnNames[i];
            int curPos = -1;
            // Find current position.
            for (int j = i; j < columnNames.length; j++) {
                if (col.equals(columns.get(j).getName())) {
                    curPos = j;
                    break;
                }
            }
            if (curPos == i) {
                continue;
            } else if (curPos == -1) {
                throw new IllegalArgumentException("Clomun not found: " + col);
            }
            columns.add(i, columns.remove(curPos));
            attributes.add(i, attributes.remove(curPos));
        }
        return;
    }

    private static class CollectionModelImpl extends CollectionModel {
        private final ViewObject viewObject;

        public CollectionModelImpl(final ViewObject vo) {
            this.viewObject = vo;
        }

        public Object getRowKey() {
            final Row row = viewObject.getCurrentRow();
            return row == null ? null : row.getKey();
        }

        public boolean isRowAvailable() {
            return viewObject.getCurrentRow() != null;
        }

        public int getRowCount() {
            return viewObject.getRowCount();
        }

        public Object getRowData() {
            // Return a map to be used in EL expressions to acces the attributes of the row.
            final Row row = viewObject.getCurrentRow();
            return new AbstractMap<String, Object>() {
                public Set<Map.Entry<String, Object>> entrySet() {
                    throw new UnsupportedOperationException("CollectionModelImpl.getRowData.entrySet not supported.");
                }

                @Override
                public Object get(final Object key) {
                    return row.getAttribute(key.toString());
                }
            };
        }

        public int getRowIndex() {
            return viewObject.getCurrentRowIndex();
        }

        public void setRowKey(final Object key) {
            if (key == null) {
                viewObject.reset();
            } else if (key instanceof Key) {
                final Row[] rows = viewObject.findByKey((Key) key, 1);
                if (rows == null || rows.length == 0) {
                    viewObject.reset();
                } else {
                    viewObject.setCurrentRow(rows[0]);
                }
            } else {
                throw new UnsupportedOperationException("CollectionModelImpl.setRowKey not (yet) supported with key: " +
                                                        key);
            }
        }

        public void setRowIndex(final int index) {
            if (viewObject.getRangeSize() != -1 || viewObject.getRangeStart() > 0) {
                throw new UnsupportedOperationException("CollectionModelImpl.setRowIndex only works without range paging " +
                                                        viewObject.getRangeSize() + " " + viewObject.getRangeStart());
            }
            viewObject.setCurrentRowAtRangeIndex(index);
        }

        public Object getWrappedData() {
            throw new UnsupportedOperationException("CollectionModelImpl.getWrappedData not (yet) supported.");
        }

        public void setWrappedData(final Object object) {
            throw new UnsupportedOperationException("CollectionModelImpl.setWrappedData not (yet) supported.");
        }
    }
}
