package org.adfemg.datacontrol.lov;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oracle.adf.view.rich.model.AttributeCriterion;
import oracle.adf.view.rich.model.AttributeDescriptor;
import oracle.adf.view.rich.model.ConjunctionCriterion;
import oracle.adf.view.rich.model.QueryDescriptor;

import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewCriteriaComponent;
import oracle.jbo.ViewCriteriaItem;
import oracle.jbo.ViewCriteriaRow;


public class AttributeCriterionImpl extends AttributeCriterion implements QueryModeHandler {

    private final AttributeDescriptor attribute;
    private boolean removable = false;
    private AttributeDescriptor.Operator operator;
    private List<? extends Object> values = null;

    private QueryDescriptor.QueryMode mode;

    public AttributeCriterionImpl(final AttributeDescriptor attribute) {
        this.attribute = attribute;
    }

    @Override
    public AttributeDescriptor getAttribute() {
        return attribute;
    }

    /**
     * Get the operator, in basic mode, this will always be the first/default operator.
     *
     * @return the Operator of the Attribute.
     */
    @Override
    public AttributeDescriptor.Operator getOperator() {
        if (QueryDescriptor.QueryMode.BASIC == getMode()) { // In basic mode, always use the first/default operator.
            return attribute.getSupportedOperators().iterator().next();
        }
        if (operator == null) { // At first call, select the first operator of the attribute.
            operator = attribute.getSupportedOperators().iterator().next();
        }
        return operator;
    }

    /**
     * The list with operators to choose in the UI, if this list is empty
     * the UI will not show the dropdown to make a selection.
     *
     * @return the list of operators.
     */
    @Override
    public Map<String, AttributeDescriptor.Operator> getOperators() {
        if (QueryDescriptor.QueryMode.BASIC == getMode()) { // Disable operators in basic mode.
            return Collections.emptyMap();
        }
        return new AbstractMap<String, AttributeDescriptor.Operator>() {
            public Set<Map.Entry<String, AttributeDescriptor.Operator>> entrySet() {
                // set with order.
                final Set<Map.Entry<String, AttributeDescriptor.Operator>> set =
                    new LinkedHashSet<Map.Entry<String, AttributeDescriptor.Operator>>();
                for (AttributeDescriptor.Operator op : attribute.getSupportedOperators()) {
                    final String key = op.getValue().toString();
                    set.add(new AbstractMap.SimpleImmutableEntry<String, AttributeDescriptor.Operator>(key, op));
                }
                return set;
            }
        };
    }

    @Override
    public List<? extends Object> getValues() {
        if (values == null) { // Make sure we have enough null values in the list for the operator with the most operands.
            int maxoperands = 0;
            for (AttributeDescriptor.Operator op : attribute.getSupportedOperators()) {
                maxoperands = Math.max(maxoperands, op.getOperandCount());
            }
            values = new ArrayList<Object>(maxoperands);
            for (int i = 0; i < maxoperands; i++) {
                values.add(null);
            }
        }
        return values;
    }

    @Override
    public boolean isRemovable() {
        return removable;
    }

    public void setRemovable(final boolean removable) {
        this.removable = removable;
    }

    @Override
    public void setOperator(final AttributeDescriptor.Operator operator) {
        this.operator = operator;
    }

    @Override
    public void changeMode(final QueryDescriptor.QueryMode mode) {
        this.mode = mode;
    }

    @Override
    public QueryDescriptor.QueryMode getMode() {
        return mode;
    }

    public ViewCriteriaRow addViewCriteriaRow(final ViewCriteria viewCriteria,
                                              final ConjunctionCriterion.Conjunction conjunction) {
        final ViewCriteriaRow vcRow = viewCriteria.createViewCriteriaRow();
        vcRow.setConjunction(ConjunctionCriterion.Conjunction.OR == conjunction ? ViewCriteriaComponent.VC_CONJ_OR :
                             ViewCriteriaComponent.VC_CONJ_AND);
        vcRow.setUpperColumns(!getMatchCase());
        final ViewCriteriaItem item = vcRow.ensureCriteriaItem(getAttribute().getName());
        item.setOperator(getOperator().getValue().toString());
        for (int i = 0; i < getOperator().getOperandCount(); i++) {
            item.getValues().get(i).setValue(getValues().get(i));
        }
        viewCriteria.add(vcRow);
        return vcRow;
    }

}
